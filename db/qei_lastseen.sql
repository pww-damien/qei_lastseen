-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 27, 2019 at 03:26 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qei_lastseen`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-14 02:45:40', '2019-05-14 02:45:40', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8888/qei_lastseen', 'yes'),
(2, 'home', 'http://localhost:8888/qei_lastseen', 'yes'),
(3, 'blogname', 'LastSeen', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'damien.doonan@publicis.com.au', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:212:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:53:\"project-type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?project-type=$matches[1]&feed=$matches[2]\";s:48:\"project-type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?project-type=$matches[1]&feed=$matches[2]\";s:29:\"project-type/([^/]+)/embed/?$\";s:45:\"index.php?project-type=$matches[1]&embed=true\";s:41:\"project-type/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?project-type=$matches[1]&paged=$matches[2]\";s:23:\"project-type/([^/]+)/?$\";s:34:\"index.php?project-type=$matches[1]\";s:59:\"project-attributes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?project-attributes=$matches[1]&feed=$matches[2]\";s:54:\"project-attributes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?project-attributes=$matches[1]&feed=$matches[2]\";s:35:\"project-attributes/([^/]+)/embed/?$\";s:51:\"index.php?project-attributes=$matches[1]&embed=true\";s:47:\"project-attributes/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?project-attributes=$matches[1]&paged=$matches[2]\";s:29:\"project-attributes/([^/]+)/?$\";s:40:\"index.php?project-attributes=$matches[1]\";s:57:\"slider-locations/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?slider-locations=$matches[1]&feed=$matches[2]\";s:52:\"slider-locations/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?slider-locations=$matches[1]&feed=$matches[2]\";s:33:\"slider-locations/([^/]+)/embed/?$\";s:49:\"index.php?slider-locations=$matches[1]&embed=true\";s:45:\"slider-locations/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?slider-locations=$matches[1]&paged=$matches[2]\";s:27:\"slider-locations/([^/]+)/?$\";s:38:\"index.php?slider-locations=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:37:\"portfolio/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"portfolio/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"portfolio/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"portfolio/([^/]+)/embed/?$\";s:42:\"index.php?portfolio=$matches[1]&embed=true\";s:30:\"portfolio/([^/]+)/trackback/?$\";s:36:\"index.php?portfolio=$matches[1]&tb=1\";s:38:\"portfolio/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&paged=$matches[2]\";s:45:\"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&cpage=$matches[2]\";s:35:\"portfolio/([^/]+)/wc-api(/(.*))?/?$\";s:50:\"index.php?portfolio=$matches[1]&wc-api=$matches[3]\";s:41:\"portfolio/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:52:\"portfolio/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:34:\"portfolio/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?portfolio=$matches[1]&page=$matches[2]\";s:26:\"portfolio/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"portfolio/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"portfolio/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"nectar_slider/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"nectar_slider/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"nectar_slider/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"nectar_slider/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"nectar_slider/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"nectar_slider/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"nectar_slider/([^/]+)/embed/?$\";s:46:\"index.php?nectar_slider=$matches[1]&embed=true\";s:34:\"nectar_slider/([^/]+)/trackback/?$\";s:40:\"index.php?nectar_slider=$matches[1]&tb=1\";s:42:\"nectar_slider/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?nectar_slider=$matches[1]&paged=$matches[2]\";s:49:\"nectar_slider/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?nectar_slider=$matches[1]&cpage=$matches[2]\";s:39:\"nectar_slider/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?nectar_slider=$matches[1]&wc-api=$matches[3]\";s:45:\"nectar_slider/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"nectar_slider/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"nectar_slider/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?nectar_slider=$matches[1]&page=$matches[2]\";s:30:\"nectar_slider/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"nectar_slider/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"nectar_slider/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"nectar_slider/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"nectar_slider/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"nectar_slider/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=6&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:35:\"js_composer_salient/js_composer.php\";i:1;s:27:\"svg-support/svg-support.php\";i:2;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'LastSeen', 'yes'),
(41, 'stylesheet', 'LastSeen', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '6', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:10:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"blog-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"page-sidebar\";a:0:{}s:19:\"woocommerce-sidebar\";a:0:{}s:20:\"nectar-extra-sidebar\";a:0:{}s:13:\"footer-area-1\";a:0:{}s:13:\"footer-area-2\";a:0:{}s:13:\"footer-area-3\";a:0:{}s:13:\"footer-area-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:15:{i:1558927355;a:1:{s:28:\"wp_1_wc_privacy_cleanup_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:37:\"wp_1_wc_privacy_cleanup_cron_interval\";s:4:\"args\";a:0:{}s:8:\"interval\";i:300;}}}i:1558927365;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1558928741;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1558930945;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1558936239;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1558947039;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1558968341;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1559001600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559011541;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559011555;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559011557;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559011839;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559011849;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1559606400;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(115, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1557802198;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(126, 'can_compress_scripts', '1', 'no'),
(139, 'current_theme', 'Salient', 'yes'),
(140, 'theme_mods_LastSeen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"top_nav\";i:62;s:13:\"secondary_nav\";i:62;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(142, 'widget_nectar_popular_posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(143, 'shop_catalog_image_size', 'a:3:{s:5:\"width\";s:3:\"375\";s:6:\"height\";s:3:\"400\";s:4:\"crop\";i:1;}', 'yes'),
(144, 'shop_single_image_size', 'a:3:{s:5:\"width\";s:3:\"600\";s:6:\"height\";s:3:\"630\";s:4:\"crop\";i:1;}', 'yes'),
(145, 'shop_thumbnail_image_size', 'a:3:{s:5:\"width\";s:3:\"130\";s:6:\"height\";s:3:\"130\";s:4:\"crop\";i:1;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(146, 'salient_redux', 'a:453:{s:8:\"last_tab\";s:1:\"8\";s:10:\"theme-skin\";s:8:\"material\";s:14:\"button-styling\";s:23:\"slightly_rounded_shadow\";s:16:\"overall-bg-color\";s:0:\"\";s:18:\"overall-font-color\";s:7:\"#5b5b5b\";s:11:\"body-border\";s:1:\"0\";s:17:\"body-border-color\";s:7:\"#ffffff\";s:16:\"body-border-size\";s:0:\"\";s:11:\"back-to-top\";s:1:\"0\";s:18:\"back-to-top-mobile\";s:1:\"0\";s:18:\"one-page-scrolling\";s:1:\"1\";s:10:\"responsive\";s:1:\"1\";s:14:\"ext_responsive\";s:1:\"1\";s:19:\"max_container_width\";s:4:\"1425\";s:15:\"lightbox_script\";s:0:\"\";s:16:\"default-lightbox\";s:1:\"0\";s:23:\"disable-mobile-parallax\";s:1:\"0\";s:24:\"disable-mobile-video-bgs\";s:1:\"0\";s:23:\"column_animation_easing\";s:6:\"linear\";s:23:\"column_animation_timing\";s:3:\"650\";s:20:\"external-dynamic-css\";s:1:\"0\";s:16:\"google-analytics\";s:0:\"\";s:19:\"google-maps-api-key\";s:0:\"\";s:10:\"custom-css\";s:0:\"\";s:11:\"disable_tgm\";s:1:\"1\";s:22:\"disable_home_slider_pt\";s:1:\"1\";s:24:\"disable_nectar_slider_pt\";s:1:\"0\";s:12:\"accent-color\";s:7:\"#046272\";s:13:\"extra-color-1\";s:7:\"#4668e2\";s:13:\"extra-color-2\";s:7:\"#8c8c8c\";s:13:\"extra-color-3\";s:7:\"#333333\";s:20:\"extra-color-gradient\";a:2:{s:4:\"from\";s:7:\"#3452ff\";s:2:\"to\";s:7:\"#ff1053\";}s:22:\"extra-color-gradient-2\";a:2:{s:4:\"from\";s:7:\"#2AC4EA\";s:2:\"to\";s:7:\"#32d6ff\";}s:12:\"boxed_layout\";s:1:\"0\";s:16:\"background-color\";s:7:\"#f1f1f1\";s:16:\"background_image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:17:\"background-repeat\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:16:\"background-cover\";s:1:\"0\";s:19:\"extended-theme-font\";s:1:\"0\";s:22:\"navigation_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"500\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:5:\"latin\";s:14:\"text-transform\";s:10:\"capitalize\";s:9:\"font-size\";s:4:\"17px\";s:11:\"line-height\";s:4:\"12px\";s:14:\"letter-spacing\";s:3:\"0px\";}s:31:\"navigation_dropdown_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"500\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:10:\"capitalize\";s:9:\"font-size\";s:4:\"16px\";s:11:\"line-height\";s:4:\"12px\";s:14:\"letter-spacing\";s:3:\"0px\";}s:24:\"page_heading_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"300\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:9:\"latin-ext\";s:14:\"text-transform\";s:4:\"none\";s:9:\"font-size\";s:4:\"52px\";s:11:\"line-height\";s:4:\"48px\";s:14:\"letter-spacing\";s:3:\"0px\";}s:33:\"page_heading_subtitle_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"26px\";s:11:\"line-height\";s:4:\"24px\";s:14:\"letter-spacing\";s:3:\"0px\";}s:26:\"off_canvas_nav_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:34:\"off_canvas_nav_subtext_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:16:\"body_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:5:\"latin\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"16px\";s:11:\"line-height\";s:4:\"24px\";s:14:\"letter-spacing\";s:3:\"1px\";}s:14:\"h1_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"500\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"54px\";s:11:\"line-height\";s:4:\"60px\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h2_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:9:\"latin-ext\";s:14:\"text-transform\";s:4:\"none\";s:9:\"font-size\";s:4:\"26px\";s:11:\"line-height\";s:4:\"32px\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h3_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"20px\";s:11:\"line-height\";s:4:\"24px\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h4_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"16px\";s:11:\"line-height\";s:4:\"20px\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h5_font_family\";a:10:{s:11:\"font-family\";s:12:\"Merriweather\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"23px\";s:11:\"line-height\";s:4:\"23px\";s:14:\"letter-spacing\";s:5:\"0.5px\";}s:14:\"h6_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:13:\"i_font_family\";a:10:{s:11:\"font-family\";s:17:\"Libre Baskerville\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:10:\"capitalize\";s:9:\"font-size\";s:4:\"18px\";s:11:\"line-height\";s:4:\"18px\";s:14:\"letter-spacing\";s:5:\"0.5px\";}s:17:\"label_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:33:\"nectar_slider_heading_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"600\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:10:\"capitalize\";s:9:\"font-size\";s:5:\"102px\";s:11:\"line-height\";s:5:\"110px\";s:14:\"letter-spacing\";s:0:\"\";}s:31:\"home_slider_caption_font_family\";a:10:{s:11:\"font-family\";s:17:\"Libre Baskerville\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"400\";s:10:\"font-style\";s:6:\"italic\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"22px\";s:11:\"line-height\";s:4:\"22px\";s:14:\"letter-spacing\";s:0:\"\";}s:23:\"testimonial_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"600\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"19px\";s:11:\"line-height\";s:4:\"19px\";s:14:\"letter-spacing\";s:0:\"\";}s:28:\"sidebar_footer_h_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"600\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"14px\";s:11:\"line-height\";s:4:\"14px\";s:14:\"letter-spacing\";s:3:\"2px\";}s:29:\"portfolio_filters_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:4:\"true\";s:11:\"font-weight\";s:3:\"500\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:10:\"capitalize\";s:9:\"font-size\";s:4:\"16px\";s:11:\"line-height\";s:4:\"24px\";s:14:\"letter-spacing\";s:0:\"\";}s:29:\"portfolio_caption_font_family\";a:10:{s:11:\"font-family\";s:10:\"Montserrat\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:3:\"300\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:9:\"latin-ext\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:4:\"16px\";s:11:\"line-height\";s:4:\"16px\";s:14:\"letter-spacing\";s:0:\"\";}s:25:\"team_member_h_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:26:\"nectar_dropcap_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:41:\"nectar_sidebar_footer_headers_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:41:\"nectar_woo_shop_product_title_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:45:\"nectar_woo_shop_product_secondary_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:33:\"use-responsive-heading-typography\";s:1:\"0\";s:26:\"h1-small-desktop-font-size\";s:2:\"75\";s:19:\"h1-tablet-font-size\";s:2:\"70\";s:18:\"h1-phone-font-size\";s:2:\"65\";s:26:\"h2-small-desktop-font-size\";s:2:\"85\";s:19:\"h2-tablet-font-size\";s:2:\"80\";s:18:\"h2-phone-font-size\";s:2:\"70\";s:26:\"h3-small-desktop-font-size\";s:2:\"85\";s:19:\"h3-tablet-font-size\";s:2:\"80\";s:18:\"h3-phone-font-size\";s:2:\"70\";s:26:\"h4-small-desktop-font-size\";s:3:\"100\";s:19:\"h4-tablet-font-size\";s:2:\"90\";s:18:\"h4-phone-font-size\";s:2:\"90\";s:26:\"h5-small-desktop-font-size\";s:3:\"100\";s:19:\"h5-tablet-font-size\";s:3:\"100\";s:18:\"h5-phone-font-size\";s:3:\"100\";s:26:\"h6-small-desktop-font-size\";s:3:\"100\";s:19:\"h6-tablet-font-size\";s:3:\"100\";s:18:\"h6-phone-font-size\";s:3:\"100\";s:28:\"body-small-desktop-font-size\";s:3:\"100\";s:21:\"body-tablet-font-size\";s:3:\"100\";s:20:\"body-phone-font-size\";s:3:\"100\";s:8:\"use-logo\";s:1:\"1\";s:4:\"logo\";a:5:{s:3:\"url\";s:92:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05x.png\";s:2:\"id\";s:4:\"3199\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:100:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05x-150x135.png\";}s:11:\"retina-logo\";a:5:{s:3:\"url\";s:88:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master.png\";s:2:\"id\";s:4:\"3198\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:96:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master-150x135.png\";}s:11:\"logo-height\";s:2:\"80\";s:18:\"mobile-logo-height\";s:0:\"\";s:11:\"mobile-logo\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:14:\"header-padding\";s:2:\"28\";s:19:\"header-remove-fixed\";s:1:\"0\";s:19:\"header-mobile-fixed\";s:1:\"1\";s:29:\"header-menu-mobile-breakpoint\";s:4:\"1000\";s:17:\"header-box-shadow\";s:4:\"none\";s:24:\"header-menu-item-spacing\";s:2:\"10\";s:17:\"header-bg-opacity\";s:3:\"100\";s:12:\"header-color\";s:5:\"light\";s:23:\"header-background-color\";s:7:\"#ffffff\";s:17:\"header-font-color\";s:7:\"#565656\";s:23:\"header-font-hover-color\";s:7:\"#2b474c\";s:32:\"header-dropdown-background-color\";s:7:\"#1F1F1F\";s:38:\"header-dropdown-background-hover-color\";s:7:\"#313233\";s:26:\"header-dropdown-font-color\";s:7:\"#CCCCCC\";s:32:\"header-dropdown-font-hover-color\";s:7:\"#2b474c\";s:34:\"header-dropdown-heading-font-color\";s:7:\"#ffffff\";s:40:\"header-dropdown-heading-font-hover-color\";s:7:\"#ffffff\";s:22:\"header-separator-color\";s:7:\"#eeeeee\";s:33:\"secondary-header-background-color\";s:7:\"#F8F8F8\";s:27:\"secondary-header-font-color\";s:7:\"#666666\";s:33:\"secondary-header-font-hover-color\";s:7:\"#222222\";s:45:\"header-slide-out-widget-area-background-color\";s:7:\"#2b474c\";s:47:\"header-slide-out-widget-area-background-color-2\";s:0:\"\";s:41:\"header-slide-out-widget-area-header-color\";s:7:\"#ffffff\";s:34:\"header-slide-out-widget-area-color\";s:7:\"#eefbfa\";s:40:\"header-slide-out-widget-area-hover-color\";s:7:\"#ffffff\";s:43:\"header-slide-out-widget-area-close-bg-color\";s:7:\"#ff1053\";s:45:\"header-slide-out-widget-area-close-icon-color\";s:7:\"#ffffff\";s:13:\"header_format\";s:26:\"centered-logo-between-menu\";s:16:\"header-fullwidth\";s:1:\"1\";s:21:\"header-disable-search\";s:1:\"1\";s:26:\"header-disable-ajax-search\";s:1:\"1\";s:21:\"header-account-button\";s:1:\"0\";s:25:\"header-account-button-url\";s:0:\"\";s:13:\"header_layout\";s:8:\"standard\";s:21:\"secondary-header-text\";s:0:\"\";s:21:\"secondary-header-link\";s:0:\"\";s:23:\"enable_social_in_header\";s:1:\"0\";s:24:\"use-facebook-icon-header\";s:0:\"\";s:23:\"use-twitter-icon-header\";s:0:\"\";s:27:\"use-google-plus-icon-header\";s:0:\"\";s:21:\"use-vimeo-icon-header\";s:0:\"\";s:24:\"use-dribbble-icon-header\";s:0:\"\";s:25:\"use-pinterest-icon-header\";s:0:\"\";s:23:\"use-youtube-icon-header\";s:0:\"\";s:22:\"use-tumblr-icon-header\";s:0:\"\";s:24:\"use-linkedin-icon-header\";s:0:\"\";s:19:\"use-rss-icon-header\";s:0:\"\";s:23:\"use-behance-icon-header\";s:0:\"\";s:25:\"use-instagram-icon-header\";s:0:\"\";s:22:\"use-flickr-icon-header\";s:0:\"\";s:23:\"use-spotify-icon-header\";s:0:\"\";s:22:\"use-github-icon-header\";s:0:\"\";s:29:\"use-stackexchange-icon-header\";s:0:\"\";s:26:\"use-soundcloud-icon-header\";s:0:\"\";s:18:\"use-vk-icon-header\";s:0:\"\";s:20:\"use-vine-icon-header\";s:0:\"\";s:21:\"use-houzz-icon-header\";s:1:\"0\";s:20:\"use-yelp-icon-header\";s:1:\"0\";s:24:\"use-mixcloud-icon-header\";s:1:\"0\";s:24:\"use-snapchat-icon-header\";s:1:\"0\";s:24:\"use-bandcamp-icon-header\";s:1:\"0\";s:27:\"use-tripadvisor-icon-header\";s:1:\"0\";s:24:\"use-telegram-icon-header\";s:1:\"0\";s:21:\"use-slack-icon-header\";s:1:\"0\";s:22:\"use-medium-icon-header\";s:1:\"0\";s:21:\"use-email-icon-header\";s:1:\"0\";s:21:\"use-phone-icon-header\";s:1:\"0\";s:18:\"transparent-header\";s:1:\"1\";s:20:\"header-starting-logo\";a:5:{s:3:\"url\";s:95:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05xNEG.png\";s:2:\"id\";s:4:\"3200\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:103:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05xNEG-150x135.png\";}s:27:\"header-starting-retina-logo\";a:5:{s:3:\"url\";s:91:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-masterNEG.png\";s:2:\"id\";s:4:\"3201\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:99:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-masterNEG-150x135.png\";}s:25:\"header-starting-logo-dark\";a:5:{s:3:\"url\";s:92:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05x.png\";s:2:\"id\";s:4:\"3199\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:100:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05x-150x135.png\";}s:32:\"header-starting-retina-logo-dark\";a:5:{s:3:\"url\";s:88:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master.png\";s:2:\"id\";s:4:\"3198\";s:6:\"height\";s:3:\"135\";s:5:\"width\";s:3:\"600\";s:9:\"thumbnail\";s:96:\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master-150x135.png\";}s:21:\"header-starting-color\";s:7:\"#ffffff\";s:29:\"header-transparent-dark-color\";s:7:\"#000000\";s:28:\"header-permanent-transparent\";s:1:\"0\";s:24:\"header-inherit-row-color\";s:1:\"0\";s:20:\"header-remove-border\";s:1:\"0\";s:32:\"transparent-header-shadow-helper\";s:1:\"0\";s:19:\"header-hover-effect\";s:18:\"animated_underline\";s:24:\"header-hide-until-needed\";s:0:\"\";s:23:\"header-resize-on-scroll\";s:1:\"1\";s:34:\"header-resize-on-scroll-shrink-num\";s:2:\"00\";s:25:\"condense-header-on-scroll\";s:0:\"\";s:21:\"header-dropdown-style\";s:7:\"minimal\";s:23:\"header-dropdown-opacity\";s:3:\"100\";s:22:\"header-dropdown-arrows\";s:7:\"inherit\";s:21:\"header-megamenu-width\";s:9:\"contained\";s:34:\"header-megamenu-remove-transparent\";s:1:\"0\";s:28:\"header-slide-out-widget-area\";s:1:\"0\";s:34:\"header-slide-out-widget-area-style\";s:20:\"slide-out-from-right\";s:46:\"header-slide-out-widget-area-dropdown-behavior\";s:7:\"default\";s:35:\"header-slide-out-widget-area-social\";s:1:\"0\";s:40:\"header-slide-out-widget-area-bottom-text\";s:0:\"\";s:44:\"header-slide-out-widget-area-overlay-opacity\";s:6:\"medium\";s:46:\"header-slide-out-widget-area-top-nav-in-mobile\";s:1:\"0\";s:23:\"enable-main-footer-area\";s:1:\"1\";s:14:\"footer_columns\";s:1:\"3\";s:19:\"footer-custom-color\";s:1:\"0\";s:23:\"footer-background-color\";s:7:\"#313233\";s:17:\"footer-font-color\";s:7:\"#CCCCCC\";s:27:\"footer-secondary-font-color\";s:7:\"#777777\";s:33:\"footer-copyright-background-color\";s:7:\"#1F1F1F\";s:27:\"footer-copyright-font-color\";s:7:\"#777777\";s:33:\"footer-copyright-icon-hover-color\";s:7:\"#ffffff\";s:21:\"footer-copyright-line\";s:1:\"1\";s:17:\"footer-full-width\";s:1:\"0\";s:13:\"footer-reveal\";s:1:\"1\";s:20:\"footer-reveal-shadow\";s:5:\"small\";s:23:\"footer-copyright-layout\";s:7:\"default\";s:29:\"disable-copyright-footer-area\";s:1:\"0\";s:21:\"footer-copyright-text\";s:44:\"All Right Reserved, Queensland Eye Institute\";s:22:\"disable-auto-copyright\";s:0:\"\";s:23:\"footer-background-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:31:\"footer-background-image-overlay\";s:3:\"0.8\";s:17:\"use-facebook-icon\";s:1:\"1\";s:16:\"use-twitter-icon\";s:1:\"1\";s:20:\"use-google-plus-icon\";s:1:\"0\";s:14:\"use-vimeo-icon\";s:1:\"0\";s:17:\"use-dribbble-icon\";s:1:\"0\";s:18:\"use-pinterest-icon\";s:1:\"0\";s:16:\"use-youtube-icon\";s:1:\"1\";s:15:\"use-tumblr-icon\";s:1:\"0\";s:17:\"use-linkedin-icon\";s:1:\"1\";s:12:\"use-rss-icon\";s:0:\"\";s:16:\"use-behance-icon\";s:0:\"\";s:18:\"use-instagram-icon\";s:1:\"1\";s:15:\"use-flickr-icon\";s:0:\"\";s:16:\"use-spotify-icon\";s:0:\"\";s:15:\"use-github-icon\";s:0:\"\";s:22:\"use-stackexchange-icon\";s:0:\"\";s:19:\"use-soundcloud-icon\";s:0:\"\";s:11:\"use-vk-icon\";s:0:\"\";s:13:\"use-vine-icon\";s:0:\"\";s:14:\"use-houzz-icon\";s:1:\"0\";s:13:\"use-yelp-icon\";s:1:\"0\";s:17:\"use-snapchat-icon\";s:1:\"0\";s:17:\"use-mixcloud-icon\";s:1:\"0\";s:17:\"use-bandcamp-icon\";s:1:\"0\";s:20:\"use-tripadvisor-icon\";s:1:\"0\";s:17:\"use-telegram-icon\";s:1:\"0\";s:14:\"use-slack-icon\";s:1:\"0\";s:15:\"use-medium-icon\";s:1:\"0\";s:17:\"ajax-page-loading\";s:1:\"1\";s:17:\"transition-method\";s:8:\"standard\";s:32:\"disable-transition-fade-on-click\";s:1:\"0\";s:28:\"disable-transition-on-mobile\";s:1:\"1\";s:17:\"transition-effect\";s:8:\"standard\";s:12:\"loading-icon\";s:8:\"material\";s:19:\"loading-icon-colors\";a:2:{s:4:\"from\";s:7:\"#12526d\";s:2:\"to\";s:7:\"#3452ff\";}s:13:\"loading-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:23:\"loading-image-animation\";s:4:\"none\";s:19:\"transition-bg-color\";s:7:\"#2b474c\";s:21:\"transition-bg-color-2\";s:0:\"\";s:17:\"header-auto-title\";s:1:\"0\";s:24:\"header-animate-in-effect\";s:4:\"none\";s:23:\"header-down-arrow-style\";s:7:\"default\";s:10:\"form-style\";s:7:\"minimal\";s:17:\"form-fancy-select\";s:1:\"1\";s:21:\"form-submit-btn-style\";s:7:\"regular\";s:8:\"cta-text\";s:31:\"Buy your tickets to the auction\";s:7:\"cta-btn\";s:18:\"Click here to book\";s:12:\"cta-btn-link\";s:1:\"#\";s:20:\"cta-background-color\";s:7:\"#2b474c\";s:14:\"cta-text-color\";s:7:\"#ffffff\";s:13:\"cta-btn-color\";s:12:\"accent-color\";s:21:\"main_portfolio_layout\";s:1:\"3\";s:28:\"main_portfolio_project_style\";s:1:\"9\";s:27:\"main_portfolio_item_spacing\";s:7:\"default\";s:21:\"portfolio_use_masonry\";s:1:\"1\";s:29:\"portfolio_masonry_grid_sizing\";s:7:\"default\";s:24:\"portfolio_inline_filters\";s:1:\"1\";s:20:\"portfolio_single_nav\";s:15:\"after_project_2\";s:27:\"portfolio_loading_animation\";s:19:\"fade_in_from_bottom\";s:24:\"portfolio_sidebar_follow\";s:1:\"0\";s:16:\"portfolio_social\";s:1:\"0\";s:22:\"portfolio_social_style\";s:18:\"fixed_bottom_right\";s:26:\"portfolio-facebook-sharing\";s:1:\"1\";s:25:\"portfolio-twitter-sharing\";s:1:\"1\";s:29:\"portfolio-google-plus-sharing\";s:1:\"1\";s:27:\"portfolio-pinterest-sharing\";s:1:\"1\";s:26:\"portfolio-linkedin-sharing\";s:1:\"1\";s:14:\"portfolio_date\";s:1:\"0\";s:20:\"portfolio_pagination\";s:1:\"1\";s:25:\"portfolio_pagination_type\";s:7:\"default\";s:26:\"portfolio_extra_pagination\";s:1:\"0\";s:27:\"portfolio_pagination_number\";s:2:\"20\";s:25:\"portfolio_remove_comments\";s:1:\"1\";s:22:\"portfolio_rewrite_slug\";s:0:\"\";s:14:\"carousel-title\";s:0:\"\";s:13:\"carousel-link\";s:0:\"\";s:23:\"portfolio-sortable-text\";s:0:\"\";s:19:\"main-portfolio-link\";s:0:\"\";s:34:\"portfolio_same_category_single_nav\";s:1:\"0\";s:9:\"blog_type\";s:30:\"masonry-blog-full-screen-width\";s:18:\"blog_standard_type\";s:17:\"featured_img_left\";s:17:\"blog_masonry_type\";s:13:\"meta_overlaid\";s:25:\"blog_auto_masonry_spacing\";s:3:\"8px\";s:22:\"blog_loading_animation\";s:7:\"fade_in\";s:16:\"blog_header_type\";s:15:\"default_minimal\";s:17:\"blog_hide_sidebar\";s:1:\"1\";s:14:\"blog_enable_ss\";s:1:\"0\";s:24:\"blog_hide_featured_image\";s:1:\"1\";s:21:\"blog_archive_bg_image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:39:\"blog_post_header_inherit_featured_image\";s:1:\"0\";s:10:\"author_bio\";s:1:\"1\";s:17:\"blog_auto_excerpt\";s:1:\"0\";s:19:\"blog_excerpt_length\";s:0:\"\";s:19:\"blog_next_post_link\";s:1:\"1\";s:25:\"blog_next_post_link_style\";s:19:\"fullwidth_next_prev\";s:18:\"blog_related_posts\";s:1:\"0\";s:24:\"blog_related_posts_style\";s:8:\"material\";s:29:\"blog_related_posts_title_text\";s:13:\"related_posts\";s:11:\"blog_social\";s:1:\"0\";s:17:\"blog_social_style\";s:18:\"fixed_bottom_right\";s:21:\"blog-facebook-sharing\";s:1:\"1\";s:20:\"blog-twitter-sharing\";s:1:\"1\";s:24:\"blog-google-plus-sharing\";s:1:\"1\";s:22:\"blog-pinterest-sharing\";s:1:\"1\";s:21:\"blog-linkedin-sharing\";s:1:\"1\";s:12:\"display_tags\";s:1:\"0\";s:17:\"display_full_date\";s:1:\"0\";s:20:\"blog_pagination_type\";s:7:\"default\";s:16:\"extra_pagination\";s:1:\"0\";s:18:\"recent-posts-title\";s:0:\"\";s:17:\"recent-posts-link\";s:0:\"\";s:23:\"blog_remove_single_date\";s:0:\"\";s:25:\"blog_remove_single_author\";s:0:\"\";s:33:\"blog_remove_single_comment_number\";s:0:\"\";s:30:\"blog_remove_single_nectar_love\";s:0:\"\";s:21:\"blog_remove_post_date\";s:0:\"\";s:23:\"blog_remove_post_author\";s:0:\"\";s:31:\"blog_remove_post_comment_number\";s:0:\"\";s:28:\"blog_remove_post_nectar_love\";s:0:\"\";s:11:\"enable-cart\";s:1:\"0\";s:15:\"ajax-cart-style\";s:8:\"dropdown\";s:16:\"main_shop_layout\";s:10:\"no-sidebar\";s:21:\"single_product_layout\";s:10:\"no-sidebar\";s:13:\"product_style\";s:7:\"classic\";s:20:\"product_desktop_cols\";s:7:\"default\";s:26:\"product_desktop_small_cols\";s:7:\"default\";s:19:\"product_tablet_cols\";s:7:\"default\";s:18:\"product_phone_cols\";s:7:\"default\";s:18:\"product_quick_view\";s:0:\"\";s:16:\"product_bg_color\";s:7:\"#ffffff\";s:24:\"product_minimal_bg_color\";s:7:\"#ffffff\";s:24:\"product_archive_bg_color\";s:7:\"#f6f6f6\";s:23:\"product_hover_alt_image\";s:1:\"0\";s:27:\"single_product_gallery_type\";s:10:\"ios_slider\";s:20:\"product_tab_position\";s:9:\"fullwidth\";s:21:\"woo-products-per-page\";s:0:\"\";s:20:\"woo_hide_product_sku\";s:1:\"0\";s:10:\"woo_social\";s:1:\"1\";s:16:\"woo_social_style\";s:18:\"fixed_bottom_right\";s:20:\"woo-facebook-sharing\";s:1:\"0\";s:19:\"woo-twitter-sharing\";s:1:\"0\";s:23:\"woo-google-plus-sharing\";s:1:\"0\";s:21:\"woo-pinterest-sharing\";s:1:\"0\";s:20:\"woo-linkedin-sharing\";s:1:\"0\";s:21:\"search-results-layout\";s:15:\"list-no-sidebar\";s:30:\"search-results-header-bg-color\";s:0:\"\";s:32:\"search-results-header-font-color\";s:0:\"\";s:30:\"search-results-header-bg-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:17:\"page-404-bg-color\";s:0:\"\";s:19:\"page-404-font-color\";s:0:\"\";s:17:\"page-404-bg-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:31:\"page-404-bg-image-overlay-color\";s:0:\"\";s:20:\"page-404-home-button\";s:1:\"1\";s:24:\"sharing_btn_accent_color\";s:1:\"1\";s:12:\"facebook-url\";s:0:\"\";s:11:\"twitter-url\";s:0:\"\";s:15:\"google-plus-url\";s:0:\"\";s:9:\"vimeo-url\";s:0:\"\";s:12:\"dribbble-url\";s:0:\"\";s:13:\"pinterest-url\";s:0:\"\";s:11:\"youtube-url\";s:0:\"\";s:10:\"tumblr-url\";s:0:\"\";s:12:\"linkedin-url\";s:0:\"\";s:7:\"rss-url\";s:0:\"\";s:11:\"behance-url\";s:0:\"\";s:10:\"flickr-url\";s:0:\"\";s:11:\"spotify-url\";s:0:\"\";s:13:\"instagram-url\";s:0:\"\";s:10:\"github-url\";s:0:\"\";s:17:\"stackexchange-url\";s:0:\"\";s:14:\"soundcloud-url\";s:0:\"\";s:6:\"vk-url\";s:0:\"\";s:8:\"vine-url\";s:0:\"\";s:9:\"houzz-url\";s:0:\"\";s:8:\"yelp-url\";s:0:\"\";s:12:\"snapchat-url\";s:0:\"\";s:12:\"mixcloud-url\";s:0:\"\";s:12:\"bandcamp-url\";s:0:\"\";s:15:\"tripadvisor-url\";s:0:\"\";s:12:\"telegram-url\";s:0:\"\";s:9:\"slack-url\";s:0:\"\";s:10:\"medium-url\";s:0:\"\";s:9:\"email-url\";s:0:\"\";s:9:\"phone-url\";s:0:\"\";s:10:\"zoom-level\";s:0:\"\";s:15:\"enable-map-zoom\";s:1:\"0\";s:10:\"center-lat\";s:0:\"\";s:10:\"center-lng\";s:0:\"\";s:14:\"use-marker-img\";s:1:\"0\";s:10:\"marker-img\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:20:\"enable-map-animation\";s:1:\"1\";s:11:\"map-point-1\";s:1:\"0\";s:9:\"latitude1\";s:0:\"\";s:10:\"longitude1\";s:0:\"\";s:9:\"map-info1\";s:0:\"\";s:11:\"map-point-2\";s:1:\"0\";s:9:\"latitude2\";s:0:\"\";s:10:\"longitude2\";s:0:\"\";s:9:\"map-info2\";s:0:\"\";s:11:\"map-point-3\";s:1:\"0\";s:9:\"latitude3\";s:0:\"\";s:10:\"longitude3\";s:0:\"\";s:9:\"map-info3\";s:0:\"\";s:11:\"map-point-4\";s:1:\"0\";s:9:\"latitude4\";s:0:\"\";s:10:\"longitude4\";s:0:\"\";s:9:\"map-info4\";s:0:\"\";s:11:\"map-point-5\";s:1:\"0\";s:9:\"latitude5\";s:0:\"\";s:10:\"longitude5\";s:0:\"\";s:9:\"map-info5\";s:0:\"\";s:11:\"map-point-6\";s:1:\"0\";s:9:\"latitude6\";s:0:\"\";s:10:\"longitude6\";s:0:\"\";s:9:\"map-info6\";s:0:\"\";s:11:\"map-point-7\";s:1:\"0\";s:9:\"latitude7\";s:0:\"\";s:10:\"longitude7\";s:0:\"\";s:9:\"map-info7\";s:0:\"\";s:11:\"map-point-8\";s:1:\"0\";s:9:\"latitude8\";s:0:\"\";s:10:\"longitude8\";s:0:\"\";s:9:\"map-info8\";s:0:\"\";s:11:\"map-point-9\";s:1:\"0\";s:9:\"latitude9\";s:0:\"\";s:10:\"longitude9\";s:0:\"\";s:9:\"map-info9\";s:0:\"\";s:12:\"map-point-10\";s:1:\"0\";s:10:\"latitude10\";s:0:\"\";s:11:\"longitude10\";s:0:\"\";s:10:\"map-info10\";s:0:\"\";s:20:\"add-remove-locations\";s:0:\"\";s:13:\"map-greyscale\";s:1:\"0\";s:9:\"map-color\";s:0:\"\";s:14:\"map-ultra-flat\";s:0:\"\";s:21:\"map-dark-color-scheme\";s:0:\"\";s:24:\"slider-caption-animation\";s:1:\"1\";s:23:\"slider-background-cover\";s:1:\"1\";s:15:\"slider-autoplay\";s:1:\"1\";s:20:\"slider-advance-speed\";s:0:\"\";s:22:\"slider-animation-speed\";s:0:\"\";s:13:\"slider-height\";s:0:\"\";s:15:\"slider-bg-color\";s:7:\"#000000\";}', 'yes'),
(147, 'salient_redux-transients', 'a:3:{s:14:\"changed_values\";a:2:{s:28:\"main_portfolio_project_style\";s:1:\"1\";s:20:\"portfolio_single_nav\";s:13:\"after_project\";}s:9:\"last_save\";i:1557915281;s:13:\"last_compiler\";i:1557903502;}', 'yes'),
(151, 'woocommerce_store_address', '', 'yes'),
(152, 'woocommerce_store_address_2', '', 'yes'),
(153, 'woocommerce_store_city', '', 'yes'),
(154, 'woocommerce_default_country', 'GB', 'yes'),
(155, 'woocommerce_store_postcode', '', 'yes'),
(156, 'woocommerce_allowed_countries', 'all', 'yes'),
(157, 'woocommerce_all_except_countries', '', 'yes'),
(158, 'woocommerce_specific_allowed_countries', '', 'yes'),
(159, 'woocommerce_ship_to_countries', '', 'yes'),
(160, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(161, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(162, 'woocommerce_calc_taxes', 'no', 'yes'),
(163, 'woocommerce_enable_coupons', 'yes', 'yes'),
(164, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(165, 'woocommerce_currency', 'GBP', 'yes'),
(166, 'woocommerce_currency_pos', 'left', 'yes'),
(167, 'woocommerce_price_thousand_sep', ',', 'yes'),
(168, 'woocommerce_price_decimal_sep', '.', 'yes'),
(169, 'woocommerce_price_num_decimals', '2', 'yes'),
(170, 'woocommerce_shop_page_id', '', 'yes'),
(171, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(172, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(173, 'woocommerce_placeholder_image', '5', 'yes'),
(174, 'woocommerce_weight_unit', 'kg', 'yes'),
(175, 'woocommerce_dimension_unit', 'cm', 'yes'),
(176, 'woocommerce_enable_reviews', 'yes', 'yes'),
(177, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(178, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(179, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(180, 'woocommerce_review_rating_required', 'yes', 'no'),
(181, 'woocommerce_manage_stock', 'yes', 'yes'),
(182, 'woocommerce_hold_stock_minutes', '60', 'no'),
(183, 'woocommerce_notify_low_stock', 'yes', 'no'),
(184, 'woocommerce_notify_no_stock', 'yes', 'no'),
(185, 'woocommerce_stock_email_recipient', 'damien.doonan@publicis.com.au', 'no'),
(186, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(187, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(188, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(189, 'woocommerce_stock_format', '', 'yes'),
(190, 'woocommerce_file_download_method', 'force', 'no'),
(191, 'woocommerce_downloads_require_login', 'no', 'no'),
(192, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(193, 'woocommerce_prices_include_tax', 'no', 'yes'),
(194, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(195, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(196, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(197, 'woocommerce_tax_classes', 'Reduced rate\nZero rate', 'yes'),
(198, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(199, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(200, 'woocommerce_price_display_suffix', '', 'yes'),
(201, 'woocommerce_tax_total_display', 'itemized', 'no'),
(202, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(203, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(204, 'woocommerce_ship_to_destination', 'billing', 'no'),
(205, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(206, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(207, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(208, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(209, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(210, 'woocommerce_registration_generate_username', 'yes', 'no'),
(211, 'woocommerce_registration_generate_password', 'yes', 'no'),
(212, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(213, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(214, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(215, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(216, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(217, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(218, 'woocommerce_trash_pending_orders', '', 'no'),
(219, 'woocommerce_trash_failed_orders', '', 'no'),
(220, 'woocommerce_trash_cancelled_orders', '', 'no'),
(221, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(222, 'woocommerce_email_from_name', 'LastSeen', 'no'),
(223, 'woocommerce_email_from_address', 'damien.doonan@publicis.com.au', 'no'),
(224, 'woocommerce_email_header_image', '', 'no'),
(225, 'woocommerce_email_footer_text', '{site_title}<br/>Built with <a href=\"https://woocommerce.com/\">WooCommerce</a>', 'no'),
(226, 'woocommerce_email_base_color', '#96588a', 'no'),
(227, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(228, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(229, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(230, 'woocommerce_cart_page_id', '', 'yes'),
(231, 'woocommerce_checkout_page_id', '', 'yes'),
(232, 'woocommerce_myaccount_page_id', '', 'yes'),
(233, 'woocommerce_terms_page_id', '', 'no'),
(234, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(235, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(236, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(237, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(238, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(239, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(240, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(241, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(242, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(243, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(244, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(245, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(246, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(247, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(248, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(249, 'woocommerce_api_enabled', 'no', 'yes'),
(250, 'woocommerce_allow_tracking', 'no', 'no'),
(251, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(252, 'woocommerce_single_image_width', '600', 'yes'),
(253, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(254, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(255, 'woocommerce_demo_store', 'no', 'no'),
(256, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(257, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(258, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(259, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(261, 'default_product_cat', '15', 'yes'),
(264, 'woocommerce_version', '3.6.2', 'yes'),
(265, 'woocommerce_db_version', '3.6.2', 'yes'),
(266, 'recently_activated', 'a:0:{}', 'yes'),
(267, 'woocommerce_admin_notices', 'a:2:{i:0;s:7:\"install\";i:1;s:20:\"no_secure_connection\";}', 'yes'),
(270, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(271, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(279, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(281, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(282, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(283, 'vc_version', '5.7', 'yes'),
(286, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(290, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(291, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"1\";s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(298, 'wbc_import_progress', 'a:3:{s:10:\"total_post\";i:115;s:14:\"imported_count\";i:115;s:9:\"remaining\";i:0;}', 'yes'),
(299, '_transient_product_query-transient-version', '1558564467', 'yes'),
(300, 'category_children', 'a:0:{}', 'yes'),
(302, 'project-attributes_children', 'a:0:{}', 'yes'),
(303, 'slider-locations_children', 'a:0:{}', 'yes'),
(304, 'product_cat_children', 'a:0:{}', 'yes'),
(305, 'wbc_imported_demos', 'a:1:{s:13:\"wbc-import-26\";a:6:{s:9:\"directory\";s:11:\"Simple Blog\";s:7:\"widgets\";s:11:\"widgets.txt\";s:12:\"content_file\";s:11:\"content.xml\";s:13:\"theme_options\";s:17:\"theme-options.txt\";s:5:\"image\";s:16:\"screen-image.jpg\";s:8:\"imported\";s:8:\"imported\";}}', 'yes'),
(324, '_transient_timeout_wc_low_stock_count', '1560395241', 'no'),
(325, '_transient_wc_low_stock_count', '0', 'no'),
(326, '_transient_timeout_wc_outofstock_count', '1560395241', 'no'),
(327, '_transient_wc_outofstock_count', '0', 'no'),
(400, '_transient_timeout_wc_term_counts', '1560402780', 'no'),
(401, '_transient_wc_term_counts', 'a:1:{i:15;s:0:\"\";}', 'no'),
(430, 'WPLANG', 'en_AU', 'yes'),
(431, 'new_admin_email', 'damien.doonan@publicis.com.au', 'yes'),
(566, '_transient_health-check-site-status-result', '{\"good\":\"11\",\"recommended\":\"4\",\"critical\":\"1\"}', 'yes'),
(568, 'widget_recent-posts-extra', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(569, 'widget_recent-projects', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(572, 'woocommerce_maybe_regenerate_images_hash', 'f295711100eef51cf7195e603d0739ad', 'yes'),
(595, 'project-type_children', 'a:0:{}', 'yes'),
(717, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(733, 'bodhi_svgs_plugin_version', '2.3.15', 'yes'),
(799, '_transient_timeout_external_ip_address_::1', '1559096517', 'no'),
(800, '_transient_external_ip_address_::1', '203.28.166.10', 'no'),
(816, '_site_transient_timeout_browser_6eaaf991d59f1ada56000d97adcf5bdd', '1559169266', 'no'),
(817, '_site_transient_browser_6eaaf991d59f1ada56000d97adcf5bdd', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"74.0.3729.157\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(818, '_site_transient_timeout_php_check_90e738eca301c4d89366b1a4d15fe37f', '1559169267', 'no'),
(819, '_site_transient_php_check_90e738eca301c4d89366b1a4d15fe37f', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(838, '_site_transient_timeout_theme_roots', '1558929148', 'no'),
(839, '_site_transient_theme_roots', 'a:4:{s:8:\"LastSeen\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(841, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_AU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.1-partial-0.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:3:\"5.2\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.1-partial-0.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-rollback-0.zip\";}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:3:\"5.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1558927352;s:15:\"version_checked\";s:3:\"5.2\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-18 01:07:42\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/en_AU.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(842, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1558927353;s:7:\"checked\";a:4:{s:8:\"LastSeen\";s:6:\"10.0.4\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:14:\"twentynineteen\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"1.4\";s:7:\"updated\";s:19:\"2019-05-11 03:19:15\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/theme/twentynineteen/1.4/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:15:\"twentyseventeen\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"2.2\";s:7:\"updated\";s:19:\"2019-01-14 00:38:55\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/theme/twentyseventeen/2.2/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:13:\"twentysixteen\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"2.0\";s:7:\"updated\";s:19:\"2018-12-07 06:30:22\";s:7:\"package\";s:77:\"https://downloads.wordpress.org/translation/theme/twentysixteen/2.0/en_AU.zip\";s:10:\"autoupdate\";b:1;}}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(843, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1558927354;s:7:\"checked\";a:5:{s:19:\"akismet/akismet.php\";s:5:\"4.1.1\";s:9:\"hello.php\";s:5:\"1.7.2\";s:35:\"js_composer_salient/js_composer.php\";s:3:\"5.7\";s:27:\"svg-support/svg-support.php\";s:6:\"2.3.15\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.6.2\";}s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.6.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.6.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:4:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.1.1\";s:7:\"updated\";s:19:\"2018-11-12 20:00:17\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/translation/plugin/akismet/4.1.1/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2018-03-20 09:37:00\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"svg-support\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:6:\"2.3.10\";s:7:\"updated\";s:19:\"2017-08-31 05:49:41\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/svg-support/2.3.10/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"3.6.2\";s:7:\"updated\";s:19:\"2019-04-25 00:40:22\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.6.2/en_AU.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:2:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"svg-support/svg-support.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/svg-support\";s:4:\"slug\";s:11:\"svg-support\";s:6:\"plugin\";s:27:\"svg-support/svg-support.php\";s:11:\"new_version\";s:6:\"2.3.15\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/svg-support/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/svg-support.2.3.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:64:\"https://ps.w.org/svg-support/assets/icon-256x256.png?rev=1417738\";s:2:\"1x\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";s:3:\"svg\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/svg-support/assets/banner-1544x500.jpg?rev=1215377\";s:2:\"1x\";s:66:\"https://ps.w.org/svg-support/assets/banner-772x250.jpg?rev=1215377\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', 'woocommerce-placeholder.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:21:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-900x604.png\";s:5:\"width\";i:900;s:6:\"height\";i:604;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x403.png\";s:5:\"width\";i:600;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-400x269.png\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-140x140.png\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"wide\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-1000x500.png\";s:5:\"width\";i:1000;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-670x335.png\";s:5:\"width\";i:670;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-350x350.png\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-500x1000.png\";s:5:\"width\";i:500;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"wide_tall\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1000x1000.png\";s:5:\"width\";i:1000;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"wide_photography\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-900x600.png\";s:5:\"width\";i:900;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"large_featured\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-1200x700.png\";s:5:\"width\";i:1200;s:6:\"height\";i:700;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"medium_featured\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-800x800.png\";s:5:\"width\";i:800;s:6:\"height\";i:800;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 6, '_edit_lock', '1557914988:1'),
(6, 6, '_edit_last', '1'),
(7, 7, '_menu_item_type', 'custom'),
(8, 7, '_menu_item_menu_item_parent', '0'),
(9, 7, '_menu_item_object_id', '7'),
(10, 7, '_menu_item_object', 'custom'),
(11, 7, '_menu_item_target', ''),
(12, 7, '_menu_item_classes', 'a:2:{i:0;s:8:\"megamenu\";i:1;s:9:\"columns-4\";}'),
(13, 7, '_menu_item_xfn', ''),
(14, 7, '_menu_item_url', 'http://themenectar.com/demo/salient-blog/features/elements/'),
(15, 8, '_menu_item_type', 'custom'),
(16, 8, '_menu_item_menu_item_parent', '7'),
(17, 8, '_menu_item_object_id', '8'),
(18, 8, '_menu_item_object', 'custom'),
(19, 8, '_menu_item_target', ''),
(20, 8, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(21, 8, '_menu_item_xfn', ''),
(22, 8, '_menu_item_url', '#'),
(23, 9, '_menu_item_type', 'custom'),
(24, 9, '_menu_item_menu_item_parent', '7'),
(25, 9, '_menu_item_object_id', '9'),
(26, 9, '_menu_item_object', 'custom'),
(27, 9, '_menu_item_target', ''),
(28, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(29, 9, '_menu_item_xfn', ''),
(30, 9, '_menu_item_url', '#'),
(31, 10, '_menu_item_type', 'custom'),
(32, 10, '_menu_item_menu_item_parent', '8'),
(33, 10, '_menu_item_object_id', '10'),
(34, 10, '_menu_item_object', 'custom'),
(35, 10, '_menu_item_target', '_blank'),
(36, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(37, 10, '_menu_item_xfn', ''),
(38, 10, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/home-basic/'),
(39, 11, '_menu_item_type', 'custom'),
(40, 11, '_menu_item_menu_item_parent', '8'),
(41, 11, '_menu_item_object_id', '11'),
(42, 11, '_menu_item_object', 'custom'),
(43, 11, '_menu_item_target', '_blank'),
(44, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(45, 11, '_menu_item_xfn', ''),
(46, 11, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/home-portfolio-eye-candy-2/'),
(47, 12, '_menu_item_type', 'custom'),
(48, 12, '_menu_item_menu_item_parent', '8'),
(49, 12, '_menu_item_object_id', '12'),
(50, 12, '_menu_item_object', 'custom'),
(51, 12, '_menu_item_target', '_blank'),
(52, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(53, 12, '_menu_item_xfn', ''),
(54, 12, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/portfolio/geowolf-social/'),
(55, 13, '_menu_item_type', 'custom'),
(56, 13, '_menu_item_menu_item_parent', '7'),
(57, 13, '_menu_item_object_id', '13'),
(58, 13, '_menu_item_object', 'custom'),
(59, 13, '_menu_item_target', ''),
(60, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(61, 13, '_menu_item_xfn', ''),
(62, 13, '_menu_item_url', '#'),
(63, 14, '_menu_item_type', 'custom'),
(64, 14, '_menu_item_menu_item_parent', '9'),
(65, 14, '_menu_item_object_id', '14'),
(66, 14, '_menu_item_object', 'custom'),
(67, 14, '_menu_item_target', ''),
(68, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(69, 14, '_menu_item_xfn', ''),
(70, 14, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/portfolio/mobile-weather-app/'),
(71, 15, '_menu_item_type', 'custom'),
(72, 15, '_menu_item_menu_item_parent', '9'),
(73, 15, '_menu_item_object_id', '15'),
(74, 15, '_menu_item_object', 'custom'),
(75, 15, '_menu_item_target', ''),
(76, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(77, 15, '_menu_item_xfn', ''),
(78, 15, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/show-off-your-work/'),
(79, 16, '_menu_item_type', 'custom'),
(80, 16, '_menu_item_menu_item_parent', '8'),
(81, 16, '_menu_item_object_id', '16'),
(82, 16, '_menu_item_object', 'custom'),
(83, 16, '_menu_item_target', ''),
(84, 16, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(85, 16, '_menu_item_xfn', ''),
(86, 16, '_menu_item_url', '#'),
(87, 17, '_menu_item_type', 'custom'),
(88, 17, '_menu_item_menu_item_parent', '0'),
(89, 17, '_menu_item_object_id', '17'),
(90, 17, '_menu_item_object', 'custom'),
(91, 17, '_menu_item_target', ''),
(92, 17, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(93, 17, '_menu_item_xfn', ''),
(94, 17, '_menu_item_url', '#'),
(95, 18, '_menu_item_type', 'custom'),
(96, 18, '_menu_item_menu_item_parent', '17'),
(97, 18, '_menu_item_object_id', '18'),
(98, 18, '_menu_item_object', 'custom'),
(99, 18, '_menu_item_target', ''),
(100, 18, '_menu_item_classes', 'a:1:{i:0;s:8:\"no-ajaxy\";}'),
(101, 18, '_menu_item_xfn', ''),
(102, 18, '_menu_item_url', 'http://themenectar.com/demo/salient'),
(103, 19, '_menu_item_type', 'custom'),
(104, 19, '_menu_item_menu_item_parent', '17'),
(105, 19, '_menu_item_object_id', '19'),
(106, 19, '_menu_item_object', 'custom'),
(107, 19, '_menu_item_target', ''),
(108, 19, '_menu_item_classes', 'a:1:{i:0;s:8:\"no-ajaxy\";}'),
(109, 19, '_menu_item_xfn', ''),
(110, 19, '_menu_item_url', 'http://themenectar.com/demo/salient-frostwave'),
(111, 20, '_menu_item_type', 'custom'),
(112, 20, '_menu_item_menu_item_parent', '0'),
(113, 20, '_menu_item_object_id', '20'),
(114, 20, '_menu_item_object', 'custom'),
(115, 20, '_menu_item_target', ''),
(116, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(117, 20, '_menu_item_xfn', ''),
(118, 20, '_menu_item_url', 'http://themenectar.com/demo/salient-blog/blog-masonry-fullwidth/'),
(119, 21, '_menu_item_type', 'custom'),
(120, 21, '_menu_item_menu_item_parent', '22'),
(121, 21, '_menu_item_object_id', '21'),
(122, 21, '_menu_item_object', 'custom'),
(123, 21, '_menu_item_target', ''),
(124, 21, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(125, 21, '_menu_item_xfn', ''),
(126, 21, '_menu_item_url', 'http://themenectar.com/demo/salient-blog/shop/?sidebar=true'),
(127, 22, '_menu_item_type', 'custom'),
(128, 22, '_menu_item_menu_item_parent', '0'),
(129, 22, '_menu_item_object_id', '22'),
(130, 22, '_menu_item_object', 'custom'),
(131, 22, '_menu_item_target', ''),
(132, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(133, 22, '_menu_item_xfn', ''),
(134, 22, '_menu_item_url', 'http://themenectar.com/demo/salient-blog/shop/?sidebar=true'),
(135, 23, '_menu_item_type', 'custom'),
(136, 23, '_menu_item_menu_item_parent', '17'),
(137, 23, '_menu_item_object_id', '23'),
(138, 23, '_menu_item_object', 'custom'),
(139, 23, '_menu_item_target', ''),
(140, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(141, 23, '_menu_item_xfn', ''),
(142, 23, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend'),
(143, 24, '_menu_item_type', 'custom'),
(144, 24, '_menu_item_menu_item_parent', '20'),
(145, 24, '_menu_item_object_id', '24'),
(146, 24, '_menu_item_object', 'custom'),
(147, 24, '_menu_item_target', '_blank'),
(148, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(149, 24, '_menu_item_xfn', ''),
(150, 24, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/blog-masonry-fullwidth/'),
(151, 25, '_menu_item_type', 'custom'),
(152, 25, '_menu_item_menu_item_parent', '20'),
(153, 25, '_menu_item_object_id', '25'),
(154, 25, '_menu_item_object', 'custom'),
(155, 25, '_menu_item_target', '_blank'),
(156, 25, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(157, 25, '_menu_item_xfn', ''),
(158, 25, '_menu_item_url', 'http://themenectar.com/demo/salient-ascend/blog-masonry-sidebar/'),
(159, 26, '_menu_item_type', 'custom'),
(160, 26, '_menu_item_menu_item_parent', '17'),
(161, 26, '_menu_item_object_id', '26'),
(162, 26, '_menu_item_object', 'custom'),
(163, 26, '_menu_item_target', ''),
(164, 26, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(165, 26, '_menu_item_xfn', ''),
(166, 26, '_menu_item_url', 'http://themenectar.com/demo/salient-one-page/'),
(167, 27, '_menu_item_type', 'custom'),
(168, 27, '_menu_item_menu_item_parent', '17'),
(169, 27, '_menu_item_object_id', '27'),
(170, 27, '_menu_item_object', 'custom'),
(171, 27, '_menu_item_target', ''),
(172, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(173, 27, '_menu_item_xfn', ''),
(174, 27, '_menu_item_url', 'http://themenectar.com/demo/salient-ecommerce'),
(175, 28, '_menu_item_type', 'custom'),
(176, 28, '_menu_item_menu_item_parent', '17'),
(177, 28, '_menu_item_object_id', '28'),
(178, 28, '_menu_item_object', 'custom'),
(179, 28, '_menu_item_target', ''),
(180, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(181, 28, '_menu_item_xfn', ''),
(182, 28, '_menu_item_url', '#'),
(183, 29, '_wp_page_template', 'default'),
(184, 29, '_nectar_header_bg', ''),
(185, 29, '_nectar_header_title', ''),
(186, 29, '_nectar_header_subtitle', ''),
(187, 29, '_aioseop_title', 'Salient | Blog'),
(188, 30, '_wp_page_template', 'default'),
(189, 30, '_nectar_header_bg', ''),
(190, 30, '_nectar_header_title', ''),
(191, 30, '_nectar_header_subtitle', ''),
(192, 30, '_aioseop_title', 'Salient | Features'),
(193, 197, '_nectar_header_bg', ''),
(194, 197, '_nectar_header_title', 'Sidebar page'),
(195, 197, '_nectar_header_subtitle', 'consectetur adipiscing elit quisque'),
(196, 197, '_wp_page_template', 'page-sidebar.php'),
(197, 197, '_aioseop_title', 'Salient | Features - Sidebar'),
(198, 205, '_nectar_header_bg', ''),
(199, 205, '_nectar_header_title', ''),
(200, 205, '_nectar_header_subtitle', ''),
(201, 205, '_wp_page_template', 'default'),
(202, 205, '_aioseop_title', 'Salient | Features - Columns'),
(203, 205, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(204, 205, 'nectar-metabox-portfolio-display-sortable', 'off'),
(205, 205, '_nectar_header_parallax', 'off'),
(206, 205, '_nectar_header_bg_height', ''),
(207, 205, '_wpb_vc_js_status', 'true'),
(208, 205, '_wpb_vc_js_interface_version', '2'),
(209, 205, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(210, 207, '_nectar_header_title', 'Stunning Headers'),
(211, 207, '_nectar_header_bg', 'http://themenectar.com/demo/salient-frostwave/wp-content/uploads/2013/06/about-bg.jpg'),
(212, 207, '_nectar_header_subtitle', 'With optional parallax effect on any page'),
(213, 207, '_wp_page_template', 'default'),
(214, 207, '_aioseop_title', 'Salient | Features - Elements'),
(215, 207, '_nectar_header_bg_height', '400'),
(216, 207, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(217, 207, 'nectar-metabox-portfolio-display-sortable', 'off'),
(218, 207, '_nectar_header_parallax', 'on'),
(219, 207, '_wpb_vc_js_status', 'true'),
(220, 207, '_wpb_vc_js_interface_version', '2'),
(221, 207, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(222, 207, '_nectar_page_header_alignment', 'center'),
(223, 207, '_nectar_header_bg_color', ''),
(224, 207, '_nectar_header_font_color', ''),
(225, 207, '_disable_transparent_header', 'off'),
(226, 207, '_force_transparent_header', 'off'),
(227, 482, '_nectar_header_bg', 'http://themenectar.com/demo/salient/wp-content/uploads/2013/04/pricing-tables.jpg'),
(228, 482, '_nectar_header_bg_height', '400'),
(229, 482, '_nectar_header_title', ' Pricing Tables'),
(230, 482, '_nectar_header_subtitle', 'Explore all of our awesome plans '),
(231, 482, '_wp_page_template', 'default'),
(232, 482, '_wpb_vc_js_status', 'true'),
(233, 482, '_wpb_vc_js_interface_version', '2'),
(234, 482, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(235, 482, 'nectar-metabox-portfolio-display-sortable', 'off'),
(236, 482, '_nectar_header_parallax', 'off'),
(237, 482, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(238, 482, '_nectar_page_header_alignment', 'left'),
(239, 482, '_nectar_header_bg_color', ''),
(240, 482, '_nectar_header_font_color', ''),
(241, 551, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(242, 551, 'nectar-metabox-portfolio-display-sortable', 'off'),
(243, 551, '_nectar_header_bg', ''),
(244, 551, '_nectar_header_bg_height', ''),
(245, 551, '_nectar_header_title', ''),
(246, 551, '_nectar_header_subtitle', ''),
(247, 551, '_wp_page_template', 'default'),
(248, 551, '_nectar_header_parallax', 'off'),
(249, 551, '_wpb_vc_js_status', 'true'),
(250, 551, '_wpb_vc_js_interface_version', '2'),
(251, 551, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(252, 551, '_nectar_page_header_alignment', 'left'),
(253, 551, '_nectar_header_bg_color', ''),
(254, 551, '_nectar_header_font_color', ''),
(255, 551, '_disable_transparent_header', 'off'),
(256, 551, '_force_transparent_header', 'off'),
(257, 568, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(258, 568, 'nectar-metabox-portfolio-display-sortable', 'off'),
(259, 568, '_nectar_header_bg', 'http://themenectar.com/demo/salient-frostwave/wp-content/uploads/2013/07/header3.jpg'),
(260, 568, '_nectar_header_parallax', 'off'),
(261, 568, '_nectar_header_bg_height', '400'),
(262, 568, '_nectar_header_title', 'Header Options'),
(263, 568, '_nectar_header_subtitle', 'the power & design you\'ve been craving'),
(264, 568, '_wp_page_template', 'default'),
(265, 568, '_wpb_vc_js_status', 'true'),
(266, 568, '_wpb_vc_js_interface_version', '0'),
(267, 568, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(268, 568, '_nectar_page_header_alignment', 'center'),
(269, 568, '_nectar_header_bg_color', ''),
(270, 568, '_nectar_header_font_color', ''),
(271, 596, '_wp_page_template', 'default'),
(272, 596, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(273, 596, 'nectar-metabox-portfolio-display-sortable', 'off'),
(274, 596, '_nectar_header_bg', ''),
(275, 596, '_nectar_header_parallax', 'off'),
(276, 596, '_nectar_header_bg_height', ''),
(277, 596, '_nectar_header_title', ''),
(278, 596, '_nectar_header_subtitle', ''),
(279, 596, '_wpb_vc_js_status', 'true'),
(280, 596, '_wpb_vc_js_interface_version', '2'),
(281, 596, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(282, 596, '_nectar_page_header_alignment', 'left'),
(283, 596, '_nectar_header_bg_color', ''),
(284, 596, '_nectar_header_font_color', ''),
(285, 596, '_disable_transparent_header', 'off'),
(286, 596, '_force_transparent_header', 'on'),
(287, 615, '_nectar_slider_bg_type', 'image_bg'),
(288, 615, '_nectar_slider_image', 'http://themenectar.com/demo/salient/wp-content/uploads/2013/09/black-bg1.png'),
(289, 615, '_nectar_media_upload_webm', ''),
(290, 615, '_nectar_media_upload_mp4', ''),
(291, 615, '_nectar_media_upload_ogv', ''),
(292, 615, '_nectar_slider_preview_image', ''),
(293, 615, '_nectar_slider_video_texture', 'off'),
(294, 615, '_nectar_slider_slide_bg_alignment', 'center'),
(295, 615, '_nectar_slider_slide_font_color', 'light'),
(296, 615, '_nectar_slider_heading', 'Simplicity.'),
(297, 615, '_nectar_slider_caption', 'Build your online identity intuitively'),
(298, 615, '_nectar_slider_link_type', 'button_links'),
(299, 615, '_nectar_slider_button', 'Read Our Blog'),
(300, 615, '_nectar_slider_button_url', 'http://themenectar.com/demo/salient-blog/portfolio-fullwidth-masonry/'),
(301, 615, '_nectar_slider_button_style', 'solid_color_2'),
(302, 615, '_nectar_slider_button_color', 'primary-color'),
(303, 615, '_nectar_slider_button_2', ''),
(304, 615, '_nectar_slider_button_url_2', ''),
(305, 615, '_nectar_slider_button_style_2', 'solid_color'),
(306, 615, '_nectar_slider_button_color_2', 'extra-color-3'),
(307, 615, '_nectar_slider_entire_link', ''),
(308, 615, '_nectar_slider_video_popup', ''),
(309, 615, '_nectar_slide_xpos_alignment', 'centered'),
(310, 615, '_nectar_slide_ypos_alignment', 'middle'),
(311, 615, '_nectar_slider_slide_custom_class', ''),
(312, 615, '_nectar_slider_caption_background', 'off'),
(313, 615, '_nectar_slider_down_arrow', 'off'),
(314, 615, '_nectar_love', '0'),
(315, 617, '_nectar_slider_bg_type', 'image_bg'),
(316, 617, '_nectar_slider_image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2015/02/blank-white.jpg'),
(317, 617, '_nectar_media_upload_webm', ''),
(318, 617, '_nectar_media_upload_mp4', ''),
(319, 617, '_nectar_media_upload_ogv', ''),
(320, 617, '_nectar_slider_preview_image', ''),
(321, 617, '_nectar_slider_video_texture', 'off'),
(322, 617, '_nectar_slider_slide_bg_alignment', 'center'),
(323, 617, '_nectar_slider_slide_font_color', 'dark'),
(324, 617, '_nectar_slider_heading', 'Experience.'),
(325, 617, '_nectar_slider_caption', 'Real creative freedom and start standing out\n '),
(326, 617, '_nectar_slider_link_type', 'button_links'),
(327, 617, '_nectar_slider_button', 'Learn More Now'),
(328, 617, '_nectar_slider_button_url', 'http://themenectar.com/demo/salient-blog/about/about-me-extended/'),
(329, 617, '_nectar_slider_button_style', 'solid_color_2'),
(330, 617, '_nectar_slider_button_color', 'extra-color-2'),
(331, 617, '_nectar_slider_button_2', ''),
(332, 617, '_nectar_slider_button_url_2', ''),
(333, 617, '_nectar_slider_button_style_2', 'transparent_2'),
(334, 617, '_nectar_slider_button_color_2', 'extra-color-3'),
(335, 617, '_nectar_slider_entire_link', 'http://google.com'),
(336, 617, '_nectar_slider_video_popup', ''),
(337, 617, '_nectar_slide_xpos_alignment', 'centered'),
(338, 617, '_nectar_slide_ypos_alignment', 'middle'),
(339, 617, '_nectar_slider_slide_custom_class', ''),
(340, 617, '_nectar_slider_caption_background', 'off'),
(341, 617, '_nectar_slider_down_arrow', 'off'),
(490, 701, '_nectar_slider_bg_type', 'image_bg'),
(491, 701, '_nectar_slider_image', 'http://themenectar.com/demo/salient/wp-content/uploads/2013/09/black-bg1.png'),
(492, 701, '_nectar_media_upload_webm', ''),
(493, 701, '_nectar_media_upload_mp4', ''),
(494, 701, '_nectar_media_upload_ogv', ''),
(495, 701, '_nectar_slider_preview_image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2015/02/forest-preview.jpg'),
(496, 701, '_nectar_slider_video_texture', 'off'),
(497, 701, '_nectar_slider_slide_bg_alignment', 'center'),
(498, 701, '_nectar_slider_slide_font_color', 'light'),
(499, 701, '_nectar_slider_heading', 'Perfection.'),
(500, 701, '_nectar_slider_caption', 'An excetionally high attention to details'),
(501, 701, '_nectar_slider_caption_background', 'off'),
(502, 701, '_nectar_slider_link_type', 'button_links'),
(503, 701, '_nectar_slider_button', 'Learn More Now'),
(504, 701, '_nectar_slider_button_url', ''),
(505, 701, '_nectar_slider_button_style', 'solid_color_2'),
(506, 701, '_nectar_slider_button_color', 'primary-color'),
(507, 701, '_nectar_slider_button_2', ''),
(508, 701, '_nectar_slider_button_url_2', ''),
(509, 701, '_nectar_slider_button_style_2', 'solid_color'),
(510, 701, '_nectar_slider_button_color_2', 'primary-color'),
(511, 701, '_nectar_slider_entire_link', ''),
(512, 701, '_nectar_slider_video_popup', ''),
(513, 701, '_nectar_slide_xpos_alignment', 'centered'),
(514, 701, '_nectar_slide_ypos_alignment', 'middle'),
(515, 701, '_nectar_slider_slide_custom_class', ''),
(516, 701, '_nectar_love', '0'),
(517, 701, '_nectar_slider_down_arrow', 'off'),
(743, 764, '_wp_page_template', 'default'),
(744, 764, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(745, 764, 'nectar-metabox-portfolio-display-sortable', 'off'),
(746, 764, '_nectar_header_bg', ''),
(747, 764, '_nectar_header_parallax', 'off'),
(748, 764, '_nectar_header_bg_height', ''),
(749, 764, '_nectar_header_title', ''),
(750, 764, '_nectar_header_subtitle', ''),
(751, 764, '_wpb_vc_js_status', 'true'),
(752, 764, '_wpb_vc_js_interface_version', '0'),
(753, 764, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(754, 764, '_nectar_page_header_alignment', 'left'),
(755, 764, '_nectar_header_bg_color', ''),
(756, 764, '_nectar_header_font_color', ''),
(757, 767, '_wp_page_template', 'default'),
(758, 767, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(759, 767, 'nectar-metabox-portfolio-display-sortable', 'off'),
(760, 767, '_nectar_header_bg', ''),
(761, 767, '_nectar_header_parallax', 'off'),
(762, 767, '_nectar_header_bg_height', ''),
(763, 767, '_nectar_header_title', ''),
(764, 767, '_nectar_header_subtitle', ''),
(765, 767, '_wpb_vc_js_status', 'true'),
(766, 767, '_wpb_vc_js_interface_version', '0'),
(767, 767, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(768, 767, '_nectar_page_header_alignment', 'left'),
(769, 767, '_nectar_header_bg_color', ''),
(770, 767, '_nectar_header_font_color', ''),
(771, 767, '_disable_transparent_header', 'off'),
(772, 767, '_force_transparent_header', 'off'),
(773, 770, '_wp_page_template', 'default'),
(774, 770, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(775, 770, 'nectar-metabox-portfolio-display-sortable', 'off'),
(776, 770, '_nectar_header_bg', ''),
(777, 770, '_nectar_header_parallax', 'off'),
(778, 770, '_nectar_header_bg_height', ''),
(779, 770, '_nectar_header_title', ''),
(780, 770, '_nectar_header_subtitle', ''),
(781, 770, '_wpb_vc_js_status', 'true'),
(782, 770, '_wpb_vc_js_interface_version', '0'),
(783, 770, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(784, 770, '_nectar_page_header_alignment', 'left'),
(785, 770, '_nectar_header_bg_color', ''),
(786, 770, '_nectar_header_font_color', ''),
(787, 770, '_disable_transparent_header', 'off'),
(788, 770, '_force_transparent_header', 'off'),
(789, 965, '_wp_page_template', 'default'),
(790, 965, '_wpb_vc_js_status', 'true'),
(791, 965, '_wpb_vc_js_interface_version', '2'),
(792, 965, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(793, 965, 'nectar-metabox-portfolio-display-sortable', 'off'),
(794, 965, '_nectar_header_bg', ''),
(795, 965, '_nectar_header_parallax', 'off'),
(796, 965, '_nectar_header_bg_height', ''),
(797, 965, '_nectar_header_title', ''),
(798, 965, '_nectar_header_subtitle', ''),
(799, 965, 'vc_teaser', 'a:2:{s:4:\"data\";s:115:\"[{\"name\":\"title\",\"link\":\"post\"},{\"name\":\"image\",\"image\":\"featured\",\"link\":\"none\"},{\"name\":\"text\",\"mode\":\"excerpt\"}]\";s:7:\"bgcolor\";s:0:\"\";}'),
(800, 965, '_post_restored_from', 'a:3:{s:20:\"restored_revision_id\";i:1035;s:16:\"restored_by_user\";i:1;s:13:\"restored_time\";i:1390003151;}'),
(801, 965, '_nectar_page_header_alignment', 'left'),
(802, 965, '_nectar_header_bg_color', ''),
(803, 965, '_nectar_header_font_color', ''),
(804, 965, '_disable_transparent_header', 'off'),
(805, 965, '_force_transparent_header', 'off'),
(806, 2034, '_wp_page_template', 'default'),
(807, 2034, '_wpb_vc_js_status', 'true'),
(808, 2034, '_wpb_vc_js_interface_version', '2'),
(809, 2034, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(810, 2034, 'nectar-metabox-portfolio-display-sortable', 'off'),
(811, 2034, '_nectar_header_bg', ''),
(812, 2034, '_nectar_header_parallax', 'on'),
(813, 2034, '_nectar_header_bg_height', '450'),
(814, 2034, '_nectar_header_title', 'Masonry Fullwidth'),
(815, 2034, '_nectar_header_subtitle', 'Posts Fresh Out The Oven'),
(816, 2034, '_nectar_page_header_alignment', 'center'),
(817, 2034, '_nectar_header_bg_color', '#4f4f4f'),
(818, 2034, '_nectar_header_font_color', ''),
(819, 2034, '_disable_transparent_header', 'off'),
(820, 2034, '_force_transparent_header', 'off'),
(821, 2036, '_wp_page_template', 'default'),
(822, 2036, '_wpb_vc_js_status', 'true'),
(823, 2036, '_wpb_vc_js_interface_version', '2'),
(824, 2036, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(825, 2036, 'nectar-metabox-portfolio-display-sortable', 'off'),
(826, 2036, '_nectar_header_bg', ''),
(827, 2036, '_nectar_header_parallax', 'off'),
(828, 2036, '_nectar_header_bg_height', '450'),
(829, 2036, '_nectar_header_title', 'Masonry No Sidebar'),
(830, 2036, '_nectar_header_subtitle', 'Posts Fresh Out The Oven'),
(831, 2036, '_nectar_page_header_alignment', 'center'),
(832, 2036, '_nectar_header_bg_color', '#6b6b6b'),
(833, 2036, '_nectar_header_font_color', ''),
(834, 2036, '_disable_transparent_header', 'off'),
(835, 2036, '_force_transparent_header', 'off'),
(836, 2038, '_wp_page_template', 'default'),
(837, 2038, '_wpb_vc_js_status', 'true'),
(838, 2038, '_wpb_vc_js_interface_version', '2'),
(839, 2038, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(840, 2038, 'nectar-metabox-portfolio-display-sortable', 'off'),
(841, 2038, '_nectar_header_bg', ''),
(842, 2038, '_nectar_header_parallax', 'off'),
(843, 2038, '_nectar_header_bg_height', '450'),
(844, 2038, '_nectar_header_title', 'Masonry Sidebar'),
(845, 2038, '_nectar_header_subtitle', 'Posts Fresh Out The Oven'),
(846, 2038, '_nectar_page_header_alignment', 'left'),
(847, 2038, '_nectar_header_bg_color', '#595959'),
(848, 2038, '_nectar_header_font_color', ''),
(849, 2038, '_disable_transparent_header', 'off'),
(850, 2038, '_force_transparent_header', 'off'),
(851, 2040, '_wp_page_template', 'default'),
(852, 2040, '_wpb_vc_js_status', 'false'),
(853, 2040, '_wpb_vc_js_interface_version', '2'),
(854, 2040, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(855, 2040, 'nectar-metabox-portfolio-display-sortable', 'off'),
(856, 2040, '_nectar_header_bg', ''),
(857, 2040, '_nectar_header_parallax', 'off'),
(858, 2040, '_nectar_header_bg_height', ''),
(859, 2040, '_nectar_header_title', ''),
(860, 2040, '_nectar_header_subtitle', ''),
(861, 2040, '_nectar_page_header_alignment', 'left'),
(862, 2040, '_nectar_header_bg_color', ''),
(863, 2040, '_nectar_header_font_color', ''),
(864, 2092, '_wp_page_template', 'default'),
(865, 2092, '_wpb_vc_js_status', 'true'),
(866, 2092, '_wpb_vc_js_interface_version', '2'),
(867, 2092, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(868, 2092, 'nectar-metabox-portfolio-display-sortable', 'off'),
(869, 2092, '_nectar_header_bg', ''),
(870, 2092, '_nectar_header_parallax', 'off'),
(871, 2092, '_nectar_header_bg_height', '450'),
(872, 2092, '_nectar_header_title', 'Say Hello'),
(873, 2092, '_nectar_header_subtitle', 'We’d love to talk to you!'),
(874, 2092, '_nectar_page_header_alignment', 'left'),
(875, 2092, '_nectar_header_bg_color', '#333333'),
(876, 2092, '_nectar_header_font_color', ''),
(877, 2092, '_disable_transparent_header', 'off'),
(878, 2092, '_force_transparent_header', 'off'),
(879, 2563, '_wp_page_template', 'default'),
(880, 2563, '_wpb_vc_js_status', 'true'),
(881, 2563, '_wpb_vc_js_interface_version', '2'),
(882, 2563, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(883, 2563, 'nectar-metabox-portfolio-display-sortable', 'off'),
(884, 2563, '_nectar_header_bg', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2014/05/latest2.jpg'),
(885, 2563, '_nectar_header_parallax', 'on'),
(886, 2563, '_nectar_header_bg_height', '450'),
(887, 2563, '_nectar_header_title', 'Our News'),
(888, 2563, '_nectar_header_subtitle', 'Fresh Out The Oven'),
(889, 2563, '_nectar_page_header_alignment', 'center'),
(890, 2563, '_nectar_header_bg_color', ''),
(891, 2563, '_nectar_header_font_color', ''),
(892, 2563, '_disable_transparent_header', 'off'),
(893, 2563, '_force_transparent_header', 'off'),
(898, 137, '_nectar_quote', ''),
(899, 137, '_nectar_link', 'http://www.themenectar.com'),
(900, 137, '_nectar_video_m4v', ''),
(901, 137, '_nectar_video_ogv', ''),
(902, 137, '_nectar_video_poster', ''),
(903, 137, '_nectar_video_embed', ''),
(904, 137, '_nectar_audio_mp3', ''),
(905, 137, '_nectar_audio_ogg', ''),
(906, 137, '_nectar_love', '73'),
(907, 137, '_wpb_vc_js_status', 'false'),
(908, 137, '_wpb_vc_js_interface_version', '0'),
(909, 137, '_nectar_gallery_slider', 'off'),
(910, 137, '_post_item_masonry_sizing', 'regular'),
(911, 137, '_nectar_header_bg', ''),
(912, 137, '_nectar_header_parallax', 'off'),
(913, 137, '_nectar_header_bg_color', ''),
(914, 137, '_nectar_header_font_color', ''),
(915, 137, '_nectar_header_overlay', 'off'),
(916, 137, '_nectar_header_bottom_shadow', 'off'),
(917, 137, '_disable_transparent_header', 'off'),
(1148, 82, '_nectar_quote', ''),
(1149, 82, '_nectar_link', ''),
(1150, 82, '_nectar_video_m4v', ''),
(1151, 82, '_nectar_video_ogv', ''),
(1152, 82, '_nectar_video_poster', ''),
(1153, 82, '_nectar_video_embed', ''),
(1154, 82, '_nectar_audio_mp3', ''),
(1155, 82, '_nectar_audio_ogg', ''),
(1156, 82, '_nectar_love', '153'),
(1157, 82, '_nectar_gallery_slider', 'off'),
(1158, 82, '_wpb_vc_js_status', 'false'),
(1159, 82, '_wpb_vc_js_interface_version', '0'),
(1160, 82, '_nectar_header_bg', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2013/03/mountains-preview3.jpg'),
(1161, 82, '_nectar_header_parallax', 'on'),
(1162, 82, '_nectar_header_bg_color', ''),
(1163, 82, '_nectar_header_font_color', ''),
(1164, 82, '_nectar_header_overlay', 'off'),
(1165, 82, '_nectar_header_bottom_shadow', 'off'),
(1166, 82, '_disable_transparent_header', 'off'),
(1167, 82, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2013/03/mountains-preview3.jpg'),
(1168, 82, '_wp_old_slug', 'nulla-fringilla-magna'),
(1169, 82, '_post_item_masonry_sizing', 'regular'),
(1170, 82, '_nectar_header_bg_height', ''),
(1172, 84, '_nectar_quote', ''),
(1173, 84, '_nectar_link', ''),
(1174, 84, '_nectar_video_m4v', 'http://themenectar.com/demo/salient/video/big_buck_bunny.mp4'),
(1175, 84, '_nectar_video_ogv', 'http://themenectar.com/demo/salient/video/big_buck_bunny.ogg'),
(1176, 84, '_nectar_video_poster', 'http://themenectar.com/demo/salient/wp-content/uploads/2013/03/bigBuck.jpg'),
(1177, 84, '_nectar_video_embed', ''),
(1178, 84, '_nectar_audio_mp3', ''),
(1179, 84, '_nectar_audio_ogg', ''),
(1180, 84, '_nectar_love', '971'),
(1181, 84, '_wpb_vc_js_status', 'false'),
(1182, 84, '_wpb_vc_js_interface_version', '0'),
(1183, 84, '_nectar_gallery_slider', 'off'),
(1184, 84, '_nectar_header_bg', ''),
(1185, 84, '_nectar_header_parallax', 'on'),
(1186, 84, '_nectar_header_bg_height', ''),
(1187, 84, '_nectar_header_bg_color', '#353535'),
(1188, 84, '_nectar_header_font_color', ''),
(1189, 84, '_nectar_header_overlay', 'on'),
(1190, 84, '_nectar_header_bottom_shadow', 'on'),
(1191, 84, '_disable_transparent_header', 'off'),
(1192, 84, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2013/03/bird1.jpg'),
(1193, 84, '_wp_old_slug', 'self-hosted-video'),
(1194, 84, '_post_item_masonry_sizing', 'regular'),
(1196, 108, '_nectar_quote', ''),
(1197, 108, '_nectar_link', ''),
(1198, 108, '_nectar_video_m4v', ''),
(1199, 108, '_nectar_video_ogv', ''),
(1200, 108, '_nectar_video_poster', ''),
(1201, 108, '_nectar_video_embed', '<iframe width=\"1280\" height=\"720\" src=\"//www.youtube.com/embed/4BfKFCOCJe8\" frameborder=\"0\" allowfullscreen></iframe>'),
(1202, 108, '_nectar_audio_mp3', ''),
(1203, 108, '_nectar_audio_ogg', ''),
(1204, 108, '_nectar_love', '89'),
(1205, 108, '_nectar_gallery_slider', 'off'),
(1206, 108, '_wpb_vc_js_status', 'false'),
(1207, 108, '_wpb_vc_js_interface_version', '0'),
(1208, 108, '_nectar_header_bg', ''),
(1209, 108, '_nectar_header_parallax', 'on'),
(1210, 108, '_nectar_header_bg_height', ''),
(1211, 108, '_nectar_header_bg_color', '#3a3a3a'),
(1212, 108, '_nectar_header_font_color', ''),
(1213, 108, '_nectar_header_overlay', 'on'),
(1214, 108, '_nectar_header_bottom_shadow', 'on'),
(1215, 108, '_disable_transparent_header', 'off'),
(1216, 108, '_yoast_wpseo_opengraph-description', 'Sed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci. Donec at blandit augue, nec tristique ligula. Donec euismod nulla at erat fringilla.'),
(1217, 108, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2013/01/Comp-1-0000119.png'),
(1218, 108, '_wp_old_slug', 'quisque-sit-amet'),
(1219, 108, '_post_item_masonry_sizing', 'large_featured'),
(1221, 110, '_nectar_quote', ''),
(1222, 110, '_nectar_link', ''),
(1223, 110, '_nectar_video_m4v', ''),
(1224, 110, '_nectar_video_ogv', ''),
(1225, 110, '_nectar_video_poster', ''),
(1226, 110, '_nectar_video_embed', ''),
(1227, 110, '_nectar_audio_mp3', ''),
(1228, 110, '_nectar_audio_ogg', ''),
(1229, 110, '_nectar_love', '3075'),
(1230, 110, '_wpb_vc_js_status', 'false'),
(1231, 110, '_wpb_vc_js_interface_version', '0'),
(1232, 110, '_nectar_gallery_slider', 'off'),
(1233, 110, '_nectar_header_bg', ''),
(1234, 110, '_nectar_header_parallax', 'on'),
(1235, 110, '_nectar_header_bg_height', '380'),
(1236, 110, '_nectar_header_bg_color', '#262626'),
(1237, 110, '_nectar_header_font_color', ''),
(1238, 110, '_nectar_header_overlay', 'on'),
(1239, 110, '_nectar_header_bottom_shadow', 'on'),
(1240, 110, '_disable_transparent_header', 'off'),
(1241, 110, '_wp_old_slug', 'amazing-post-with-all-the-goodies'),
(1242, 110, '_yoast_wpseo_opengraph-description', 'Nulla sed mi leo, sit amet molestie nulla. Phasellus lobortis blandit ipsum, at adipiscing eros porta quis. Phasellus in nisi ipsum, quis dapibus magna'),
(1243, 110, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2013/03/tumblr_n6rzpcsMk41st5lhmo1_12802.jpg'),
(1244, 110, '_post_item_masonry_sizing', 'regular'),
(1246, 152, '_nectar_quote', ''),
(1247, 152, '_nectar_link', ''),
(1248, 152, '_nectar_video_m4v', ''),
(1249, 152, '_nectar_video_ogv', ''),
(1250, 152, '_nectar_video_poster', ''),
(1251, 152, '_nectar_video_embed', '<iframe src=\"//player.vimeo.com/video/51478176?title=0&byline=0\" width=\"500\" height=\"281\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> '),
(1252, 152, '_nectar_audio_mp3', ''),
(1253, 152, '_nectar_audio_ogg', ''),
(1254, 152, '_nectar_love', '107'),
(1255, 152, '_nectar_gallery_slider', 'off'),
(1256, 152, '_wpb_vc_js_status', 'false'),
(1257, 152, '_wpb_vc_js_interface_version', '0'),
(1258, 152, '_nectar_header_bg', ''),
(1259, 152, '_nectar_header_parallax', 'on'),
(1260, 152, '_nectar_header_bg_height', ''),
(1261, 152, '_nectar_header_bg_color', '#2b2b2b'),
(1262, 152, '_nectar_header_font_color', ''),
(1263, 152, '_nectar_header_overlay', 'off'),
(1264, 152, '_nectar_header_bottom_shadow', 'on'),
(1265, 152, '_disable_transparent_header', 'off'),
(1266, 152, '_post_item_masonry_sizing', 'regular'),
(1268, 559, '_nectar_quote', ''),
(1269, 559, '_nectar_link', ''),
(1270, 559, '_nectar_video_m4v', ''),
(1271, 559, '_nectar_video_ogv', ''),
(1272, 559, '_nectar_video_poster', ''),
(1273, 559, '_nectar_video_embed', ''),
(1274, 559, '_nectar_audio_mp3', ''),
(1275, 559, '_nectar_audio_ogg', ''),
(1276, 559, '_nectar_love', '125'),
(1277, 559, '_wpb_vc_js_status', 'false'),
(1278, 559, '_wpb_vc_js_interface_version', '0'),
(1279, 559, '_nectar_gallery_slider', 'off'),
(1280, 559, '_nectar_header_bg', ''),
(1281, 559, '_nectar_header_parallax', 'off'),
(1282, 559, '_nectar_header_bg_height', ''),
(1283, 559, '_nectar_header_bg_color', '#3a3a3a'),
(1284, 559, '_nectar_header_font_color', ''),
(1285, 559, '_nectar_header_overlay', 'on'),
(1286, 559, '_nectar_header_bottom_shadow', 'on'),
(1287, 559, '_disable_transparent_header', 'off'),
(1288, 559, '_post_item_masonry_sizing', 'regular'),
(1290, 1239, '_nectar_love', '52'),
(1291, 1239, '_nectar_gallery_slider', 'off'),
(1292, 1239, '_nectar_quote', ''),
(1293, 1239, '_nectar_link', ''),
(1294, 1239, '_nectar_video_m4v', ''),
(1295, 1239, '_nectar_video_ogv', ''),
(1296, 1239, '_nectar_video_poster', ''),
(1297, 1239, '_nectar_video_embed', ''),
(1298, 1239, '_nectar_audio_mp3', ''),
(1299, 1239, '_nectar_audio_ogg', ''),
(1300, 1239, '_wpb_vc_js_status', 'false'),
(1301, 1239, '_wpb_vc_js_interface_version', '0'),
(1302, 1239, '_nectar_header_bg', ''),
(1303, 1239, '_nectar_header_parallax', 'on'),
(1304, 1239, '_nectar_header_bg_height', ''),
(1305, 1239, '_nectar_header_bg_color', '#3d3d3d'),
(1306, 1239, '_nectar_header_font_color', ''),
(1307, 1239, '_nectar_header_overlay', 'on'),
(1308, 1239, '_nectar_header_bottom_shadow', 'on'),
(1309, 1239, '_disable_transparent_header', 'off'),
(1310, 1239, '_post_item_masonry_sizing', 'regular'),
(1312, 1251, '_nectar_love', '49'),
(1313, 1251, '_nectar_gallery_slider', 'off'),
(1314, 1251, '_nectar_quote', ''),
(1315, 1251, '_nectar_link', ''),
(1316, 1251, '_nectar_video_m4v', ''),
(1317, 1251, '_nectar_video_ogv', ''),
(1318, 1251, '_nectar_video_poster', ''),
(1319, 1251, '_nectar_video_embed', ''),
(1320, 1251, '_nectar_audio_mp3', ''),
(1321, 1251, '_nectar_audio_ogg', ''),
(1322, 1251, '_wp_old_slug', 'facilisis-nulla-eget'),
(1323, 1251, '_wpb_vc_js_status', 'false'),
(1324, 1251, '_wpb_vc_js_interface_version', '0'),
(1325, 1251, '_nectar_header_bg', ''),
(1326, 1251, '_nectar_header_parallax', 'on'),
(1327, 1251, '_nectar_header_bg_height', ''),
(1328, 1251, '_nectar_header_bg_color', '#1c1c1c'),
(1329, 1251, '_nectar_header_font_color', ''),
(1330, 1251, '_nectar_header_overlay', 'on'),
(1331, 1251, '_nectar_header_bottom_shadow', 'on'),
(1332, 1251, '_disable_transparent_header', 'off'),
(1333, 1251, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2014/07/1gnYuBa2.jpg'),
(1334, 1251, '_post_item_masonry_sizing', 'regular'),
(1336, 2326, 'post_second-slide_thumbnail_id', '100'),
(1337, 2326, '_nectar_quote', ''),
(1338, 2326, '_nectar_link', ''),
(1339, 2326, '_nectar_video_m4v', ''),
(1340, 2326, '_nectar_video_ogv', ''),
(1341, 2326, '_nectar_video_poster', ''),
(1342, 2326, '_nectar_video_embed', ''),
(1343, 2326, '_nectar_audio_mp3', ''),
(1344, 2326, '_nectar_audio_ogg', ''),
(1345, 2326, '_nectar_love', '493'),
(1346, 2326, '_nectar_gallery_slider', 'on'),
(1347, 2326, '_wpb_vc_js_status', 'false'),
(1348, 2326, '_wpb_vc_js_interface_version', '0'),
(1349, 2326, '_nectar_header_bg', ''),
(1350, 2326, '_nectar_header_parallax', 'on'),
(1351, 2326, '_nectar_header_bg_height', '400'),
(1352, 2326, '_nectar_header_bg_color', '#3a3a3a'),
(1353, 2326, '_nectar_header_font_color', ''),
(1354, 2326, '_nectar_header_overlay', 'on'),
(1355, 2326, '_nectar_header_bottom_shadow', 'on'),
(1356, 2326, '_disable_transparent_header', 'off'),
(1357, 2326, '_post_item_masonry_sizing', 'wide_tall'),
(1359, 2327, '_nectar_quote', 'Courage is not the absence of fear, but rather the judgment that something else is more important than fear'),
(1360, 2327, '_nectar_link', ''),
(1361, 2327, '_nectar_video_m4v', ''),
(1362, 2327, '_nectar_video_ogv', ''),
(1363, 2327, '_nectar_video_poster', ''),
(1364, 2327, '_nectar_video_embed', ''),
(1365, 2327, '_nectar_audio_mp3', ''),
(1366, 2327, '_nectar_audio_ogg', ''),
(1367, 2327, '_nectar_love', '366'),
(1368, 2327, '_wpb_vc_js_status', 'false'),
(1369, 2327, '_wpb_vc_js_interface_version', '0'),
(1370, 2327, '_nectar_gallery_slider', 'off'),
(1371, 2327, '_nectar_header_bg', ''),
(1372, 2327, '_nectar_header_parallax', 'on'),
(1373, 2327, '_nectar_header_bg_color', '#2d2d2d'),
(1374, 2327, '_nectar_header_font_color', ''),
(1375, 2327, '_nectar_header_overlay', 'on'),
(1376, 2327, '_nectar_header_bottom_shadow', 'on'),
(1377, 2327, '_disable_transparent_header', 'off'),
(1378, 2327, '_wp_old_slug', 'ambrose-redmoon-2'),
(1379, 2327, '_yoast_wpseo_opengraph-description', 'Courage is not the absence of fear, but rather the judgment that something else is more important than fear'),
(1380, 2327, '_post_item_masonry_sizing', 'regular'),
(1381, 2327, '_nectar_header_bg_height', '400'),
(1383, 2328, '_nectar_quote', ''),
(1384, 2328, '_nectar_link', ''),
(1385, 2328, '_nectar_video_m4v', ''),
(1386, 2328, '_nectar_video_ogv', ''),
(1387, 2328, '_nectar_video_poster', ''),
(1388, 2328, '_nectar_video_embed', ''),
(1389, 2328, '_nectar_audio_mp3', ''),
(1390, 2328, '_nectar_audio_ogg', ''),
(1391, 2328, '_nectar_love', '184'),
(1392, 2328, '_wpb_vc_js_status', 'false'),
(1393, 2328, '_wpb_vc_js_interface_version', '0'),
(1394, 2328, '_nectar_gallery_slider', 'off'),
(1395, 2328, '_nectar_header_bg', ''),
(1396, 2328, '_nectar_header_parallax', 'on'),
(1397, 2328, '_nectar_header_bg_height', ''),
(1398, 2328, '_nectar_header_bg_color', '#333333'),
(1399, 2328, '_nectar_header_font_color', ''),
(1400, 2328, '_nectar_header_overlay', 'off'),
(1401, 2328, '_nectar_header_bottom_shadow', 'off'),
(1402, 2328, '_disable_transparent_header', 'off'),
(1403, 2328, '_wp_old_slug', 'magna-fringilla-quis-condimentum-2'),
(1404, 2328, '_post_item_masonry_sizing', 'regular'),
(1406, 2329, '_nectar_quote', ''),
(1407, 2329, '_nectar_link', ''),
(1408, 2329, '_nectar_video_m4v', ''),
(1409, 2329, '_nectar_video_ogv', ''),
(1410, 2329, '_nectar_video_poster', ''),
(1411, 2329, '_nectar_video_embed', ''),
(1412, 2329, '_nectar_audio_mp3', ''),
(1413, 2329, '_nectar_audio_ogg', ''),
(1414, 2329, '_nectar_love', '84'),
(1415, 2329, '_wpb_vc_js_status', 'false'),
(1416, 2329, '_wpb_vc_js_interface_version', '0'),
(1417, 2329, '_nectar_gallery_slider', 'off'),
(1418, 2329, '_nectar_header_bg', ''),
(1419, 2329, '_nectar_header_parallax', 'on'),
(1420, 2329, '_nectar_header_bg_height', ''),
(1421, 2329, '_nectar_header_bg_color', '#2d2d2d'),
(1422, 2329, '_nectar_header_font_color', ''),
(1423, 2329, '_nectar_header_overlay', 'on'),
(1424, 2329, '_nectar_header_bottom_shadow', 'on'),
(1425, 2329, '_disable_transparent_header', 'off'),
(1426, 2329, '_post_item_masonry_sizing', 'wide_tall'),
(1428, 2671, '_wpb_vc_js_status', 'true'),
(1429, 2671, '_wpb_vc_js_interface_version', '2'),
(1430, 2671, '_nectar_gallery_slider', 'off'),
(1431, 2671, '_nectar_quote', ''),
(1432, 2671, '_nectar_link', ''),
(1433, 2671, '_nectar_video_m4v', ''),
(1434, 2671, '_nectar_video_ogv', ''),
(1435, 2671, '_nectar_video_poster', ''),
(1436, 2671, '_nectar_video_embed', ''),
(1437, 2671, '_nectar_audio_mp3', ''),
(1438, 2671, '_nectar_audio_ogg', ''),
(1439, 2671, '_nectar_header_bg', ''),
(1440, 2671, '_nectar_header_parallax', 'on'),
(1441, 2671, '_nectar_header_bg_color', '#353535'),
(1442, 2671, '_nectar_header_font_color', ''),
(1443, 2671, '_nectar_header_overlay', 'on'),
(1444, 2671, '_nectar_header_bottom_shadow', 'on'),
(1445, 2671, '_disable_transparent_header', 'off'),
(1446, 2671, '_wp_old_slug', 'ownage'),
(1447, 2671, '_nectar_love', '2'),
(1448, 2671, '_yoast_wpseo_opengraph-image', 'http://themenectar.com/demo/salient-blog/wp-content/uploads/2014/07/sky.jpg'),
(1449, 2671, '_yoast_wpseo_google-plus-description', 'Sed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci. Donec at blandit augue, nec tristique ligula.'),
(1450, 2671, '_post_item_masonry_sizing', 'regular'),
(1451, 2671, '_nectar_header_bg_height', ''),
(1453, 2677, '_wpb_vc_js_status', 'false'),
(1454, 2677, '_wpb_vc_js_interface_version', '2'),
(1455, 2677, '_nectar_gallery_slider', 'off'),
(1456, 2677, '_nectar_quote', ''),
(1457, 2677, '_nectar_link', ''),
(1458, 2677, '_nectar_video_m4v', ''),
(1459, 2677, '_nectar_video_ogv', ''),
(1460, 2677, '_nectar_video_poster', ''),
(1461, 2677, '_nectar_video_embed', ''),
(1462, 2677, '_nectar_audio_mp3', ''),
(1463, 2677, '_nectar_audio_ogg', ''),
(1464, 2677, '_nectar_header_bg', ''),
(1465, 2677, '_nectar_header_parallax', 'off'),
(1466, 2677, '_nectar_header_bg_color', '#353535'),
(1467, 2677, '_nectar_header_font_color', ''),
(1468, 2677, '_nectar_header_overlay', 'on'),
(1469, 2677, '_nectar_header_bottom_shadow', 'on'),
(1470, 2677, '_disable_transparent_header', 'off'),
(1471, 2677, '_nectar_love', '0'),
(1472, 2677, '_post_item_masonry_sizing', 'regular'),
(1473, 2677, '_nectar_header_bg_height', '400'),
(1475, 3145, '_wpb_vc_js_status', 'false'),
(1476, 3145, '_wpb_vc_js_interface_version', '2'),
(1477, 3145, '_nectar_gallery_slider', 'off'),
(1478, 3145, '_nectar_quote', ''),
(1479, 3145, '_nectar_link', ''),
(1480, 3145, '_nectar_video_m4v', ''),
(1481, 3145, '_nectar_video_ogv', ''),
(1482, 3145, '_nectar_video_poster', ''),
(1483, 3145, '_nectar_video_embed', ''),
(1484, 3145, '_nectar_audio_mp3', ''),
(1485, 3145, '_nectar_audio_ogg', ''),
(1486, 3145, '_post_item_masonry_sizing', 'wide_tall'),
(1487, 3145, '_nectar_header_bg', ''),
(1488, 3145, '_nectar_header_parallax', 'on'),
(1489, 3145, '_nectar_header_bg_color', '#3a3a3a'),
(1490, 3145, '_nectar_header_font_color', ''),
(1491, 3145, '_nectar_header_overlay', 'on'),
(1492, 3145, '_nectar_header_bottom_shadow', 'on'),
(1493, 3145, '_disable_transparent_header', 'off'),
(1494, 3145, '_nectar_header_bg_height', ''),
(1495, 3146, '_menu_item_type', 'post_type'),
(1496, 3146, '_menu_item_menu_item_parent', '3148'),
(1497, 3146, '_menu_item_object_id', '568'),
(1498, 3146, '_menu_item_object', 'page'),
(1499, 3146, '_menu_item_target', ''),
(1500, 3146, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1501, 3146, '_menu_item_xfn', ''),
(1502, 3146, '_menu_item_url', ''),
(1503, 3147, '_menu_item_type', 'post_type'),
(1504, 3147, '_menu_item_menu_item_parent', '3148'),
(1505, 3147, '_menu_item_object_id', '205'),
(1506, 3147, '_menu_item_object', 'page'),
(1507, 3147, '_menu_item_target', ''),
(1508, 3147, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1509, 3147, '_menu_item_xfn', ''),
(1510, 3147, '_menu_item_url', ''),
(1511, 3148, '_menu_item_type', 'post_type'),
(1512, 3148, '_menu_item_menu_item_parent', '7'),
(1513, 3148, '_menu_item_object_id', '207'),
(1514, 3148, '_menu_item_object', 'page'),
(1515, 3148, '_menu_item_target', ''),
(1516, 3148, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1517, 3148, '_menu_item_xfn', ''),
(1518, 3148, '_menu_item_url', ''),
(1519, 3149, '_menu_item_type', 'post_type'),
(1520, 3149, '_menu_item_menu_item_parent', '3148'),
(1521, 3149, '_menu_item_object_id', '482'),
(1522, 3149, '_menu_item_object', 'page'),
(1523, 3149, '_menu_item_target', ''),
(1524, 3149, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1525, 3149, '_menu_item_xfn', ''),
(1526, 3149, '_menu_item_url', ''),
(1527, 3150, '_menu_item_type', 'post_type'),
(1528, 3150, '_menu_item_menu_item_parent', '0'),
(1529, 3150, '_menu_item_object_id', '551'),
(1530, 3150, '_menu_item_object', 'page'),
(1531, 3150, '_menu_item_target', ''),
(1532, 3150, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1533, 3150, '_menu_item_xfn', ''),
(1534, 3150, '_menu_item_url', ''),
(1535, 3151, '_menu_item_type', 'post_type'),
(1536, 3151, '_menu_item_menu_item_parent', '13'),
(1537, 3151, '_menu_item_object_id', '764'),
(1538, 3151, '_menu_item_object', 'page'),
(1539, 3151, '_menu_item_target', ''),
(1540, 3151, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1541, 3151, '_menu_item_xfn', ''),
(1542, 3151, '_menu_item_url', ''),
(1543, 3152, '_menu_item_type', 'post_type'),
(1544, 3152, '_menu_item_menu_item_parent', '13'),
(1545, 3152, '_menu_item_object_id', '767'),
(1546, 3152, '_menu_item_object', 'page');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1547, 3152, '_menu_item_target', ''),
(1548, 3152, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1549, 3152, '_menu_item_xfn', ''),
(1550, 3152, '_menu_item_url', ''),
(1551, 3153, '_menu_item_type', 'post_type'),
(1552, 3153, '_menu_item_menu_item_parent', '13'),
(1553, 3153, '_menu_item_object_id', '770'),
(1554, 3153, '_menu_item_object', 'page'),
(1555, 3153, '_menu_item_target', ''),
(1556, 3153, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1557, 3153, '_menu_item_xfn', ''),
(1558, 3153, '_menu_item_url', ''),
(1559, 3154, '_menu_item_type', 'post_type'),
(1560, 3154, '_menu_item_menu_item_parent', '3148'),
(1561, 3154, '_menu_item_object_id', '207'),
(1562, 3154, '_menu_item_object', 'page'),
(1563, 3154, '_menu_item_target', ''),
(1564, 3154, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1565, 3154, '_menu_item_xfn', ''),
(1566, 3154, '_menu_item_url', ''),
(1567, 3155, '_menu_item_type', 'post_type'),
(1568, 3155, '_menu_item_menu_item_parent', '13'),
(1569, 3155, '_menu_item_object_id', '965'),
(1570, 3155, '_menu_item_object', 'page'),
(1571, 3155, '_menu_item_target', ''),
(1572, 3155, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1573, 3155, '_menu_item_xfn', ''),
(1574, 3155, '_menu_item_url', ''),
(1575, 3156, '_menu_item_type', 'post_type'),
(1576, 3156, '_menu_item_menu_item_parent', '20'),
(1577, 3156, '_menu_item_object_id', '2040'),
(1578, 3156, '_menu_item_object', 'page'),
(1579, 3156, '_menu_item_target', ''),
(1580, 3156, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1581, 3156, '_menu_item_xfn', ''),
(1582, 3156, '_menu_item_url', ''),
(1583, 3157, '_menu_item_type', 'post_type'),
(1584, 3157, '_menu_item_menu_item_parent', '20'),
(1585, 3157, '_menu_item_object_id', '2038'),
(1586, 3157, '_menu_item_object', 'page'),
(1587, 3157, '_menu_item_target', ''),
(1588, 3157, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1589, 3157, '_menu_item_xfn', ''),
(1590, 3157, '_menu_item_url', ''),
(1591, 3158, '_menu_item_type', 'post_type'),
(1592, 3158, '_menu_item_menu_item_parent', '20'),
(1593, 3158, '_menu_item_object_id', '2036'),
(1594, 3158, '_menu_item_object', 'page'),
(1595, 3158, '_menu_item_target', ''),
(1596, 3158, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1597, 3158, '_menu_item_xfn', ''),
(1598, 3158, '_menu_item_url', ''),
(1599, 3159, '_menu_item_type', 'post_type'),
(1600, 3159, '_menu_item_menu_item_parent', '20'),
(1601, 3159, '_menu_item_object_id', '2034'),
(1602, 3159, '_menu_item_object', 'page'),
(1603, 3159, '_menu_item_target', ''),
(1604, 3159, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1605, 3159, '_menu_item_xfn', ''),
(1606, 3159, '_menu_item_url', ''),
(1607, 3160, '_menu_item_type', 'post_type'),
(1608, 3160, '_menu_item_menu_item_parent', '0'),
(1609, 3160, '_menu_item_object_id', '2092'),
(1610, 3160, '_menu_item_object', 'page'),
(1611, 3160, '_menu_item_target', ''),
(1612, 3160, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1613, 3160, '_menu_item_xfn', ''),
(1614, 3160, '_menu_item_url', ''),
(1623, 3162, '_menu_item_type', 'post_type'),
(1624, 3162, '_menu_item_menu_item_parent', '20'),
(1625, 3162, '_menu_item_object_id', '2563'),
(1626, 3162, '_menu_item_object', 'page'),
(1627, 3162, '_menu_item_target', ''),
(1628, 3162, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1629, 3162, '_menu_item_xfn', ''),
(1630, 3162, '_menu_item_url', ''),
(1632, 3163, '_menu_item_type', 'post_type'),
(1633, 3163, '_menu_item_menu_item_parent', '0'),
(1634, 3163, '_menu_item_object_id', '596'),
(1635, 3163, '_menu_item_object', 'page'),
(1636, 3163, '_menu_item_target', ''),
(1637, 3163, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1638, 3163, '_menu_item_xfn', ''),
(1639, 3163, '_menu_item_url', ''),
(1649, 6, '_wp_page_template', 'default'),
(1650, 6, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(1651, 6, 'nectar-metabox-portfolio-display-sortable', 'off'),
(1652, 6, '_nectar_full_screen_rows', 'off'),
(1653, 6, '_nectar_full_screen_rows_animation', 'none'),
(1654, 6, '_nectar_full_screen_rows_animation_speed', 'medium'),
(1655, 6, '_nectar_full_screen_rows_overall_bg_color', ''),
(1656, 6, '_nectar_full_screen_rows_anchors', 'off'),
(1657, 6, '_nectar_full_screen_rows_mobile_disable', 'off'),
(1658, 6, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(1659, 6, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(1660, 6, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(1661, 6, '_nectar_full_screen_rows_footer', 'none'),
(1662, 6, '_nectar_slider_bg_type', 'video_bg'),
(1663, 6, '_nectar_canvas_shapes', ''),
(1664, 6, '_nectar_media_upload_webm', ''),
(1665, 6, '_nectar_media_upload_mp4', 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/shutterstock_28142716.mp4'),
(1666, 6, '_nectar_media_upload_ogv', ''),
(1667, 6, '_nectar_slider_preview_image', ''),
(1668, 6, '_nectar_header_bg', ''),
(1669, 6, '_nectar_header_parallax', 'off'),
(1670, 6, '_nectar_header_box_roll', 'off'),
(1671, 6, '_nectar_header_bg_height', '400'),
(1672, 6, '_nectar_header_fullscreen', 'off'),
(1673, 6, '_nectar_header_title', 'An exhibition to save sight'),
(1674, 6, '_nectar_header_subtitle', ''),
(1675, 6, '_nectar_page_header_text-effect', 'none'),
(1676, 6, '_nectar_particle_rotation_timing', ''),
(1677, 6, '_nectar_particle_disable_explosion', 'off'),
(1678, 6, '_nectar_page_header_alignment', 'center'),
(1679, 6, '_nectar_page_header_alignment_v', 'middle'),
(1680, 6, '_nectar_page_header_bg_alignment', 'center'),
(1681, 6, '_nectar_header_bg_color', ''),
(1682, 6, '_nectar_header_font_color', ''),
(1683, 6, '_nectar_header_bg_overlay_color', '#046272'),
(1684, 6, '_wpb_vc_js_status', 'true'),
(1685, 3165, '_wpb_shortcodes_custom_css', '.vc_custom_1485638236800{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1686, 6, '_wpb_shortcodes_custom_css', '.vc_custom_1557912914701{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1687, 3166, '_wpb_shortcodes_custom_css', '.vc_custom_1485638236800{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1688, 3167, '_wp_attached_file', '2019/05/last-seen-title-master400sx90.png'),
(1689, 3167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:90;s:4:\"file\";s:41:\"2019/05/last-seen-title-master400sx90.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-400x90.png\";s:5:\"width\";i:400;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-140x90.png\";s:5:\"width\";i:140;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-350x90.png\";s:5:\"width\";i:350;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master400sx90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1690, 2326, 'nectar_blog_post_view_count', '0'),
(1691, 6, '_disable_transparent_header', 'off'),
(1692, 6, '_force_transparent_header', 'on'),
(1693, 6, '_force_transparent_header_color', 'light'),
(1694, 3187, '_wpb_shortcodes_custom_css', '.vc_custom_1485638236800{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1695, 3186, '_wpb_shortcodes_custom_css', '.vc_custom_1557912914701{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1696, 3190, '_wp_attached_file', '2019/05/shutterstock_28142716.mp4'),
(1697, 3190, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:2332026;s:9:\"mime_type\";s:15:\"video/quicktime\";s:6:\"length\";i:19;s:16:\"length_formatted\";s:4:\"0:19\";s:5:\"width\";i:2046;s:6:\"height\";i:1080;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:6:{s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"codec\";s:0:\"\";s:11:\"sample_rate\";d:6144;s:8:\"channels\";i:0;s:15:\"bits_per_sample\";i:12288;s:8:\"lossless\";b:0;}s:17:\"created_timestamp\";i:-2082844800;}'),
(1698, 3191, '_wp_attached_file', '2019/05/last-seen-title-master-neg-450x90.png'),
(1699, 3191, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:90;s:4:\"file\";s:45:\"2019/05/last-seen-title-master-neg-450x90.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-400x90.png\";s:5:\"width\";i:400;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-140x90.png\";s:5:\"width\";i:140;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-350x90.png\";s:5:\"width\";i:350;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:44:\"last-seen-title-master-neg-450x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1700, 2327, 'nectar_blog_post_view_count', '0'),
(1701, 3192, '_wp_trash_meta_status', 'publish'),
(1702, 3192, '_wp_trash_meta_time', '1557903626'),
(1703, 2, '_wp_trash_meta_status', 'draft'),
(1704, 2, '_wp_trash_meta_time', '1557903656'),
(1705, 2, '_wp_desired_post_slug', 'sample-page'),
(1706, 3193, '_edit_last', '1'),
(1707, 3193, '_edit_lock', '1557912268:1'),
(1708, 3193, '_nectar_portfolio_extra_content', '[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"padding-10-percent\" column_padding_position=\"all\" background_color=\"#f4f4f4\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/2\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_custom_heading text=\"The story\" font_container=\"tag:h5|text_align:left|color:%23bfbfbf\" use_theme_fonts=\"yes\" css=\".vc_custom_1557907567019{margin-bottom: 10px !important;}\"][vc_custom_heading text=\"Meet Yuma\" font_container=\"tag:h2|font_size:40|text_align:left|line_height:50px\" use_theme_fonts=\"yes\"][vc_custom_heading text=\"On New Year’s Eve 2008, Yuma was a victim of a terrible accident. Fireworks were set off and instead of going vertically they launched horizontally into Yuma’s face leaving him permanently blind.\" font_container=\"tag:p|text_align:left|color:rgba(10%2C10%2C10%2C0.6)\" use_theme_fonts=\"yes\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" background_image=\"https://source.unsplash.com/Nj0mCM6nikI\" enable_bg_scale=\"true\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/2\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" background_image=\"3203\" enable_bg_scale=\"true\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/2\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"padding-10-percent\" column_padding_position=\"all\" background_color=\"#ffffff\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/2\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_custom_heading text=\"A Little Of Our Story\" font_container=\"tag:h5|text_align:left|color:%23bfbfbf\" use_theme_fonts=\"yes\" css=\".vc_custom_1486408382586{margin-bottom: 10px !important;}\"][vc_custom_heading text=\"We were simply born to paint pixels for a living\" font_container=\"tag:h2|font_size:40|text_align:left|line_height:50px\" use_theme_fonts=\"yes\"][vc_custom_heading text=\"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia\" font_container=\"tag:p|text_align:left|color:rgba(10%2C10%2C10%2C0.6)\" use_theme_fonts=\"yes\"][nectar_btn size=\"large\" button_style=\"see-through-2\" hover_text_color_override=\"#ffffff\" icon_family=\"none\" url=\"#\" text=\"See Our Story\" margin_top=\"30\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" enable_video_color_overlay=\"true\" video_overlay_color=\"#046272\" video_mp4=\"http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/shutterstock_28142716.mp4\" text_color=\"light\" text_align=\"center\" top_padding=\"20%\" bottom_padding=\"20%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"#\"][/vc_column][/vc_row]'),
(1709, 3193, '_nectar_portfolio_item_layout', 'enabled'),
(1710, 3193, '_nectar_portfolio_custom_grid_item', 'off'),
(1711, 3193, '_nectar_portfolio_custom_grid_item_content', ''),
(1712, 3193, '_nectar_portfolio_custom_thumbnail', 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Yuma-Test-1920x.jpg'),
(1713, 3193, '_nectar_hide_featured', 'on'),
(1714, 3193, '_portfolio_item_masonry_sizing', 'tall'),
(1715, 3193, '_portfolio_item_masonry_content_pos', 'middle'),
(1716, 3193, '_nectar_external_project_url', ''),
(1717, 3193, 'nectar-metabox-portfolio-parent-override', 'default'),
(1718, 3193, '_nectar_project_excerpt', 'by Kate McKay'),
(1719, 3193, '_nectar_project_accent_color', ''),
(1720, 3193, '_nectar_project_title_color', ''),
(1721, 3193, '_nectar_project_subtitle_color', ''),
(1722, 3193, '_nectar_project_css_class', ''),
(1723, 3193, '_nectar_slider_bg_type', 'image_bg'),
(1724, 3193, '_nectar_media_upload_webm', ''),
(1725, 3193, '_nectar_media_upload_mp4', ''),
(1726, 3193, '_nectar_media_upload_ogv', ''),
(1727, 3193, '_nectar_slider_preview_image', ''),
(1728, 3193, '_nectar_header_bg', 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Yuma-Test-1920x.jpg'),
(1729, 3193, '_nectar_header_parallax', 'off'),
(1730, 3193, '_nectar_header_bg_height', '350'),
(1731, 3193, '_nectar_header_fullscreen', 'off'),
(1732, 3193, '_nectar_page_header_bg_alignment', 'center'),
(1733, 3193, '_nectar_header_bg_color', ''),
(1734, 3193, '_nectar_header_bg_overlay_color', '#104b60'),
(1735, 3193, '_nectar_header_subtitle', ''),
(1736, 3193, '_nectar_header_font_color', ''),
(1737, 3193, '_disable_transparent_header', 'off'),
(1738, 3193, '_force_transparent_header_color', 'light'),
(1739, 3193, '_nectar_video_m4v', 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/shutterstock_28142716.mp4'),
(1740, 3193, '_nectar_video_ogv', ''),
(1741, 3193, '_nectar_video_poster', ''),
(1742, 3193, '_nectar_video_embed', ''),
(1743, 3193, '_wpb_vc_js_status', 'true'),
(1744, 3193, '_wpb_shortcodes_custom_css', '.vc_custom_1557907567019{margin-bottom: 10px !important;}.vc_custom_1486408382586{margin-bottom: 10px !important;}'),
(1745, 3193, '_nectar_love', '0'),
(1746, 3195, '_wp_attached_file', '2019/05/Screen-Shot-2019-02-25-at-10.18.33-am-e1557906420370.png'),
(1747, 3191, '_edit_lock', '1557907343:1'),
(1748, 3195, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:714;s:4:\"file\";s:64:\"2019/05/Screen-Shot-2019-02-25-at-10.18.33-am-e1557906420370.png\";s:5:\"sizes\";a:24:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-300x178.png\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-768x457.png\";s:5:\"width\";i:768;s:6:\"height\";i:457;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:50:\"Screen-Shot-2019-02-25-at-10.18.33-am-1024x609.png\";s:5:\"width\";i:1024;s:6:\"height\";i:609;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_large\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-900x604.png\";s:5:\"width\";i:900;s:6:\"height\";i:604;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-600x403.png\";s:5:\"width\";i:600;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-400x269.png\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-140x140.png\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"wide\";a:4:{s:4:\"file\";s:50:\"Screen-Shot-2019-02-25-at-10.18.33-am-1000x500.png\";s:5:\"width\";i:1000;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-670x335.png\";s:5:\"width\";i:670;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-350x350.png\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:50:\"Screen-Shot-2019-02-25-at-10.18.33-am-500x1000.png\";s:5:\"width\";i:500;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"wide_tall\";a:4:{s:4:\"file\";s:51:\"Screen-Shot-2019-02-25-at-10.18.33-am-1000x1000.png\";s:5:\"width\";i:1000;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"wide_photography\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-900x600.png\";s:5:\"width\";i:900;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"large_featured\";a:4:{s:4:\"file\";s:50:\"Screen-Shot-2019-02-25-at-10.18.33-am-1700x700.png\";s:5:\"width\";i:1700;s:6:\"height\";i:700;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"medium_featured\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-800x800.png\";s:5:\"width\";i:800;s:6:\"height\";i:800;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-600x357.png\";s:5:\"width\";i:600;s:6:\"height\";i:357;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-600x357.png\";s:5:\"width\";i:600;s:6:\"height\";i:357;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:49:\"Screen-Shot-2019-02-25-at-10.18.33-am-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1749, 3195, '_edit_lock', '1557906730:1'),
(1750, 3195, '_wp_attachment_backup_sizes', 'a:1:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:2710;s:6:\"height\";i:1612;s:4:\"file\";s:41:\"Screen-Shot-2019-02-25-at-10.18.33-am.png\";}}'),
(1751, 3195, 'nectar_image_gal_url', ''),
(1752, 3195, 'nectar_image_gal_masonry_sizing', 'regular'),
(1753, 3195, 'nectar_particle_shape_bg_color', ''),
(1754, 3195, 'nectar_particle_shape_color', ''),
(1755, 3195, 'nectar_particle_shape_color_mapping', 'original'),
(1756, 3195, 'nectar_particle_shape_color_alpha', 'original'),
(1757, 3195, 'nectar_particle_shape_density', 'very_low'),
(1758, 3195, 'nectar_particle_max_particle_size', ''),
(1759, 3195, '_edit_last', '1'),
(1760, 3196, '_wp_attached_file', '2019/05/Yuma-Test-1920x.jpg'),
(1761, 3196, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1142;s:4:\"file\";s:27:\"2019/05/Yuma-Test-1920x.jpg\";s:5:\"sizes\";a:24:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-768x457.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:457;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"Yuma-Test-1920x-1024x609.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:609;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"portfolio-thumb_large\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-900x604.jpg\";s:5:\"width\";i:900;s:6:\"height\";i:604;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-600x403.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:403;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-400x269.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-140x140.jpg\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"wide\";a:4:{s:4:\"file\";s:28:\"Yuma-Test-1920x-1000x500.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:500;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-670x335.jpg\";s:5:\"width\";i:670;s:6:\"height\";i:335;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:7:\"regular\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-500x500.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tall\";a:4:{s:4:\"file\";s:28:\"Yuma-Test-1920x-500x1000.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:1000;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"wide_tall\";a:4:{s:4:\"file\";s:29:\"Yuma-Test-1920x-1000x1000.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:1000;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"wide_photography\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-900x600.jpg\";s:5:\"width\";i:900;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"large_featured\";a:4:{s:4:\"file\";s:28:\"Yuma-Test-1920x-1700x700.jpg\";s:5:\"width\";i:1700;s:6:\"height\";i:700;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"medium_featured\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-800x800.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:800;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"Yuma-Test-1920x-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-600x357.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-600x357.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"Yuma-Test-1920x-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1762, 3193, '_thumbnail_id', '3196'),
(1763, 3198, '_wp_attached_file', '2019/05/last-seen-title-master.png'),
(1764, 3198, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:135;s:4:\"file\";s:34:\"2019/05/last-seen-title-master.png\";s:5:\"sizes\";a:15:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"last-seen-title-master-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-400x135.png\";s:5:\"width\";i:400;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-140x135.png\";s:5:\"width\";i:140;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-350x135.png\";s:5:\"width\";i:350;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"last-seen-title-master-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"last-seen-title-master-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1765, 3199, '_wp_attached_file', '2019/05/last-seen-title-master@05x.png'),
(1766, 3199, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:135;s:4:\"file\";s:38:\"2019/05/last-seen-title-master@05x.png\";s:5:\"sizes\";a:15:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"last-seen-title-master@05x-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-400x135.png\";s:5:\"width\";i:400;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-140x135.png\";s:5:\"width\";i:140;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-350x135.png\";s:5:\"width\";i:350;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:38:\"last-seen-title-master@05x-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:38:\"last-seen-title-master@05x-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1767, 3200, '_wp_attached_file', '2019/05/last-seen-title-master@05xNEG.png'),
(1768, 3200, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:135;s:4:\"file\";s:41:\"2019/05/last-seen-title-master@05xNEG.png\";s:5:\"sizes\";a:15:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master@05xNEG-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-400x135.png\";s:5:\"width\";i:400;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-140x135.png\";s:5:\"width\";i:140;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-350x135.png\";s:5:\"width\";i:350;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:41:\"last-seen-title-master@05xNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1769, 3201, '_wp_attached_file', '2019/05/last-seen-title-masterNEG.png'),
(1770, 3201, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:135;s:4:\"file\";s:37:\"2019/05/last-seen-title-masterNEG.png\";s:5:\"sizes\";a:15:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"last-seen-title-masterNEG-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-400x135.png\";s:5:\"width\";i:400;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-140x135.png\";s:5:\"width\";i:140;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-350x135.png\";s:5:\"width\";i:350;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-500x135.png\";s:5:\"width\";i:500;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:37:\"last-seen-title-masterNEG-150x135.png\";s:5:\"width\";i:150;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1771, 3203, '_wp_attached_file', '2019/05/Yuma-Test-800xSketch.jpg'),
(1772, 3203, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:448;s:4:\"file\";s:32:\"2019/05/Yuma-Test-800xSketch.jpg\";s:5:\"sizes\";a:18:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-300x168.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-768x430.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:430;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-600x403.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:403;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-400x269.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-140x140.jpg\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-670x335.jpg\";s:5:\"width\";i:670;s:6:\"height\";i:335;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:7:\"regular\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-500x448.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:448;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:4:\"tall\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-500x448.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:448;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"medium_featured\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-800x448.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:448;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-600x336.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:336;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-600x336.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:336;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"Yuma-Test-800xSketch-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1800, 3209, '_menu_item_type', 'custom'),
(1801, 3209, '_menu_item_menu_item_parent', '0'),
(1802, 3209, '_menu_item_object_id', '3209'),
(1803, 3209, '_menu_item_object', 'custom'),
(1804, 3209, '_menu_item_target', ''),
(1805, 3209, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1806, 3209, '_menu_item_xfn', ''),
(1807, 3209, '_menu_item_url', '#'),
(1818, 3211, '_menu_item_type', 'custom'),
(1819, 3211, '_menu_item_menu_item_parent', '0'),
(1820, 3211, '_menu_item_object_id', '3211'),
(1821, 3211, '_menu_item_object', 'custom'),
(1822, 3211, '_menu_item_target', ''),
(1823, 3211, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1824, 3211, '_menu_item_xfn', ''),
(1825, 3211, '_menu_item_url', '#'),
(1827, 3212, '_menu_item_type', 'custom'),
(1828, 3212, '_menu_item_menu_item_parent', '0'),
(1829, 3212, '_menu_item_object_id', '3212'),
(1830, 3212, '_menu_item_object', 'custom'),
(1831, 3212, '_menu_item_target', ''),
(1832, 3212, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1833, 3212, '_menu_item_xfn', ''),
(1834, 3212, '_menu_item_url', '#'),
(1836, 3213, '_wpb_shortcodes_custom_css', '.vc_custom_1557912914701{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1837, 3214, '_wp_attached_file', '2019/05/last-seen-title-master-400x90.png');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1838, 3214, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:90;s:4:\"file\";s:41:\"2019/05/last-seen-title-master-400x90.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-400x90.png\";s:5:\"width\";i:400;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-140x90.png\";s:5:\"width\";i:140;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-350x90.png\";s:5:\"width\";i:350;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:40:\"last-seen-title-master-400x90-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1839, 3215, '_wp_attached_file', '2019/05/Tagline_PWW-Lion-Grey@1.png'),
(1840, 3215, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:35:\"2019/05/Tagline_PWW-Lion-Grey@1.png\";s:5:\"sizes\";a:14:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-500x403.png\";s:5:\"width\";i:500;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-400x269.png\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-140x140.png\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"wide\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-500x335.png\";s:5:\"width\";i:500;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-350x350.png\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"Tagline_PWW-Lion-Grey@1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1841, 3216, '_wp_attached_file', '2019/05/Tagline_PWW-Lion-Grey@1-1.png'),
(1842, 3216, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:37:\"2019/05/Tagline_PWW-Lion-Grey@1-1.png\";s:5:\"sizes\";a:14:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-500x403.png\";s:5:\"width\";i:500;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-400x269.png\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-140x140.png\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"wide\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-500x335.png\";s:5:\"width\";i:500;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-350x350.png\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:37:\"Tagline_PWW-Lion-Grey@1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1843, 3217, '_wpb_shortcodes_custom_css', '.vc_custom_1557912914701{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1844, 6, 'inline_featured_image', '0'),
(1845, 3218, '_wpb_shortcodes_custom_css', '.vc_custom_1557912914701{padding-right: 10% !important;}.vc_custom_1485638241294{padding-right: 10% !important;}'),
(1846, 3219, 'inline_featured_image', '0'),
(1847, 3219, '_edit_lock', '1557915432:1'),
(1848, 3219, '_wp_page_template', 'template-portfolio.php'),
(1849, 3219, '_edit_last', '1'),
(1850, 3219, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(1851, 3219, 'nectar-metabox-portfolio-display-sortable', 'off'),
(1852, 3219, '_nectar_slider_bg_type', 'image_bg'),
(1853, 3219, '_nectar_canvas_shapes', ''),
(1854, 3219, '_nectar_media_upload_webm', ''),
(1855, 3219, '_nectar_media_upload_mp4', ''),
(1856, 3219, '_nectar_media_upload_ogv', ''),
(1857, 3219, '_nectar_slider_preview_image', ''),
(1858, 3219, '_nectar_header_bg', ''),
(1859, 3219, '_nectar_header_parallax', 'off'),
(1860, 3219, '_nectar_header_box_roll', 'off'),
(1861, 3219, '_nectar_header_bg_height', ''),
(1862, 3219, '_nectar_header_fullscreen', 'off'),
(1863, 3219, '_nectar_header_title', ''),
(1864, 3219, '_nectar_header_subtitle', ''),
(1865, 3219, '_nectar_page_header_text-effect', 'none'),
(1866, 3219, '_nectar_particle_rotation_timing', ''),
(1867, 3219, '_nectar_particle_disable_explosion', 'off'),
(1868, 3219, '_nectar_page_header_alignment', 'left'),
(1869, 3219, '_nectar_page_header_alignment_v', 'middle'),
(1870, 3219, '_nectar_page_header_bg_alignment', 'center'),
(1871, 3219, '_nectar_header_bg_color', ''),
(1872, 3219, '_nectar_header_font_color', ''),
(1873, 3219, '_nectar_header_bg_overlay_color', ''),
(1874, 3219, '_disable_transparent_header', 'off'),
(1875, 3219, '_force_transparent_header', 'off'),
(1876, 3219, '_force_transparent_header_color', 'light'),
(1877, 3219, '_wpb_vc_js_status', 'false'),
(1878, 596, '_edit_lock', '1557915358:1'),
(1881, 3222, 'inline_featured_image', '0'),
(1882, 3222, '_edit_lock', '1557962222:1'),
(1883, 3222, '_edit_last', '1'),
(1884, 3222, '_wp_page_template', 'default'),
(1885, 3222, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(1886, 3222, 'nectar-metabox-portfolio-display-sortable', 'off'),
(1887, 3222, '_nectar_full_screen_rows', 'off'),
(1888, 3222, '_nectar_full_screen_rows_animation', 'none'),
(1889, 3222, '_nectar_full_screen_rows_animation_speed', 'medium'),
(1890, 3222, '_nectar_full_screen_rows_overall_bg_color', ''),
(1891, 3222, '_nectar_full_screen_rows_anchors', 'off'),
(1892, 3222, '_nectar_full_screen_rows_mobile_disable', 'off'),
(1893, 3222, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(1894, 3222, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(1895, 3222, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(1896, 3222, '_nectar_full_screen_rows_footer', 'none'),
(1897, 3222, '_nectar_slider_bg_type', 'image_bg'),
(1898, 3222, '_nectar_canvas_shapes', ''),
(1899, 3222, '_nectar_media_upload_webm', ''),
(1900, 3222, '_nectar_media_upload_mp4', ''),
(1901, 3222, '_nectar_media_upload_ogv', ''),
(1902, 3222, '_nectar_slider_preview_image', ''),
(1903, 3222, '_nectar_header_bg', ''),
(1904, 3222, '_nectar_header_parallax', 'off'),
(1905, 3222, '_nectar_header_box_roll', 'off'),
(1906, 3222, '_nectar_header_bg_height', ''),
(1907, 3222, '_nectar_header_fullscreen', 'off'),
(1908, 3222, '_nectar_header_title', ''),
(1909, 3222, '_nectar_header_subtitle', ''),
(1910, 3222, '_nectar_page_header_text-effect', 'none'),
(1911, 3222, '_nectar_particle_rotation_timing', ''),
(1912, 3222, '_nectar_particle_disable_explosion', 'off'),
(1913, 3222, '_nectar_page_header_alignment', 'left'),
(1914, 3222, '_nectar_page_header_alignment_v', 'middle'),
(1915, 3222, '_nectar_page_header_bg_alignment', 'center'),
(1916, 3222, '_nectar_header_bg_color', ''),
(1917, 3222, '_nectar_header_font_color', ''),
(1918, 3222, '_nectar_header_bg_overlay_color', ''),
(1919, 3222, '_disable_transparent_header', 'off'),
(1920, 3222, '_force_transparent_header', 'off'),
(1921, 3222, '_force_transparent_header_color', 'light'),
(1922, 3222, '_wpb_vc_js_status', 'true'),
(1923, 3226, 'inline_featured_image', '0'),
(1924, 3226, '_edit_lock', '1557916362:1'),
(1925, 3226, '_edit_last', '1'),
(1926, 3226, '_wp_page_template', 'default'),
(1927, 3226, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(1928, 3226, 'nectar-metabox-portfolio-display-sortable', 'off'),
(1929, 3226, '_nectar_full_screen_rows', 'off'),
(1930, 3226, '_nectar_full_screen_rows_animation', 'none'),
(1931, 3226, '_nectar_full_screen_rows_animation_speed', 'medium'),
(1932, 3226, '_nectar_full_screen_rows_overall_bg_color', ''),
(1933, 3226, '_nectar_full_screen_rows_anchors', 'off'),
(1934, 3226, '_nectar_full_screen_rows_mobile_disable', 'off'),
(1935, 3226, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(1936, 3226, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(1937, 3226, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(1938, 3226, '_nectar_full_screen_rows_footer', 'none'),
(1939, 3226, '_nectar_slider_bg_type', 'image_bg'),
(1940, 3226, '_nectar_canvas_shapes', ''),
(1941, 3226, '_nectar_media_upload_webm', ''),
(1942, 3226, '_nectar_media_upload_mp4', ''),
(1943, 3226, '_nectar_media_upload_ogv', ''),
(1944, 3226, '_nectar_slider_preview_image', ''),
(1945, 3226, '_nectar_header_bg', ''),
(1946, 3226, '_nectar_header_parallax', 'off'),
(1947, 3226, '_nectar_header_box_roll', 'off'),
(1948, 3226, '_nectar_header_bg_height', '400'),
(1949, 3226, '_nectar_header_fullscreen', 'off'),
(1950, 3226, '_nectar_header_title', 'The Artists'),
(1951, 3226, '_nectar_header_subtitle', ''),
(1952, 3226, '_nectar_page_header_text-effect', 'none'),
(1953, 3226, '_nectar_particle_rotation_timing', ''),
(1954, 3226, '_nectar_particle_disable_explosion', 'off'),
(1955, 3226, '_nectar_page_header_alignment', 'center'),
(1956, 3226, '_nectar_page_header_alignment_v', 'middle'),
(1957, 3226, '_nectar_page_header_bg_alignment', 'center'),
(1958, 3226, '_nectar_header_bg_color', '#105189'),
(1959, 3226, '_nectar_header_font_color', '#ffffff'),
(1960, 3226, '_nectar_header_bg_overlay_color', ''),
(1961, 3226, '_disable_transparent_header', 'off'),
(1962, 3226, '_force_transparent_header', 'on'),
(1963, 3226, '_force_transparent_header_color', 'light'),
(1964, 3226, '_wpb_vc_js_status', 'true'),
(1966, 3232, '_menu_item_type', 'post_type'),
(1967, 3232, '_menu_item_menu_item_parent', '0'),
(1968, 3232, '_menu_item_object_id', '3226'),
(1969, 3232, '_menu_item_object', 'page'),
(1970, 3232, '_menu_item_target', ''),
(1971, 3232, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1972, 3232, '_menu_item_xfn', ''),
(1973, 3232, '_menu_item_url', ''),
(1975, 3233, '_menu_item_type', 'post_type'),
(1976, 3233, '_menu_item_menu_item_parent', '0'),
(1977, 3233, '_menu_item_object_id', '3222'),
(1978, 3233, '_menu_item_object', 'page'),
(1979, 3233, '_menu_item_target', ''),
(1980, 3233, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1981, 3233, '_menu_item_xfn', ''),
(1982, 3233, '_menu_item_url', ''),
(1984, 3234, '_menu_item_type', 'post_type'),
(1985, 3234, '_menu_item_menu_item_parent', '0'),
(1986, 3234, '_menu_item_object_id', '3219'),
(1987, 3234, '_menu_item_object', 'page'),
(1988, 3234, '_menu_item_target', ''),
(1989, 3234, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1990, 3234, '_menu_item_xfn', ''),
(1991, 3234, '_menu_item_url', ''),
(1992, 3235, 'inline_featured_image', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-14 02:45:40', '2019-05-14 02:45:40', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-05-14 02:45:40', '2019-05-14 02:45:40', '', 0, 'http://localhost:8888/qei_lastseen/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-14 02:45:40', '2019-05-14 02:45:40', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8888/qei_lastseen/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2019-05-15 07:00:56', '2019-05-15 07:00:56', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-05-14 02:45:40', '2019-05-14 02:45:40', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8888/qei_lastseen.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-05-14 02:45:40', '2019-05-14 02:45:40', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-05-14 02:50:39', '2019-05-14 02:50:39', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2019-05-14 02:50:39', '2019-05-14 02:50:39', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2019-05-14 03:06:44', '2019-05-14 03:06:44', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" enable_shape_divider=\"true\" shape_divider_position=\"both\" shape_divider_height=\"100\" bg_image_animation=\"none\" shape_type=\"fan\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h1>What would you want to see</h1>\r\n<h1>one more time if you</h1>\r\n<h1>lost your sight?</h1>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1557912914701{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" project_style=\"9\" item_spacing=\"default\" load_in_animation=\"perspective\" projects_per_page=\"4\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Watch the reel</h2>\r\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" video_external=\"https://www.youtube.com/watch?v=4BfKFCOCJe8\" text_color=\"light\" text_align=\"left\" top_padding=\"15%\" bottom_padding=\"15%\" enable_gradient=\"true\" color_overlay=\"#585b64\" color_overlay_2=\"#2c2f38\" gradient_direction=\"left_to_right\" overlay_strength=\"0.8\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" vertically_center_columns=\"true\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" top_padding=\"5%\" bottom_padding=\"0\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Our sponsors</h2>\r\n[/vc_column_text][divider line_type=\"No Line\" custom_height=\"50\"][clients columns=\"4\" hover_effect=\"opacity\" additional_padding=\"3\" fade_in_animation=\"true\" carousel=\"true\"][client image=\"3216\" title=\"Client\" id=\"1557914669970-4\" tab_id=\"1557914669971-4\" url=\"#\" name=\"Publicis Worldwide\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670041-7\" tab_id=\"1557914670041-7\" url=\"#\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670106-8\" tab_id=\"1557914670107-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670156-4\" tab_id=\"1557914670157-9\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670219-2\" tab_id=\"1557914670219-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670261-9\" tab_id=\"1557914670261-7\" url=\"#\"][/client][/clients][/vc_column][/vc_row]', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-05-15 10:12:03', '2019-05-15 10:12:03', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Features', '', 'publish', 'closed', 'closed', '', 'features', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/features/', 3, 'nav_menu_item', '', 0),
(8, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Nectar Sliders', '', 'publish', 'closed', 'closed', '', 'nectar-sliders', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/nectar-sliders/', 13, 'nav_menu_item', '', 0),
(9, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Gorgeous Design', '', 'publish', 'closed', 'closed', '', 'gorgeous-design', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/gorgeous-design/', 4, 'nav_menu_item', '', 0),
(10, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Full Width Slider', '', 'publish', 'closed', 'closed', '', 'full-width-slider', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/full-width-slider/', 14, 'nav_menu_item', '', 0),
(11, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Parallax Slider', '', 'publish', 'closed', 'closed', '', 'parallax-slider', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/parallax-slider/', 15, 'nav_menu_item', '', 0),
(12, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Video Backgrounds', '', 'publish', 'closed', 'closed', '', 'video-backgrounds', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/video-backgrounds/', 16, 'nav_menu_item', '', 0),
(13, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Robust Power', '', 'publish', 'closed', 'closed', '', 'robust-power', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/robust-power/', 18, 'nav_menu_item', '', 0),
(14, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Tell Your Story', '', 'publish', 'closed', 'closed', '', 'tell-your-story', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/tell-your-story/', 7, 'nav_menu_item', '', 0),
(15, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Show Off Your Work', '', 'publish', 'closed', 'closed', '', 'show-off-your-work', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/show-off-your-work/', 6, 'nav_menu_item', '', 0),
(16, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Multiple Instances', '', 'publish', 'closed', 'closed', '', 'multiple-instances', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/multiple-instances/', 17, 'nav_menu_item', '', 0),
(17, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Demos', '', 'publish', 'closed', 'closed', '', 'demos', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/demos/', 23, 'nav_menu_item', '', 0),
(18, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Clean All-Purpose', '', 'publish', 'closed', 'closed', '', 'clean-all-purpose', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/clean-all-purpose/', 27, 'nav_menu_item', '', 0),
(19, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Modern Creative', '', 'publish', 'closed', 'closed', '', 'modern-creative', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/modern-creative/', 26, 'nav_menu_item', '', 0),
(20, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/blog/', 33, 'nav_menu_item', '', 0),
(21, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Shop With Sidebar', '', 'publish', 'closed', 'closed', '', 'shop-with-sidebar', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/shop-with-sidebar/', 32, 'nav_menu_item', '', 0),
(22, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/shop/', 30, 'nav_menu_item', '', 0),
(23, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Ascend', '', 'publish', 'closed', 'closed', '', 'ascend', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/ascend/', 24, 'nav_menu_item', '', 0),
(24, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Masonry Classic Fullwidth', '', 'publish', 'closed', 'closed', '', 'masonry-classic-fullwidth', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-classic-fullwidth/', 38, 'nav_menu_item', '', 0),
(25, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Masonry Classic Sidebar', '', 'publish', 'closed', 'closed', '', 'masonry-classic-sidebar', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-classic-sidebar/', 39, 'nav_menu_item', '', 0),
(26, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'One Page', '', 'publish', 'closed', 'closed', '', 'one-page', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/one-page/', 28, 'nav_menu_item', '', 0),
(27, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Ecommerce', '', 'publish', 'closed', 'closed', '', 'ecommerce', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/ecommerce/', 29, 'nav_menu_item', '', 0),
(28, 1, '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 'Blog Creative', '', 'publish', 'closed', 'closed', '', 'blog-creative', '', '', '2019-05-14 02:53:43', '2019-05-14 02:53:43', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/blog-creative/', 25, 'nav_menu_item', '', 0),
(29, 1, '2013-03-23 05:01:12', '2013-03-23 05:01:12', '', 'Blog', '', 'publish', 'open', 'open', '', 'blog', '', '', '2013-03-23 05:01:12', '2013-03-23 05:01:12', '', 0, 'http://themenectar.com/demo/salient/?page_id=6', 47, 'page', '', 0),
(30, 1, '2013-03-23 13:05:51', '2013-03-23 13:05:51', '', 'Features', '', 'draft', 'open', 'open', '', 'features', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient/?page_id=23', 43, 'page', '', 0),
(82, 1, '2014-03-21 16:19:14', '2014-03-21 16:19:14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae dui et nunc ornare vulputate non fringilla massa. Praesent sit amet erat sapien, auctor consectetur ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non ligula augue. Integer justo arcu, tempor eu venenatis non, sagittis nec lacus. Morbi vitae dui et nunc ornare vulputate non fringilla massa. Praesent sit amet erat sapien, auctor consectetur ligula.', 'Nulla Magna', '', 'publish', 'open', 'open', '', 'nulla-magna', '', '', '2014-03-21 16:19:14', '2014-03-21 16:19:14', '', 0, 'http://themenectar.com/demo/salient/?p=82', 5, 'post', '', 0),
(84, 1, '2014-07-15 16:40:53', '2014-07-15 16:40:53', '<!--more-->\n\nPraesent id mollis mauris. Duis in ultricies dolor, ac sagittis massa. Suspendisse potenti. Vivamus auctor, velit ac dapibus sollicitudin, diam tortor fermentum risus, in pulvinar tellus ipsum ut sapien. Cras venenatis rhoncus nisi, vitae aliquet urna blandit non. Donec quis neque posuere, dapibus nunc sed, commodo turpis. Aenean ut rutrum augue. Vivamus fermentum venenatis nulla, sed tristique arcu euismod at. Sed nibh sapien, porttitor et rhoncus sit amet, ornare sit amet libero. In venenatis orci at pulvinar cursus. Nullam ut odio bibendum, pretium ligula nec, ultrices turpis. Aliquam volutpat sed dui ut porttitor.\n\nEtiam consequat, lacus nec laoreet auctor, ligula urna cursus felis, consectetur convallis lectus nunc ac mauris. Phasellus sodales, sem in varius viverra, ipsum tortor sollicitudin enim, commodo ultrices est dolor consectetur est. Maecenas eget magna a nisi iaculis viverra. Maecenas egestas diam sed turpis feugiat pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque eget arcu laoreet, pretium felis sit amet, semper orci. Praesent mi erat, fermentum in ante vel, auctor fermentum magna. Duis congue vel arcu eu consectetur. Maecenas ut consectetur elit. Fusce malesuada consequat diam, quis pulvinar nunc lobortis rutrum. Mauris tempus nulla nibh, a tincidunt lorem semper feugiat. Vestibulum facilisis diam ac nulla laoreet, sed pharetra velit dictum. Sed consectetur iaculis leo, ut tincidunt nisl varius vitae.', 'Video Post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae dui et nunc ornare vulputate non fringilla massa. Praesent sit amet erat sapien, auctor consectetur ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non ligula augue. Praesent imperdiet magna at risus lobortis ac accumsan lorem ornare. In aliquam, sapien ac vehicula vestibulum, arcu magna aliquet velit,', 'publish', 'open', 'open', '', 'video-post', '', '', '2014-07-15 16:40:53', '2014-07-15 16:40:53', '', 0, 'http://themenectar.com/demo/salient/?p=84', 7, 'post', '', 0),
(108, 1, '2013-01-10 17:19:25', '2013-01-10 17:19:25', 'Sed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci. Donec at blandit augue, nec tristique ligula. Donec euismod nulla at erat fringilla, a porttitor lorem condimentum. Aliquam ut interdum libero, at pharetra orci. Quisque fermentum, felis ut gravida ornare, eros neque egestas dui, quis sollicitudin sem sem non ante. Fusce metus lorem, scelerisque ut diam non, vehicula dapibus sapien. Sed sagittis, ante at ornare pellentesque, sem justo consequat enim, quis adipiscing quam enim ac velit. Vestibulum condimentum tempus sapien eu porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque interdum odio vel consectetur dignissim. Fusce eget tristique nulla. Duis at purus ut est cursus imperdiet. Sed condimentum massa in enim cursus, sed mattis elit malesuada. Nunc tincidunt magna sit amet metus sagittis tempor. Maecenas lacinia condimentum ultricies. Sed et lacus nec augue venenatis interdum. Fusce sit amet luctus augue. Aenean scelerisque porttitor ultrices. Nulla dui tellus, egestas sit amet posuere sit amet, dictum tristique mauris. Praesent sed elementum mauris. Nullam et enim eget nisl eleifend auctor lobortis et dui. Curabitur at malesuada magna. Nunc nec tincidunt mauris, vitae tincidunt enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ante leo, bibendum at metus in, dignissim sodales nisi. Curabitur id augue iaculis, elementum diam eget, ultrices massa. Fusce eleifend tincidunt neque, a porta nibh faucibus non.', 'Ink In Water', 'Sed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci.', 'publish', 'open', 'open', '', 'ink-in-water', '', '', '2013-01-10 17:19:25', '2013-01-10 17:19:25', '', 0, 'http://themenectar.com/demo/salient/?p=108', 12, 'post', '', 0),
(110, 1, '2013-03-23 17:20:09', '2013-03-23 17:20:09', 'In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. <!--more-->\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non enim ut enim fringilla adipiscing id in lorem. Quisque aliquet neque vitae lectus tempus consectetur. Aliquam erat volutpat. Nunc eu nibh nulla, id cursus arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam at velit nisl. Aenean vitae est nisl. Cras molestie molestie nisl vel imperdiet. Donec vel mi sem.\n<h4>Luctus fermentum commodo</h4>\nNulla sed mi leo, sit amet molestie nulla. Phasellus lobortis blandit ipsum, at adipiscing eros porta quis. Phasellus in nisi ipsum, quis dapibus magna. Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque eu ipsum et quam faucibus scelerisque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla mi. Duis lobortis cursus mi vel tristique. Maecenas eu lorem hendrerit neque dapibus cursus id sit amet nisi. Proin rhoncus semper sem nec aliquet.\n<blockquote>Nulla facilisi. Vestibulum pretium, dui eu aliquam faucibus, est dui hendrerit nulla, mattis semper turpis mauris eget tellus. Nulla accumsan rutrum nibh, sed eleifend felis blandit.</blockquote>\nInteger vel libero arcu, egestas tempor ipsum. Vestibulum id dolor aliquet dolor fringilla ornare. Nunc non massa erat. Vivamus odio sem, rhoncus vel bibendum vitae, euismod a urna. Aliquam erat volutpat. Aenean non lorem arcu. Phasellus in neque nulla, sed sodales ipsum. Morbi a massa sed sapien vulputate lacinia. Vivamus et urna vitae felis malesuada aliquet sit amet et metus.\n<ul>\n	<li>Consectetur adipiscing elit vtae elit libero</li>\n	<li>Nullam id dolor id eget lacinia odio posuere erat a ante</li>\n	<li>Integer posuere erat dapibus posuere velit</li>\n</ul>\nNulla sed mi leo, sit amet molestie nulla. Phasellus lobortis blandit ipsum, at adipiscing eros porta quis. Phasellus in nisi ipsum, quis dapibus magna. Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque eu ipsum et quam faucibus scelerisque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla mi. Duis lobortis cursus mi vel tristique. Maecenas eu lorem hendrerit neque dapibus cursus id sit amet nisi. Proin rhoncus semper sem nec aliquet. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum. Donec id elit non mi porta gravida at eget metus.\n\n<code>\n.some-style {\nwidth: 960px;\nheight: 200px;\nmargin: 0 auto;\nbackground-color: #000000;\n}\n</code>\n\n<strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non enim ut enim fringilla adipiscing id in lorem. Quisque aliquet neque vitae lectus tempus consectetur. Aliquam erat volutpat. Nunc eu nibh nulla, id cursus arcu.</strong>\n\n<em> </em>', 'The Field', '', 'publish', 'open', 'open', '', 'the-field', '', '', '2013-03-23 17:20:09', '2013-03-23 17:20:09', '', 0, 'http://themenectar.com/demo/salient/?p=110', 3, 'post', '', 0),
(137, 1, '2012-01-29 17:59:15', '2012-01-29 17:59:15', '', 'The Sweetest WordPress Themes Around.', '', 'publish', 'open', 'open', '', 'premium-wordpress-themes-bursting-with-quality', '', '', '2012-01-29 17:59:15', '2012-01-29 17:59:15', '', 0, 'http://themenectar.com/demo/salient/?p=137', 16, 'post', '', 0),
(152, 1, '2013-01-07 18:10:00', '2013-01-07 18:10:00', '<!--more-->\n\nPhasellus venenatis venenatis velit ut ultricies. Cras porta dignissim malesuada. Etiam auctor, justo et facilisis ultrices, justo mauris imperdiet ligula, vitae tincidunt justo quam fermentum nulla. Nunc interdum porta ligula, eu malesuada nunc tristique eu. Integer sed mi ac velit congue vulputate sed quis lacus. Fusce tellus sem, ultricies consequat porta at, ultrices in odio. Nullam a sapien vitae erat porta faucibus rhoncus eleifend dolor. Mauris semper rutrum ante a auctor. Nullam non arcu erat, vel sollicitudin felis. Nam erat nisl, mattis euismod lacinia at, tempus quis nunc.\n\nDonec luctus odio eget ipsum lacinia lacinia. Suspendisse porttitor laoreet sagittis. Nulla mi justo, auctor sed posuere sit amet, pretium ac metus. Nulla porta justo ac velit accumsan faucibus. In dignissim, orci nec ultricies elementum, erat mi tristique dui, a tristique ligula quam non est. Suspendisse mattis aliquet est, at tempor ipsum lacinia a. Cras enim sapien, dapibus sit amet iaculis sit amet, pharetra non diam. Curabitur sed nisi in metus eleifend fringilla. Aenean viverra sem in quam luctus eget gravida diam scelerisque.', 'Suspendisse diam', '', 'publish', 'open', 'open', '', 'suspendisse-vulputate-diam', '', '', '2013-01-07 18:10:00', '2013-01-07 18:10:00', '', 0, 'http://themenectar.com/demo/salient/?p=152', 15, 'post', '', 0),
(197, 1, '2013-03-23 21:55:09', '2013-03-23 21:55:09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada felis eget sapien viverra fringilla. Nullam placerat, odio quis vestibulum condimentum, mi ligula venenatis tortor, in fermentum lacus augue sit amet enim. Curabitur pretium neque in metus volutpat id ultrices arcu scelerisque. Nunc lacus leo, dictum non scelerisque sed, ultricies ac ipsum. Vestibulum eget ultrices sapien. Nam faucibus tempus scelerisque. In elit quam, tempus gravida gravida sit amet, laoreet id nulla. Phasellus ligula sapien, tempor accumsan euismod vitae, molestie ut nisl. Aliquam elementum posuere felis, eget molestie magna viverra sed.\n\nDuis consectetur nisl ut odio aliquam sodales. In hac habitasse platea dictumst. Duis in purus augue. Nunc magna mi, pretium eu tempus ac, scelerisque quis turpis. Morbi aliquet consectetur metus, sed iaculis sapien tincidunt nec. Praesent condimentum vulputate erat vel sodales. Sed sollicitudin venenatis adipiscing.uspendisse ornare mi a felis consequat sagittis. Ut accumsan mi sit amet mauris fringilla fringilla. Pellentesque in sem in lacus varius gravida vitae et velit. at ante\n\n[one_half]\n<h3>[icon size=\"small\" image=\"icon-cloud\"] Quisque malesuada felis</h3>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada felis eget sapien viverra fringilla. Nullam placerat, odio quis vestibulum condimentum, mi ligula venenatis tortor, in fermentum lacus augue sit amet enim. Curabitur pretium neque in metus volutpat id ultrices arcu scelerisque. Nunc lacus leo, dictum non scelerisque sed, ultricies ac ipsum. [/one_half]\n\n[one_half_last]\n<h3>[icon size=\"small\" image=\"icon-hdd\"] Phasellus ligula elit</h3>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada felis eget sapien viverra fringilla. Nullam placerat, odio quis vestibulum condimentum, mi ligula venenatis tortor, in fermentum lacus augue sit amet enim. Curabitur pretium neque in metus volutpat id ultrices arcu scelerisque. Nunc lacus leo, dictum non scelerisque sed, ultricies ac ipsum.  [/one_half_last]', 'Sidebar', '', 'publish', 'open', 'open', '', 'page-w-sidebar', '', '', '2013-03-23 21:55:09', '2013-03-23 21:55:09', '', 30, 'http://themenectar.com/demo/salient/?page_id=197', 41, 'page', '', 0),
(205, 1, '2013-03-23 22:31:02', '2013-03-23 22:31:02', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][vc_column width=\"1/1\"][heading subtitle=\"The good ol\' standard column, available in a variety of sizes.\"] Standard Columns [/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_half][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half][one_half_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_third][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"0\"][vc_column width=\"1/1\"][heading subtitle=\"Easily turn any column into a boxed column with one click!\"]Boxed Columns[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Donec Ac Lacus</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Aliquam Ac Urna</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Sed Congue Nunc</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Consectetur Adipiscing</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"10\"][vc_column width=\"1/1\"][heading subtitle=\"For those looking for a little extra flash in their content.\"] Animated Columns [/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_half animation=\"Fade In From Left\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half][one_half_last animation=\"Fade In From Right\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_third animation=\"Fade In\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third animation=\"Fade In\" delay=\"300\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third_last animation=\"Fade In\" delay=\"600\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth animation=\"Fade In From Bottom\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth animation=\"Fade In From Bottom\" delay=\"200\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth animation=\"Fade In From Bottom\" delay=\"400\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last animation=\"Fade In From Bottom\" delay=\"600\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row]', 'Columns', '', 'draft', 'open', 'open', '', 'columns', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://themenectar.com/demo/salient/?page_id=205', 40, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(207, 1, '2013-03-23 22:45:31', '2013-03-23 22:45:31', '[vc_row bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"no-repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"30\" background_color=\"#fcfcfc\" type=\"full_width_background\" text_align=\"left\" bg_position=\"left top\" scene_position=\"center\"][one_half][toggles][toggle title=\"Toggle Panel #1\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viv erra a, dapibus at dolor. In iaculis vive rra neque, ac eleifend ante lobo rtis id. congue id  [/vc_column_text][/toggle][toggle title=\"Toggle Panel #2\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. [/vc_column_text][/toggle][toggle title=\"Toggle Panel #3\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id.. [/vc_column_text][/toggle][/toggles][/one_half][one_half_last][tabbed_section][tab title=\"Sample Tab #1\" id=\"1396210176255-5\" tab_id=\"1424844241428-6\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][tab title=\"Sample Tab #2\" id=\"1396210195201-9\" tab_id=\"1424844241491-9\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][tab title=\"Sample Tab #3\" id=\"1396210195262-7\" tab_id=\"1424844241547-1\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][/tabbed_section][/one_half_last][/vc_row][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"90\" background_color=\"#fcfcfc\" type=\"full_width_background\" text_align=\"left\"][one_third centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"small\" url=\"#\" text=\"Small Button\" color=\"extra-color-2\"][/vc_column_text][/one_third][one_third centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"large\" url=\"#\" text=\"Medium Button\" color=\"extra-color-2\"][/vc_column_text][/one_third][one_third_last centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"large\" url=\"#\" text=\"Large Button\" color=\"extra-color-2\"][/vc_column_text][/one_third_last][/full_width_section][full_width_section bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"50\" background_color=\"#f4f4f4\" type=\"full_width_background\" text_align=\"left\"][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-heart\" color=\"extra-color-1\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-bolt\" color=\"extra-color-2\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-leaf\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-key\" color=\"extra-color-1\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-lightbulb\" color=\"extra-color-2\"][/vc_column_text][/one_sixth][one_sixth_last centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-eye-open\" color=\"accent-color\"][/vc_column_text][/one_sixth_last][/full_width_section][full_width_section bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"40\" bottom_padding=\"70\" background_color=\"#f4f4f4\"][three_fourths][image_with_animation animation=\"Fade In From Left\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/03/fontawesome.jpg\"] [/image_with_animation][/three_fourths][one_fourth_last][vc_column_text]\n<h4>[icon size=\"small\" image=\"icon-heart\"] Lorem ipsum dolor sit</h4>\n<h4>[icon size=\"small\" image=\"icon-bolt\"] Rhoncus iaculis et at</h4>\n<h4>[icon size=\"small\" image=\"icon-leaf\"] Curabitur lectus nun</h4>\n<h4>[icon size=\"small\" image=\"icon-key\"] Posuere feugiat risus</h4>\n<h4>[icon size=\"small\" image=\"icon-lightbulb\"] Iaculis et at tortor</h4>\n<h4>[icon size=\"small\" image=\"icon-eye-open\"] Nulla facil Donec</h4>\n[divider line_type=\"Full Width Line\" custom_height=\"50\"][icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-heart\"] Lorem ipsum dolor sit\n[icon size=\"tiny\"  color=\"extra-color-1\" image=\"icon-bolt\"] Rhoncus iaculis et at\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-leaf\"] Curabitur lectus nun\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-key\"] Posuere feugiat risus\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-lightbulb\"] Iaculis et at tortor[/vc_column_text][/one_fourth_last][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"40\" bottom_padding=\"35\" background_color=\"#5de3dc\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Full width color sections anywhere you want! [button color=\"See-Through\" size=\"medium\" url=\"\" text=\"Alt button\" image=\"default-arrow\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/06/elements-testimonials-bg.jpg\" top_padding=\"90\" bottom_padding=\"90\" background_color=\"#fff\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][testimonial_slider][testimonial name=\"Greg Ross, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec massa placerat, ac aliquam dui. Quisque semper tempus tellus ac lobortis. Aenean consectetur laoreet massa non interdum. Massa non interdum. \" id=\"t1\" title=\"Testimonial\" tab_id=\"1424844241795-8\"][/testimonial][testimonial name=\"Jeff Gemmell, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus erat ac massa placerat, ac aliquam Aenean consectetur laoreet massa non interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. \" id=\"t2\" title=\"Testimonial\" tab_id=\"1424844241877-7\"][/testimonial][testimonial name=\"Mark Levin, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus erat ac massa placerat, ac aliquam dui pulvinar. Quisque semper tempus tellus ac lobortis. Aenean consectetur laoreet massa non interdum. \" id=\"t3\" title=\"Testimonial\" tab_id=\"1424844241906-3\"][/testimonial][/testimonial_slider][/vc_column][/full_width_section][vc_row bg_pos=\"Center Center\" parallax_bg=\"flase\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"70\" background_color=\"#F5F5F5\"][vc_column width=\"1/4\"][milestone color=\"extra-color-1\" number=\"65\" subject=\"Awards Won\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-1\" number=\"37\" subject=\"GitHub Repo\'s\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-2\" number=\"439\" subject=\"Cups of Coffee\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-2\" number=\"268\" subject=\"Completed Designs\"][/vc_column][/vc_row][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"70\" background_color=\"#f1f1f1\"][one_third][vc_column_text]\n<h2>Animated Bar Graph</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie. Curabitur pellentesque massa eu nulla consequat sed porttitor arcu porttitor. Quisque volutpat pharetra felis, eu cursus lorem molestie vitae. Nulla vehicula, lacus ut suscipit fermentum, turpis felis ultricies dui, ut rhoncus libero augue at libero. Morbi ut arcu dolor.  [/vc_column_text][/one_third][two_thirds_last][bar title=\"HTML/CSS\" percent=\"70\" id=\"b1\"][bar title=\"Logo Design\" percent=\"80\" id=\"b2\"][bar title=\"WordPress\" percent=\"100\" id=\"b3\"][bar title=\"Photoshop\" percent=\"90\" id=\"b4\"][bar title=\"Illustrator\" percent=\"75\" id=\"b5\"][/two_thirds_last][/full_width_section][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"80\" bottom_padding=\"30\"][/vc_row][vc_row][vc_column width=\"1/1\"][carousel easing=\"easeInOutQuart\" carousel_title=\"Team Members - Available in Two Styles\" scroll_speed=\"1000\"][item id=\"1389858025982-8\" title=\"Item\" tab_id=\"1424844242054-2\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2319\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026029-3\" title=\"Item\" tab_id=\"1424844242098-10\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2321\" name=\"Carlton Harris\" job_position=\"Developer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026071-1\" title=\"Item\" tab_id=\"1424844242134-9\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2320\" name=\"Amanda Kliens\" job_position=\"Graphic Designer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026114-10\" title=\"Item\" tab_id=\"1424844242169-9\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2322\" name=\"Tyler Johnson\" job_position=\"Front-End Developer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][/carousel][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"left\" top_padding=\"30\" bottom_padding=\"80\"][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2309\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2305\" name=\"Carlton Harris\" job_position=\"Developer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2307\" name=\"Amanda Kliens\" job_position=\"Graphic Designer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2496\" name=\"Tyler Johnson\" job_position=\"Front-End Developer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][/vc_row]', 'Elements', '', 'draft', 'open', 'open', '', 'elements', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://themenectar.com/demo/salient/?page_id=207', 39, 'page', '', 0),
(482, 1, '2013-04-13 06:24:03', '2013-04-13 06:24:03', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"40\"][vc_column width=\"1/1\"][pricing_table columns=\"4\"][pricing_column title=\"Basic\" price=\"99\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950167991-0\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>Yes, this too!</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Standard\" highlight=\"true\" highlight_reason=\"Most Popular\" price=\"199\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168041-9\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And this too</li>\n	<li>Maybe even this</li>\n	<li>Nevermind, it\'s not</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Advanced\" price=\"299\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168076-2\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>Even this too</li>\n	<li>Also included</li>\n	<li>You even get this</li>\n	<li>And a little of this</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Premium\" price=\"399\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168111-3\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>Even unlimited this</li>\n	<li>You also get this!</li>\n	<li>Add even some of this</li>\n	<li>Only if you want it</li>\n</ul>\n[button size=\"large\" url=\"http://themenectar.com\" text=\"Sign up now!\"][/pricing_column][/pricing_table][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"20\"][vc_column width=\"1/1\"][heading subtitle=\"Lorem ipsum dolor sit amet, consectetur adipiscing\"] More Pricing Table Examples! [/heading][/vc_column][/vc_row][vc_row][vc_column width=\"1/1\"][pricing_table columns=\"5\"][pricing_column title=\"Free\" price=\"0\" currency_symbol=\"$\" id=\"1389950168281-10\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>But that\'s it</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Light\" price=\"5\" currency_symbol=\"$\" interval=\"Per Week\" id=\"1389950168324-5\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>Even unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Basic\" price=\"10\" currency_symbol=\"$\" interval=\"Per Week\" id=\"1389950168360-1\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And some of  this</li>\n	<li>Even unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Super\" highlight=\"true\" highlight_reason=\"Recommended\" price=\"30\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168396-7\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And this too</li>\n	<li>Maybe even this</li>\n	<li>[icon size=\"tiny\" image=\"icon-star-empty\"]Unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Deluxe\" price=\"50\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168435-10\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>And a lot of this!</li>\n	<li>[icon size=\"tiny\" image=\"icon-star-empty\"]Unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][/pricing_table][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"30\"][vc_column width=\"1/2\"][vc_column_text]\n<h2>Are these the only plans?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][vc_column width=\"1/2\"][vc_column_text]\n<h2>Are there any discounts?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"40\"][vc_column width=\"1/2\"][vc_column_text]\n<h2>When will my account be activated?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][vc_column width=\"1/2\"][vc_column_text]\n<h2>Can I cancel at any time?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][/vc_row]', 'Pricing Tables', '', 'draft', 'open', 'open', '', 'pricing-tables', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://themenectar.com/demo/salient/?page_id=482', 38, 'page', '', 0),
(551, 1, '2013-06-26 22:56:49', '2013-06-26 22:56:49', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"center\" scene_position=\"center\" layer_one_image=\"2518\" top_padding=\"0\" bottom_padding=\"0\" layer_four_image=\"2655\"][vc_column width=\"1/1\"][nectar_slider location=\"Home\" full_width=\"true\" arrow_navigation=\"true\" desktop_swipe=\"true\" slider_transition=\"fade\" parallax=\"true\" slider_button_styling=\"btn_with_count\" overall_style=\"directional\" button_sizing=\"large\" slider_height=\"650\" flexible_slider_height=\"true\" autorotate=\"5000\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" bg_color=\"#f4f4f4\" top_padding=\"70\" bottom_padding=\"60\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2>We are a creative agency with a passion for design &amp; developing beautiful creations. We\'ve had the privilege to work with some of the largest names in the business and enjoy blogging about our projects and adventures as often as possible</h2>\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"fashion,food-for-thought,gaming,music,uncategorized\" pagination_type=\"default\" posts_per_page=\"10\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"padding-2-percent\" background_color_opacity=\"1\" column_padding_position=\"all\" background_color=\"#9a4fff\" background_color_hover=\"#2d2d2d\" column_link=\"#\"][vc_column_text]View The Full Blog[/vc_column_text][/vc_column][/vc_row]', 'Home - Landing Page', '', 'draft', 'open', 'open', '', 'home-extended', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://localhost/wordpress-3-5/?page_id=551', 37, 'page', '', 0),
(559, 1, '2013-01-08 23:55:44', '2013-01-08 23:55:44', 'Vivamus risus mi, lobortis ut congue vitae, vestibulum vitae augue. Maecenas nunc odio, pulvinar id vulputate nec, porttitor at quam. Suspendisse vulputate diam eu leo bibendum feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non ligula augue. Praesent imperdiet magna at risus lobortis ac accumsan lorem ornare Cras sed lobortis libero. Pellentesque arcu lacus, dignissim ut porta in, interdum vel risus. Curabitur non est purus. Ut adipiscing purus augue, quis elementum dolor convallis id.', 'Aenean laoreet tortor', 'Vivamus risus mi, lobortis ut congue vitae, vestibulum vitae augue. Maecenas nunc odio, pulvinar id vulputate nec, porttitor at quam. Suspendisse vulputate diam eu leo bibendum feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non ligula augue.', 'publish', 'open', 'open', '', 'aenean-laoreet-tortor', '', '', '2013-01-08 23:55:44', '2013-01-08 23:55:44', '', 0, 'http://themenectar.com/demo/salient/?p=559', 9, 'post', '', 0),
(568, 1, '2013-07-24 05:09:12', '2013-07-24 05:09:12', '[full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"70\" bottom_padding=\"0\" background_color=\"f4f4f4\" type=\"full_width_background\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n\n<img alt=\"header options panel\" src=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/header-options.png\" />[/vc_column_text][/vc_column][/full_width_section][full_width_section parallax_bg=\"true\" bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"65\" bottom_padding=\"65\" type=\"full_width_background\" text_align=\"left\" image_url=\"2404\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Check Out Some Of The Possible Combinations</h2>\n<p style=\"text-align: center;\">Every site should have its own unique design to it. Salient gives you the power you need to truly create something different!</p>\n[/vc_column_text][divider line_type=\"No Line\" custom_height=\"30\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/1.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/2.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/3.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/4.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/5.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"40\" background_color=\"f4f4f4\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Introducing The AJAX Shopping Cart</h2>\n<p style=\"text-align: center;\">Another cool feature that Salient offers is the AJAX shopping cart. When enabled, users will have immediate feedback after adding items to their cart in\nyour WooCommerce shop. They will also have access to view the contents of their cart without ever leaving or refreshing the page.</p>\n<img alt=\"header options panel\" src=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/wooCommerce-ajax-cart1.png\" />[/vc_column_text][/vc_column][/full_width_section]', 'Headers', '', 'draft', 'open', 'open', '', 'headers', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://localhost/wordpress-3-5/?page_id=568', 36, 'page', '', 0),
(596, 1, '2013-08-12 07:09:46', '2013-08-12 07:09:46', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"bottom\" text_color=\"light\" text_align=\"left\" top_padding=\"21%\" bottom_padding=\"19%\" mouse_based_parallax_bg=\"true\" layer_one_image=\"3123\" layer_one_strength=\"0.3\" layer_two_image=\"3124\" layer_two_strength=\"0.4\" layer_three_image=\"3125\" layer_three_strength=\"0.5\"][vc_column width=\"1/1\"][vc_column_text]\n<h1 class=\"jumbo\">Hello</h1>\n<h2><i>We are Salient. It\'s nice to meet you!\n</i></h2>\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#fbfbfb\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"0\" top_padding=\"0\" vertically_center_columns=\"true\"][vc_column width=\"2/3\" animation=\"fade-in\" delay=\"0\" column_padding=\"padding-5-percent\" background_color=\"#f7f7f7\" background_color_opacity=\"1\"][vc_row_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-atom\" color=\"Accent-Color\"]\n<h4>Web Development</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"200\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-email2\" color=\"Accent-Color\"]\n<h4>Social Marketing</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"400\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-diamond\" color=\"Accent-Color\"]\n<h4>Graphic Design</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"600\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-cloud\" color=\"Accent-Color\"]\n<h4>Cloud Hosting</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"800\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"linecon-icon-display\" color=\"Accent-Color\"]\n<h4>Web Design</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"1000\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-meter\" color=\"Accent-Color\"]\n<h4>Server Optimization</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/3\" animation=\"fade-in\" delay=\"200\" column_padding=\"padding-5-percent\" background_color=\"#ffffff\" background_color_opacity=\"1\"][toggles accordion=\"true\"][toggle title=\"Building your brand\" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][toggle title=\"Working with the best\" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][toggle title=\"Project availability \" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][/toggles][/vc_column][/vc_row][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" background_color=\"#fbfbfb\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" vertically_center_columns=\"true\"][vc_column width=\"1/3\" animation=\"none\" column_padding=\"padding-4-percent\" background_color=\"#9a4fff\" background_color_opacity=\"1\" column_padding_position=\"all\"][testimonial_slider][testimonial name=\"apawlik, Theme User\" quote=\"It\'s easy to see why this is one of the most downloaded themes on themeforest. It\'s a real game-changer, and a breathe of fresh air if you work with WordPress a lot.\" id=\"t1\" title=\"Testimonial\" tab_id=\"1424829107923-2\"][/testimonial][testimonial name=\"Editor02, Theme User\" quote=\"I literally could not be happier that I chose to buy your theme! Your regular updates and superb attention to detail blows me away every time I visit my new site!\" id=\"t2\" title=\"Testimonial\" tab_id=\"1424829108051-6\"][/testimonial][testimonial name=\"mdriess, Theme User\" quote=\"Amazing work! You didn’t just make a great looking theme, but you made one that is a pleasure to customize and not the least bit difficult to keep looking classy.\" id=\"t3\" title=\"Testimonial\" tab_id=\"1424829108191-9\"][/testimonial][/testimonial_slider][/vc_column][vc_column width=\"2/3\" animation=\"none\" column_padding=\"padding-4-percent\" background_color=\"#5de3dc\" background_color_opacity=\"1\" column_padding_position=\"all\"][clients columns=\"5\" carousel=\"true\"][client title=\"Client\" id=\"1395988736-1-29\" image=\"2905\" name=\"Fluid\" tab_id=\"1424829108349-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395988736-2-10\" image=\"2906\" name=\"squirrel\" tab_id=\"1424829108528-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395989142478-0-0\" image=\"2848\" name=\"Location point\" tab_id=\"1424829108653-0\"][/client][client title=\"Client\" id=\"1395989250503-0-1\" image=\"2847\" name=\"X\" tab_id=\"1424829108792-10\"][/client][client title=\"Client\" id=\"135948736-1-29\" image=\"2850\" name=\"Fluid\" tab_id=\"1424829108932-4\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395788736-2-10\" image=\"2849\" name=\"squirrel\" tab_id=\"1424829109073-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"139999142478-0-0\" image=\"2848\" name=\"Location point\" tab_id=\"1424829109215-2\"][/client][client title=\"Client\" id=\"1393989250503-0-1\" image=\"2847\" name=\"X\" tab_id=\"1424829109372-8\"][/client][/clients][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" background_color=\"#171920\"][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2794\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2795\" name=\"Lauren Clark\" job_position=\"Graphic Designer\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2799\" name=\"Andrew Johnson\" job_position=\"Front-end Developer\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" background_color=\"#171920\"][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2797\" name=\"Tyler Kendall\" job_position=\"Art Director\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2798\" name=\"Jacob King\" job_position=\"UX Director\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2796\" name=\"Amanda Kliens\" job_position=\"Human Resources\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][/full_width_section][vc_row type=\"full_width_content\" bg_position=\"center center\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" bg_color=\"#f9f9f9\"][vc_column width=\"1/2\" animation=\"none\" column_padding=\"padding-7-percent\" background_color=\"#f9f9f9\" background_color_opacity=\"1\"][vc_column_text]\n<h3 class=\"tiny\">Seeing Is Believing</h3>\n[divider line_type=\"Small Line\" custom_height=\"50\"]\n<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut mi ornare, pretium massa eu, rutrum enim. In in lorem congue, tincidunt justo at, consectetur nulla.</h2>\n<em><a href=\"http://themenectar.com/demo/salient-blog/portfolio-fullwidth-masonry/\">View The Goods</a>\n</em>[/vc_column_text][/vc_column][vc_column width=\"1/2\" animation=\"none\" column_padding=\"padding-7-percent\" background_color=\"#ffffff\" background_color_opacity=\"1\"][vc_column_text]\n<h3 class=\"tiny\">We Blog Too</h3>\n[divider line_type=\"Small Line\" custom_height=\"50\"]\n<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut mi ornare, pretium massa eu, rutrum enim. In in lorem congue, tincidunt justo at, consectetur nulla.</h2>\n<em><a href=\"http://themenectar.com/demo/salient-blog/blog-infinite-scroll/\">Pick Our Brains</a>\n</em>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"padding-2-percent\" background_color_opacity=\"1\" background_color_hover=\"#8446e2\" column_link=\"http://themenectar.com/demo/salient-blog/contact/\" background_color=\"#9a4fff\" column_padding_position=\"all\"][vc_column_text]Let\'s take the next step and work together[/vc_column_text][/vc_column][/vc_row]', 'About Us', '', 'draft', 'open', 'open', '', 'about-2', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://localhost/wordpress-3-6/?page_id=596', 35, 'page', '', 0),
(615, 1, '2013-09-21 22:53:09', '2013-09-21 22:53:09', '', 'Auto Draft', '', 'publish', 'closed', 'closed', '', 'auto-draft', '', '', '2013-09-21 22:53:09', '2013-09-21 22:53:09', '', 0, 'http://localhost/wordpress-3-6/?post_type=nectar_slider&amp;p=615', 2, 'nectar_slider', '', 0),
(617, 1, '2013-09-21 22:54:50', '2013-09-21 22:54:50', '', 'Auto Draft', '', 'publish', 'closed', 'closed', '', 'auto-draft-3', '', '', '2013-09-21 22:54:50', '2013-09-21 22:54:50', '', 0, 'http://localhost/wordpress-3-6/?post_type=nectar_slider&amp;p=617', 8, 'nectar_slider', '', 0),
(701, 1, '2013-11-11 02:54:55', '2013-11-11 02:54:55', '', 'Auto Draft', '', 'publish', 'closed', 'closed', '', 'auto-draft-14', '', '', '2013-11-11 02:54:55', '2013-11-11 02:54:55', '', 0, 'http://localhost/wordpress-3-6/?post_type=nectar_slider&amp;p=701', 9, 'nectar_slider', '', 0),
(764, 1, '2013-12-09 00:02:14', '2013-12-09 00:02:14', '[full_width_section parallax_bg=\"true\" bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"100\" bottom_padding=\"0\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\"><span><span style=\"color: #2ac4ea;\">The Control You Desire, All Available At Your Fingertips</span>\n</span></h2>\n<h4 class=\"light\" style=\"text-align: center;\">Experience our user friendly interface loaded with options for everything you\nneed and none of the extra bloat usually seen with such power.</h4>\n[/vc_column_text][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/options-panel.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Light\" top_padding=\"50\" bottom_padding=\"30\" background_color=\"#2AC4EA \"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Are You Convinced This Is The Right Theme For You? [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://themeforest.net/item/salient-responsive-portfolio-blog-theme/4363266\" text=\"Purchase Salient\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'Intuitive Options Panel', '', 'draft', 'open', 'open', '', 'intuitive-options-panel', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://localhost/wordpress-3-6/?page_id=764', 26, 'page', '', 0),
(767, 1, '2013-12-09 00:03:46', '2013-12-09 00:03:46', '[full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/shortcode-generator-bg.jpg\" top_padding=\"140\" bottom_padding=\"100\" type=\"full_width_background\" text_align=\"center\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Nectar Shortcodes Come In a Visually Intuitive Generator</h2>\n<h4 class=\"light\" style=\"text-align: center;\">This allows you to create beautiful &amp; unique site sections in a matter of sections\nall without memorizing or knowing any shortcode parameters.</h4>\n[/vc_column_text][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/shortcode-generator1.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"50\" bottom_padding=\"50\" background_color=\"#2ac4ea\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Are You Convinced This Is The Right Theme For You? [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://themeforest.net/item/salient-responsive-portfolio-blog-theme/4363266\" text=\"Purchase Salient\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'Shortcode Generator', '', 'draft', 'open', 'open', '', 'shortcode-generator', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://localhost/wordpress-3-6/?page_id=767', 25, 'page', '', 0),
(770, 1, '2013-12-09 00:05:09', '2013-12-09 00:05:09', '[full_width_section parallax_bg=\"true\" bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/youtube-bg.png\" top_padding=\"150\" bottom_padding=\"150\" type=\"full_width_background\" text_align=\"center\"][one_whole centered_text=\"true\"][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/tn-for-youtube.png\"] [/image_with_animation][vc_column_text]\n<h2 style=\"text-align: center;\">Videos Get Posted For Every Major Release</h2>\n<h4 class=\"light\" style=\"text-align: center;\">Stop feeling overwhelmed by long text documents without any visual instructions.</h4>\n<h4 class=\"light\" style=\"text-align: center;\">All of the videos ThemeNectar posts also come narrated throughout the</h4>\n<h4 class=\"light\" style=\"text-align: center;\">entire duration to ensure your learning experience is a breeze!</h4>\n[/vc_column_text][/one_whole][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"50\" bottom_padding=\"50\" background_color=\"#fbaa5c\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Check out The ThemeNectar Youtube Channel Now [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://www.youtube.com/user/ThemeNectar\" text=\"Onward to the videos!\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'HD Video Series', '', 'draft', 'open', 'open', '', 'hd-video-series', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://localhost/wordpress-3-6/?page_id=770', 24, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(965, 1, '2014-01-17 19:06:47', '2014-01-17 19:06:47', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"center\" top_padding=\"80\" bottom_padding=\"80\" bg_image=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-bg.jpg\"][vc_column width=\"1/1\" enable_animation=\"true\" animation=\"fade-in\" delay=\"400\"][divider line_type=\"No Line\" custom_height=\"30\"][image_with_animation image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-3.png\" img_link_target=\"_self\" animation=\"Fade In\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"center\" bg_color=\"#112023\" top_padding=\"80\" bg_image=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/stars1.png\" bottom_padding=\"80\" parallax_bg=\"true\"][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-diamond\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"30\" subject=\"Drag &amp; Drop Elements\" symbol_position=\"before\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"300\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-stopwatch\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"100\" subject=\"Development Hours\" symbol=\"+\" symbol_position=\"after\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"600\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-drink\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"2\" subject=\"More Convenient\" symbol=\"x\" symbol_position=\"after\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"900\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"linecon-icon-banknote\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"25\" subject=\"Plugin Value\" symbol=\"$\" symbol_position=\"before\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#ffffff\" text_color=\"dark\" text_align=\"center\" top_padding=\"80\" bottom_padding=\"80\"][vc_column width=\"1/1\" enable_animation=\"true\" animation=\"fade-in\"][image_with_animation image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-2.png\" img_link_target=\"_self\" animation=\"Fade In\"][/vc_column][/vc_row]', 'Page Builder', '', 'draft', 'open', 'open', '', 'page-builder', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://themenectar.com/demo/salient-frostwave/?page_id=965', 22, 'page', '', 0),
(1239, 1, '2013-01-11 04:31:39', '2013-01-11 04:31:39', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et consectetur erat, ac iaculis lectus. Vestibulum porta leo vel dignissim imperdiet. Ut consectetur nisi eros, sed consectetur magna egestas id. Vivamus tincidunt faucibus ante, vel rhoncus sapien egestas nec. Nulla ultrices, dolor et dignissim adipiscing, tellus diam tempus mi, vitae egestas purus ligula eget risus. Cras scelerisque augue sapien, vitae lacinia sapien rhoncus vitae. Aliquam quis enim in nulla ullamcorper consequat. Vivamus sollicitudin tortor augue, sed fringilla neque commodo eu. Vestibulum hendrerit massa a nulla gravida ullamcorper. Vestibulum sodales magna nec lorem feugiat, ac interdum justo gravida. Duis et iaculis enim. Aenean aliquet interdum mi, sit amet dapibus est porttitor in. In eget dolor egestas, commodo ante eget, lacinia dui. Praesent at tristique erat. Proin luctus tortor et lectus gravida varius.\n\nCras rhoncus metus quis diam malesuada vulputate. Duis euismod lectus id varius interdum. Etiam quis tincidunt mi. Praesent aliquam purus at purus blandit auctor. Etiam pharetra tellus sem, in laoreet turpis euismod sed. Ut massa lectus, volutpat non convallis ut, hendrerit quis dui. In nec lacinia sapien. Proin placerat tincidunt orci, scelerisque imperdiet ante laoreet et. Fusce porta ante eget tortor sagittis ultrices. In consectetur fringilla urna, non fringilla elit consequat at. Nam vel porta lectus. Donec condimentum hendrerit erat, gravida feugiat dolor accumsan vehicula. Sed vel laoreet elit.\n\nMauris lacinia velit augue, vel pellentesque sem ultrices ut. Curabitur lobortis leo ultrices velit interdum, sed placerat urna mollis. Curabitur ullamcorper odio et sapien pharetra, non adipiscing magna faucibus. Vestibulum bibendum a magna vel eleifend. Etiam malesuada commodo tortor eu convallis. Nunc enim nisi, porta et dolor nec, porttitor tempor leo. Sed eu rhoncus risus. Aenean eu luctus lectus, vitae semper leo. Sed porta, nunc sit amet scelerisque fermentum, lacus enim elementum tortor, sed molestie nulla diam a leo. Vestibulum iaculis dolor ligula, gravida rhoncus odio sollicitudin eu. Nunc ultrices nunc ut odio feugiat fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dolor augue, suscipit eu dui quis, commodo tincidunt turpis. Nulla facilisi. Proin aliquet rhoncus ipsum eget convallis. Aliquam pretium vestibulum viverra.', 'Velit Porttito', 'Quisque porta ipsum quis neque elementum lacinia. Pellentesque ut risus rutrum, tristique lacus nec, mollis risus. Vestibulum mollis erat arcu, eu vehicula purus consequat nec.  Maecenas nunc odio, pulvinar id vulputate nec, porttitor at quam. Suspendisse vulputate diam eu leo bibendum feugiat.', 'publish', 'open', 'open', '', 'velit-feugiat-porttito', '', '', '2013-01-11 04:31:39', '2013-01-11 04:31:39', '', 0, 'http://themenectar.com/demo/salient/?p=1239', 11, 'post', '', 0),
(1251, 1, '2013-01-07 22:47:29', '2013-01-07 22:47:29', 'Quisque luctus nec sem quis vestibulum. Sed neque est, ornare eu laoreet in, molestie vitae augue. <!--more--> Donec porta eros sit amet consequat bibendum. Duis sit amet augue urna. Aliquam a vulputate neque, sit amet pretium sapien. Suspendisse sagittis odio id lacus dignissim, in auctor felis rutrum.\n\nCras sed lobortis libero. Pellentesque arcu lacus, dignissim ut porta in, interdum vel risus. Curabitur non est purus. Ut adipiscing purus augue, quis elementum dolor convallis id. Duis vitae sodales massa, et pharetra massa. Phasellus consectetur neque non ante ultricies rhoncus. Fusce ac rhoncus lorem. Vivamus molestie erat metus, at aliquam ipsum ornare sed. Nunc eu auctor enim. Cras in risus eu velit feugiat porttitor ut non risus. Nulla vel est diam. Proin commodo ligula eu elit porta, non viverra justo ullamcorper. Curabitur at tellus erat. Vivamus tincidunt congue felis, aliquam fringilla nunc bibendum laoreet. Fusce fermentum posuere tincidunt.\n<blockquote>Etiam vehicula nisi id dui consequat, non pellentesque diam ornare. Nulla turpis dui, vehicula nec tortor nec, molestie aliquet arcu.</blockquote>\nVivamus rhoncus ante velit, ac lobortis lectus tristique ut. Duis rhoncus lectus a urna hendrerit fringilla. Sed cursus mollis mattis. Praesent volutpat euismod nisi, vel elementum mi. Maecenas id velit nec massa rutrum dictum. Aenean eget massa pretium elit congue accumsan et eu turpis. Morbi imperdiet viverra leo, non imperdiet dolor fermentum nec. Nam pretium volutpat viverra. Donec a gravida magna, at gravida metus. Cras hendrerit tincidunt tellus, sit amet semper purus tempus at. Nullam dignissim venenatis nulla eget congue. Etiam vehicula nisi id dui consequat, non pellentesque diam ornare. Nulla turpis dui, vehicula nec tortor nec, molestie aliquet arcu. Mauris bibendum condimentum libero, eget posuere enim tincidunt eget. Maecenas aliquam tempus sem, sed lacinia ligula accumsan quis. Fusce turpis neque, auctor sed pellentesque quis, pretium eu ligula.', 'Facilisis Elementum', '', 'publish', 'open', 'open', '', 'facilisis-nulla', '', '', '2013-01-07 22:47:29', '2013-01-07 22:47:29', '', 0, 'http://themenectar.com/demo/salient/?p=1251', 10, 'post', '', 0),
(2034, 1, '2014-03-30 20:54:11', '2014-03-30 20:54:11', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column layout=\"masonry-blog-full-screen-width\" category=\"all\" posts_per_page=\"20\" width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"all\" pagination_type=\"infinite_scroll\" posts_per_page=\"30\"][/vc_column][/vc_row]', 'Blog Masonry Fullwidth', '', 'draft', 'open', 'open', '', 'blog-masonry-fullwidth', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2034', 17, 'page', '', 0),
(2036, 1, '2014-03-30 20:54:51', '2014-03-30 20:54:51', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"80\" bottom_padding=\"80\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-fullwidth\" category=\"all\" pagination_type=\"default\" posts_per_page=\"30\"][/vc_column][/vc_row]', 'Blog Masonry No Sidebar', '', 'draft', 'open', 'open', '', 'blog-masonry-no-sidebar', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2036', 16, 'page', '', 0),
(2038, 1, '2014-03-30 20:55:41', '2014-03-30 20:55:41', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"40\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-sidebar\" category=\"fashion,food-for-thought,gaming,music,uncategorized\" pagination_type=\"default\" posts_per_page=\"15\"][/vc_column][/vc_row]', 'Blog Masonry Sidebar', '', 'draft', 'open', 'open', '', 'blog-masonry-sidebar', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2038', 15, 'page', '', 0),
(2040, 1, '2014-03-30 20:56:08', '2014-03-30 20:56:08', '[nectar_blog enable_pagination=\"true\" layout=\"std-blog-sidebar\" category=\"all\" posts_per_page=\"4\"]', 'Blog Standard', '', 'draft', 'open', 'open', '', 'blog-standard', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2040', 14, 'page', '', 0),
(2092, 1, '2014-03-31 03:53:02', '2014-03-31 03:53:02', '[vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" bg_color=\"#000000\" id=\"locations\"][vc_column width=\"1/2\" animation=\"flip-in\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color=\"#0a0a0a\" background_color_opacity=\"1\"][nectar_gmap size=\"650\" map_center_lat=\"41.821886\" map_center_lng=\"12.579260\" zoom=\"14\" enable_zoom=\"1\" map_markers=\"41.814978|12.547846|Our awesome location\n41.807429|12.603979|Don\'t judge us for owning so many locations\n41.824445|12.656164|You can have unlimited markers on your map!\n41.827259|12.517633|Don\'t judge us for owning so many locations\n41.830840|12.597799|You can have unlimited markers on your map!\n41.821886|12.579260|You can have unlimited markers on your map!\" map_greyscale=\"1\" marker_image=\"3184\" marker_animation=\"1\" ultra_flat=\"1\" dark_color_scheme=\"1\"][/vc_column][vc_column width=\"1/2\" animation=\"flip-in\" delay=\"0\" column_padding=\"padding-4-percent\" column_padding_position=\"all\" background_color=\"#9a4fff\" background_color_opacity=\"1\"][contact-form-7 id=\"2648\"][/vc_column][/vc_row]', 'Contact', '', 'draft', 'open', 'open', '', 'contact', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2092', 13, 'page', '', 0),
(2326, 1, '2015-02-22 16:24:08', '2015-02-22 16:24:08', 'Nullam ornare, sem in malesuada sagittis, quam sapien ornare massa, id pulvinar quam augue vel orci. Praesent leo orci, cursus ac malesuada et, sollicitudin eu erat. Pellentesque ornare mi vitae sem consequat ac bibendum neque adipiscing. Donec tellus nunc, tincidunt sed faucibus a, mattis eget purus. <!--more-->\n\nNunc ipsum orci, consectetur in euismod id, adipiscing nec libero. Vivamus sed nisi quam. Donec id arcu non libero pellentesque condimentum at in mauris. Duis et lacus lectus, eu aliquet tortor. Maecenas cursus consectetur tellus non lobortis. Donec sed arcu a justo cursus varius ut et diam. Suspendisse lobortis pulvinar velit, id convallis eros pulvinar ac. Cras a lorem lorem, et feugiat leo. Nunc vestibulum venenatis est nec tempor. Nunc mattis sem in mauris posuere aliquam.', 'Portit mollis vitae', '', 'publish', 'open', 'open', '', 'porttitor-porttitor-mollis-vitae-placerat-2', '', '', '2015-02-22 16:24:08', '2015-02-22 16:24:08', '', 0, 'http://themenectar.com/demo/salient/?p=87', 8, 'post', '', 0),
(2327, 1, '2013-03-28 14:18:41', '2013-03-28 14:18:41', '', 'Ambrose Redmoon', '', 'publish', 'open', 'open', '', 'ambrose-redmoon', '', '', '2013-03-28 14:18:41', '2013-03-28 14:18:41', '', 0, 'http://themenectar.com/demo/salient/?p=106', 4, 'post', '', 0),
(2328, 1, '2013-03-21 17:45:17', '2013-03-21 17:45:17', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ullamcorper suscipit mi, id convallis risus ullamcorper eget. Curabitur ultricies elit lacinia arcu ullamcorper adipiscing. Integer velit dui, gravida semper commodo vel <!--more-->\n\nNunc interdum porta ligula, eu malesuada nunc tristique eu. Integer sed mi ac velit congue vulputate sed quis lacus. Fusce tellus sem, ultricies consequat porta at, ultrices in odio. Nullam a sapien vitae erat porta faucibus rhoncus eleifend dolor. Mauris semper rutrum ante a auctor. Nullam non arcu erat, vel sollicitudin felis. Nam erat nisl, mattis euismod lacinia at, tempus quis nunc.\n\nQuisque at dolor venenatis justo fringilla dignissim ut id eros. Quisque non elit id purus feugiat vestibulum. Phasellus eget sodales neque. Morbi eget odio nec justo consequat gravida. Phasellus dolor nisl, venenatis eget euismod et, dapibus et purus. Maecenas interdum nisi a dolor facilisis eu laoreet mi facilisis. Mauris pharetra interdum lorem eu venenatis. Praesent est diam, fringilla in hendrerit vel, ullamcorper et mauris. Vivamus risus mi, lobortis ut congue vitae, vestibulum vitae augue. Maecenas nunc odio, pulvinar id vulputate nec, porttitor at quam. Suspendisse vulputate diam eu leo bibendum feugiat. Integer luctus orci a nunc consequat eleifend. Nam tempus quam sed felis tristique faucibus. Aliquam facilisis vehicula malesuada.', 'Magna Quis', '', 'publish', 'open', 'open', '', 'magna-fringilla-quis-condimentum', '', '', '2013-03-21 17:45:17', '2013-03-21 17:45:17', '', 0, 'http://themenectar.com/demo/salient/?p=124', 6, 'post', '', 0),
(2329, 1, '2013-01-23 18:06:42', '2013-01-23 18:06:42', 'Quisque at dolor venenatis justo fringilla dignissim ut id eros. Quisque non elit id purus feugiat vestibulum. Phasellus eget sodales neque. <!--more--> Morbi eget odio nec justo consequat gravida. Phasellus dolor nisl, venenatis eget euismod et, dapibus et purus. Maecenas interdum nisi a dolor facilisis eu laoreet mi facilisis. Mauris pharetra interdum lorem eu venenatis. Praesent est diam, fringilla in hendrerit vel, ullamcorper et mauris.\n\nInteger convallis, odio ut rutrum euismod, mi purus pulvinar justo, quis mollis metus metus vitae nibh. Proin eget tincidunt arcu. Donec ante mi, elementum non adipiscing vitae, pharetra quis mauris. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur eget nibh non odio iaculis posuere. Sed ante tortor, pharetra vitae iaculis id, sodales ac tellus. Ut viverra, nulla et adipiscing condimentum, libero nisi condimentum tellus, vel pharetra neque ligula sit amet mi. Sed rutrum consectetur purus ac tincidunt.', 'Interdum', '', 'publish', 'open', 'open', '', 'mauris-pharetra-interdum-lorem-2', '', '', '2013-01-23 18:06:42', '2013-01-23 18:06:42', '', 0, 'http://themenectar.com/demo/salient/?p=144', 13, 'post', '', 0),
(2563, 1, '2014-05-20 07:14:08', '2014-05-20 07:14:08', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"all\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" posts_per_page=\"10\"][/vc_column][/vc_row]', 'Blog Infinite Scroll', '', 'draft', 'open', 'open', '', 'blog-infinite-scroll', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 0, 'http://themenectar.com/demo/salient-frostwave/?page_id=2543', 4, 'page', '', 0),
(2671, 1, '2013-01-15 06:23:38', '2013-01-15 06:23:38', '[vc_row][vc_column][vc_column_text]Sed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci. Donec at blandit augue, nec tristique ligula. Donec euismod nulla at erat fringilla, a porttitor lorem condimentum. Aliquam ut interdum libero, at pharetra orci. Quisque fermentum, felis ut gravida ornare, eros neque egestas dui, quis sollicitudin sem sem non ante. Fusce metus lorem, scelerisque ut diam non, vehicula dapibus sapien. Sed sagittis, ante at ornare pellentesque, sem justo consequat enim, quis adipiscing quam enim ac velit. Vestibulum condimentum tempus sapien eu porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque interdum odio vel consectetur dignissim. Fusce eget tristique nulla.\n\nDuis at purus ut est cursus imperdiet. Sed condimentum massa in enim cursus, sed mattis elit malesuada. Nunc tincidunt magna sit amet metus sagittis tempor. Maecenas lacinia condimentum ultricies. Sed et lacus nec augue venenatis interdum. Fusce sit amet luctus augue. Aenean scelerisque porttitor ultrices. Nulla dui tellus, egestas sit amet posuere sit amet, dictum tristique mauris. Praesent sed elementum mauris. Nullam et enim eget nisl eleifend auctor lobortis et dui. Curabitur at malesuada magna. Nunc nec tincidunt mauris, vitae tincidunt enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ante leo, bibendum at metus in, dignissim sodales nisi. Curabitur id augue iaculis, elementum diam eget, ultrices massa. Fusce eleifend tincidunt neque, a porta nibh faucibus non.[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column width=\"1/2\"][image_with_animation image_url=\"2261\" animation=\"Fade In\" img_link_target=\"_self\"][/vc_column][vc_column width=\"1/2\"][image_with_animation image_url=\"2262\" animation=\"Fade In\" img_link_target=\"_self\" delay=\"100\"][/vc_column][/vc_row][vc_row][vc_column width=\"1/3\"][image_with_animation image_url=\"2260\" animation=\"Fade In\" img_link_target=\"_self\"][/vc_column][vc_column width=\"1/3\"][image_with_animation image_url=\"2255\" animation=\"Fade In\" img_link_target=\"_self\" delay=\"100\"][/vc_column][vc_column width=\"1/3\"][image_with_animation image_url=\"2260\" animation=\"Fade In\" img_link_target=\"_self\" delay=\"200\"][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]\n\nDuis at purus ut est cursus imperdiet. Sed condimentum massa in enim cursus, sed mattis elit malesuada. Nunc tincidunt magna sit amet metus sagittis tempor. Maecenas lacinia condimentum ultricies. Sed et lacus nec augue venenatis interdum. Fusce sit amet luctus augue. Aenean scelerisque porttitor ultrices. Nulla dui tellus, egestas sit amet posuere sit amet, dictum tristique mauris. Praesent sed elementum mauris. Nullam et enim eget nisl eleifend auctor lobortis et dui. Curabitur at malesuada magna. Nunc nec tincidunt mauris, vitae tincidunt enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ante leo, bibendum at metus in, dignissim sodales nisi. Curabitur id augue iaculis, elementum diam eget, ultrices massa. Fusce eleifend tincidunt neque, a porta nibh faucibus non.[/vc_column_text][/vc_column][/vc_row]', 'Ownage In The Mountains', 'Sed condimentum massa in enim cursus, sed mattis elit malesuada. Lorem sapien acveh icula vestibulum, arcu magna aliquet velit. Nunc elementum mattis diam eu aliquam. Phasellus augue nulla, venenatis non hendrerit ac, volutpat sit amet sem. Donec eleifend nulla', 'publish', 'open', 'open', '', 'ownage-in-the-mountains', '', '', '2013-01-15 06:23:38', '2013-01-15 06:23:38', '', 0, 'http://themenectar.com/demo/salient-blog/?p=2671', 2, 'post', '', 0),
(2677, 1, '2013-02-14 06:33:01', '2013-02-14 06:33:01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ullamcorper suscipit mi, id convallis risus ullamcorper eget. Aenean sagittis eros nec eros euismod, quis dapibus leo semper. Mauris rutrum viverra adipiscing. <!--more--> Praesent ac lectus est. Quisque neque massa, interdum sed mollis vitae, suscipit at tellus. Praesent porta urna dolor, vel rutrum quam facilisis at. Duis consequat sapien et tortor pretium, vitae feugiat augue rutrum. Pellentesque quis aliquam est, vel vulputate purus. In hac habitasse platea dictumst. Maecenas aliquet blandit semper. Ut quis felis sapien. Aenean laoreet dolor ac mattis euismod.\n\nSed sit amet sem dignissim, sollicitudin ante vel, consequat neque. Nunc justo lacus, rhoncus ut interdum nec, iaculis mattis orci. Donec at blandit augue, nec tristique ligula. Donec euismod nulla at erat fringilla, a porttitor lorem condimentum. Aliquam ut interdum libero, at pharetra orci. Quisque fermentum, felis ut gravida ornare, eros neque egestas dui, quis sollicitudin sem sem non ante. Fusce metus lorem, scelerisque ut diam non, vehicula dapibus sapien. Sed sagittis, ante at ornare pellentesque, sem justo consequat enim, quis adipiscing quam enim ac velit. Vestibulum condimentum tempus sapien eu porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque interdum odio vel consectetur dignissim. Fusce eget tristique nulla.\n\nDuis at purus ut est cursus imperdiet. Sed condimentum massa in enim cursus, sed mattis elit malesuada. Nunc tincidunt magna sit amet metus sagittis tempor. Maecenas lacinia condimentum ultricies. Sed et lacus nec augue venenatis interdum. Fusce sit amet luctus augue. Aenean scelerisque porttitor ultrices. Nulla dui tellus, egestas sit amet posuere sit amet, dictum tristique mauris. Praesent sed elementum mauris. Nullam et enim eget nisl eleifend auctor lobortis et dui. Curabitur at malesuada magna. Nunc nec tincidunt mauris, vitae tincidunt enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ante leo, bibendum at metus in, dignissim sodales nisi. Curabitur id augue iaculis, elementum diam eget, ultrices massa. Fusce eleifend tincidunt neque, a porta nibh faucibus non.', 'Be My Guest', '', 'publish', 'open', 'open', '', 'be-my-guest', '', '', '2013-02-14 06:33:01', '2013-02-14 06:33:01', '', 0, 'http://themenectar.com/demo/salient-blog/?p=2677', 1, 'post', '', 0),
(3145, 1, '2013-01-08 23:17:36', '2013-01-08 23:17:36', 'Quisque luctus nec sem quis vestibulum. Sed neque est, ornare eu laoreet in, molestie vitae augue.<!--more-->Donec porta eros sit amet consequat bibendum. Duis sit amet augue urna. Aliquam a vulputate neque, sit amet pretium sapien. Suspendisse sagittis odio id lacus dignissim, in auctor felis rutrum.\n\nCras sed lobortis libero. Pellentesque arcu lacus, dignissim ut porta in, interdum vel risus. Curabitur non est purus. Ut adipiscing purus augue, quis elementum dolor convallis id. Duis vitae sodales massa, et pharetra massa. Phasellus consectetur neque non ante ultricies rhoncus. Fusce ac rhoncus lorem. Vivamus molestie erat metus, at aliquam ipsum ornare sed. Nunc eu auctor enim. Cras in risus eu velit feugiat porttitor ut non risus. Nulla vel est diam. Proin commodo ligula eu elit porta, non viverra justo ullamcorper. Curabitur at tellus erat. Vivamus tincidunt congue felis, aliquam fringilla nunc bibendum laoreet. Fusce fermentum posuere tincidunt.\n<blockquote>Etiam vehicula nisi id dui consequat, non pellentesque diam ornare. Nulla turpis dui, vehicula nec tortor nec, molestie aliquet arcu.</blockquote>\nVivamus rhoncus ante velit, ac lobortis lectus tristique ut. Duis rhoncus lectus a urna hendrerit fringilla. Sed cursus mollis mattis. Praesent volutpat euismod nisi, vel elementum mi. Maecenas id velit nec massa rutrum dictum. Aenean eget massa pretium elit congue accumsan et eu turpis. Morbi imperdiet viverra leo, non imperdiet dolor fermentum nec. Nam pretium volutpat viverra. Donec a gravida magna, at gravida metus. Cras hendrerit tincidunt tellus, sit amet semper purus tempus at. Nullam dignissim venenatis nulla eget congue. Etiam vehicula nisi id dui consequat, non pellentesque diam ornare. Nulla turpis dui, vehicula nec tortor nec, molestie aliquet arcu. Mauris bibendum condimentum libero, eget posuere enim tincidunt eget. Maecenas aliquam tempus sem, sed lacinia ligula accumsan quis. Fusce turpis neque, auctor sed pellentesque quis, pretium eu ligula.', 'Donec Porta', '', 'publish', 'open', 'open', '', 'donec-porta', '', '', '2013-01-08 23:17:36', '2013-01-08 23:17:36', '', 0, 'http://themenectar.com/demo/salient-blog/?p=3145', 0, 'post', '', 0),
(3146, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Header Layouts', '', 'publish', 'closed', 'closed', '', 'header-layouts', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/header-layouts/', 10, 'nav_menu_item', '', 0),
(3147, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Column System', '', 'publish', 'closed', 'closed', '', 'column-system', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/column-system/', 11, 'nav_menu_item', '', 0),
(3148, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3148', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3148/', 8, 'nav_menu_item', '', 0),
(3149, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3149', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3149/', 12, 'nav_menu_item', '', 0),
(3150, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/home/', 1, 'nav_menu_item', '', 0),
(3151, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3151', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3151/', 20, 'nav_menu_item', '', 0),
(3152, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3152', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3152/', 21, 'nav_menu_item', '', 0),
(3153, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3153', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3153/', 22, 'nav_menu_item', '', 0),
(3154, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Shortcode Overview', '', 'publish', 'closed', 'closed', '', 'shortcode-overview', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/shortcode-overview/', 9, 'nav_menu_item', '', 0),
(3155, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3155', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/3155/', 19, 'nav_menu_item', '', 0),
(3156, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Standard', '', 'publish', 'closed', 'closed', '', 'standard', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/standard/', 40, 'nav_menu_item', '', 0),
(3157, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Masonry Sidebar', '', 'publish', 'closed', 'closed', '', 'masonry-sidebar', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-sidebar/', 37, 'nav_menu_item', '', 0),
(3158, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Masonry No Sidebar', '', 'publish', 'closed', 'closed', '', 'masonry-no-sidebar', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-no-sidebar/', 35, 'nav_menu_item', '', 0),
(3159, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Masonry Fullwidth', '', 'publish', 'closed', 'closed', '', 'masonry-fullwidth', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-fullwidth/', 34, 'nav_menu_item', '', 0),
(3160, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', ' ', '', '', 'publish', 'closed', 'closed', '', '3160', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/3160/', 41, 'nav_menu_item', '', 0),
(3162, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'Masonry Infinite Scroll', '', 'publish', 'closed', 'closed', '', 'masonry-infinite-scroll', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/masonry-infinite-scroll/', 36, 'nav_menu_item', '', 0),
(3163, 1, '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2019-05-14 02:53:45', '2019-05-14 02:53:45', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/14/about/', 2, 'nav_menu_item', '', 0),
(3165, 1, '2019-05-14 02:57:40', '2019-05-14 02:57:40', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h2>If you wish to make an apple</h2>\r\n<h2>pie from scratch, you must first</h2>\r\n<h2>invent the universe.</h2>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1485638236800{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider flexible_slider_height=\"true\" full_width=\"true\" overall_style=\"directional\" bg_animation=\"ken_burns\" button_sizing=\"regular\" location=\"Home\" slider_height=\"350\" min_slider_height=\"350\"][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-14 02:57:40', '2019-05-14 02:57:40', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/14/6-revision-v1/', 0, 'revision', '', 0),
(3166, 1, '2019-05-14 03:06:44', '2019-05-14 03:06:44', '[vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider flexible_slider_height=\"true\" full_width=\"true\" overall_style=\"directional\" bg_animation=\"ken_burns\" button_sizing=\"regular\" location=\"Home\" slider_height=\"350\" min_slider_height=\"350\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h2>If you wish to make an apple</h2>\r\n<h2>pie from scratch, you must first</h2>\r\n<h2>invent the universe.</h2>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1485638236800{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-14 03:06:44', '2019-05-14 03:06:44', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/14/6-revision-v1/', 0, 'revision', '', 0),
(3167, 1, '2019-05-14 04:49:37', '2019-05-14 04:49:37', '', 'last-seen-title-master400sx90', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master400sx90', '', '', '2019-05-14 04:49:37', '2019-05-14 04:49:37', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master400sx90.png', 0, 'attachment', 'image/png', 0),
(3168, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8888/qei_lastseen/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2, 'http://localhost:8888/qei_lastseen/2019/05/14/2-revision-v1/', 0, 'revision', '', 0),
(3169, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"all\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" posts_per_page=\"10\"][/vc_column][/vc_row]', 'Blog Infinite Scroll', '', 'inherit', 'closed', 'closed', '', '2563-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2563, 'http://localhost:8888/qei_lastseen/2019/05/14/2563-revision-v1/', 0, 'revision', '', 0),
(3170, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" bg_color=\"#000000\" id=\"locations\"][vc_column width=\"1/2\" animation=\"flip-in\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color=\"#0a0a0a\" background_color_opacity=\"1\"][nectar_gmap size=\"650\" map_center_lat=\"41.821886\" map_center_lng=\"12.579260\" zoom=\"14\" enable_zoom=\"1\" map_markers=\"41.814978|12.547846|Our awesome location\n41.807429|12.603979|Don\'t judge us for owning so many locations\n41.824445|12.656164|You can have unlimited markers on your map!\n41.827259|12.517633|Don\'t judge us for owning so many locations\n41.830840|12.597799|You can have unlimited markers on your map!\n41.821886|12.579260|You can have unlimited markers on your map!\" map_greyscale=\"1\" marker_image=\"3184\" marker_animation=\"1\" ultra_flat=\"1\" dark_color_scheme=\"1\"][/vc_column][vc_column width=\"1/2\" animation=\"flip-in\" delay=\"0\" column_padding=\"padding-4-percent\" column_padding_position=\"all\" background_color=\"#9a4fff\" background_color_opacity=\"1\"][contact-form-7 id=\"2648\"][/vc_column][/vc_row]', 'Contact', '', 'inherit', 'closed', 'closed', '', '2092-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2092, 'http://localhost:8888/qei_lastseen/2019/05/14/2092-revision-v1/', 0, 'revision', '', 0),
(3171, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[nectar_blog enable_pagination=\"true\" layout=\"std-blog-sidebar\" category=\"all\" posts_per_page=\"4\"]', 'Blog Standard', '', 'inherit', 'closed', 'closed', '', '2040-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2040, 'http://localhost:8888/qei_lastseen/2019/05/14/2040-revision-v1/', 0, 'revision', '', 0),
(3172, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"40\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-sidebar\" category=\"fashion,food-for-thought,gaming,music,uncategorized\" pagination_type=\"default\" posts_per_page=\"15\"][/vc_column][/vc_row]', 'Blog Masonry Sidebar', '', 'inherit', 'closed', 'closed', '', '2038-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2038, 'http://localhost:8888/qei_lastseen/2019/05/14/2038-revision-v1/', 0, 'revision', '', 0),
(3173, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"80\" bottom_padding=\"80\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-fullwidth\" category=\"all\" pagination_type=\"default\" posts_per_page=\"30\"][/vc_column][/vc_row]', 'Blog Masonry No Sidebar', '', 'inherit', 'closed', 'closed', '', '2036-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2036, 'http://localhost:8888/qei_lastseen/2019/05/14/2036-revision-v1/', 0, 'revision', '', 0),
(3174, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column layout=\"masonry-blog-full-screen-width\" category=\"all\" posts_per_page=\"20\" width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"all\" pagination_type=\"infinite_scroll\" posts_per_page=\"30\"][/vc_column][/vc_row]', 'Blog Masonry Fullwidth', '', 'inherit', 'closed', 'closed', '', '2034-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 2034, 'http://localhost:8888/qei_lastseen/2019/05/14/2034-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3175, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"bottom\" text_color=\"light\" text_align=\"left\" top_padding=\"21%\" bottom_padding=\"19%\" mouse_based_parallax_bg=\"true\" layer_one_image=\"3123\" layer_one_strength=\"0.3\" layer_two_image=\"3124\" layer_two_strength=\"0.4\" layer_three_image=\"3125\" layer_three_strength=\"0.5\"][vc_column width=\"1/1\"][vc_column_text]\n<h1 class=\"jumbo\">Hello</h1>\n<h2><i>We are Salient. It\'s nice to meet you!\n</i></h2>\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#fbfbfb\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"0\" top_padding=\"0\" vertically_center_columns=\"true\"][vc_column width=\"2/3\" animation=\"fade-in\" delay=\"0\" column_padding=\"padding-5-percent\" background_color=\"#f7f7f7\" background_color_opacity=\"1\"][vc_row_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-atom\" color=\"Accent-Color\"]\n<h4>Web Development</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"200\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-email2\" color=\"Accent-Color\"]\n<h4>Social Marketing</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"400\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-diamond\" color=\"Accent-Color\"]\n<h4>Graphic Design</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"600\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-cloud\" color=\"Accent-Color\"]\n<h4>Cloud Hosting</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"800\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"linecon-icon-display\" color=\"Accent-Color\"]\n<h4>Web Design</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][vc_column_inner width=\"1/3\" animation=\"fade-in\" column_padding=\"padding-1-percent\" delay=\"1000\"][divider line_type=\"No Line\" custom_height=\"20\"][text-with-icon icon_type=\"font_icon\" icon=\"steadysets-icon-meter\" color=\"Accent-Color\"]\n<h4>Server Optimization</h4>\nPhasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis et.[/text-with-icon][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/3\" animation=\"fade-in\" delay=\"200\" column_padding=\"padding-5-percent\" background_color=\"#ffffff\" background_color_opacity=\"1\"][toggles accordion=\"true\"][toggle title=\"Building your brand\" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][toggle title=\"Working with the best\" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][toggle title=\"Project availability \" color=\"Extra-Color-2\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellent esque cursus arcu id magna et euismod in elem entum purus molestie. Cura bitur et arcu  pellen tesque massa eu nulla conse quat sed porttitor porttitor. Adipiscing condimentum.[/vc_column_text][/toggle][/toggles][/vc_column][/vc_row][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" background_color=\"#fbfbfb\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" vertically_center_columns=\"true\"][vc_column width=\"1/3\" animation=\"none\" column_padding=\"padding-4-percent\" background_color=\"#9a4fff\" background_color_opacity=\"1\" column_padding_position=\"all\"][testimonial_slider][testimonial name=\"apawlik, Theme User\" quote=\"It\'s easy to see why this is one of the most downloaded themes on themeforest. It\'s a real game-changer, and a breathe of fresh air if you work with WordPress a lot.\" id=\"t1\" title=\"Testimonial\" tab_id=\"1424829107923-2\"][/testimonial][testimonial name=\"Editor02, Theme User\" quote=\"I literally could not be happier that I chose to buy your theme! Your regular updates and superb attention to detail blows me away every time I visit my new site!\" id=\"t2\" title=\"Testimonial\" tab_id=\"1424829108051-6\"][/testimonial][testimonial name=\"mdriess, Theme User\" quote=\"Amazing work! You didn’t just make a great looking theme, but you made one that is a pleasure to customize and not the least bit difficult to keep looking classy.\" id=\"t3\" title=\"Testimonial\" tab_id=\"1424829108191-9\"][/testimonial][/testimonial_slider][/vc_column][vc_column width=\"2/3\" animation=\"none\" column_padding=\"padding-4-percent\" background_color=\"#5de3dc\" background_color_opacity=\"1\" column_padding_position=\"all\"][clients columns=\"5\" carousel=\"true\"][client title=\"Client\" id=\"1395988736-1-29\" image=\"2905\" name=\"Fluid\" tab_id=\"1424829108349-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395988736-2-10\" image=\"2906\" name=\"squirrel\" tab_id=\"1424829108528-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395989142478-0-0\" image=\"2848\" name=\"Location point\" tab_id=\"1424829108653-0\"][/client][client title=\"Client\" id=\"1395989250503-0-1\" image=\"2847\" name=\"X\" tab_id=\"1424829108792-10\"][/client][client title=\"Client\" id=\"135948736-1-29\" image=\"2850\" name=\"Fluid\" tab_id=\"1424829108932-4\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"1395788736-2-10\" image=\"2849\" name=\"squirrel\" tab_id=\"1424829109073-10\"] Click the edit button to add your testimonial. [/client][client title=\"Client\" id=\"139999142478-0-0\" image=\"2848\" name=\"Location point\" tab_id=\"1424829109215-2\"][/client][client title=\"Client\" id=\"1393989250503-0-1\" image=\"2847\" name=\"X\" tab_id=\"1424829109372-8\"][/client][/clients][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" background_color=\"#171920\"][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2794\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2795\" name=\"Lauren Clark\" job_position=\"Graphic Designer\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2799\" name=\"Andrew Johnson\" job_position=\"Front-end Developer\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"0\" bottom_padding=\"0\" type=\"full_width_content\" text_align=\"left\" parallax_bg=\"true\" background_color=\"#171920\"][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2797\" name=\"Tyler Kendall\" job_position=\"Art Director\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2798\" name=\"Jacob King\" job_position=\"UX Director\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][vc_column width=\"1/3\" enable_animation=\"true\" animation=\"flip-in\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2796\" name=\"Amanda Kliens\" job_position=\"Human Resources\" color=\"Extra-Color-2\" team_memeber_style=\"meta_overlaid\" link_element=\"none\"][/vc_column][/full_width_section][vc_row type=\"full_width_content\" bg_position=\"center center\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" bg_color=\"#f9f9f9\"][vc_column width=\"1/2\" animation=\"none\" column_padding=\"padding-7-percent\" background_color=\"#f9f9f9\" background_color_opacity=\"1\"][vc_column_text]\n<h3 class=\"tiny\">Seeing Is Believing</h3>\n[divider line_type=\"Small Line\" custom_height=\"50\"]\n<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut mi ornare, pretium massa eu, rutrum enim. In in lorem congue, tincidunt justo at, consectetur nulla.</h2>\n<em><a href=\"http://themenectar.com/demo/salient-blog/portfolio-fullwidth-masonry/\">View The Goods</a>\n</em>[/vc_column_text][/vc_column][vc_column width=\"1/2\" animation=\"none\" column_padding=\"padding-7-percent\" background_color=\"#ffffff\" background_color_opacity=\"1\"][vc_column_text]\n<h3 class=\"tiny\">We Blog Too</h3>\n[divider line_type=\"Small Line\" custom_height=\"50\"]\n<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut mi ornare, pretium massa eu, rutrum enim. In in lorem congue, tincidunt justo at, consectetur nulla.</h2>\n<em><a href=\"http://themenectar.com/demo/salient-blog/blog-infinite-scroll/\">Pick Our Brains</a>\n</em>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"padding-2-percent\" background_color_opacity=\"1\" background_color_hover=\"#8446e2\" column_link=\"http://themenectar.com/demo/salient-blog/contact/\" background_color=\"#9a4fff\" column_padding_position=\"all\"][vc_column_text]Let\'s take the next step and work together[/vc_column_text][/vc_column][/vc_row]', 'About Us', '', 'inherit', 'closed', 'closed', '', '596-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 596, 'http://localhost:8888/qei_lastseen/2019/05/14/596-revision-v1/', 0, 'revision', '', 0),
(3176, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"70\" bottom_padding=\"0\" background_color=\"f4f4f4\" type=\"full_width_background\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n\n<img alt=\"header options panel\" src=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/header-options.png\" />[/vc_column_text][/vc_column][/full_width_section][full_width_section parallax_bg=\"true\" bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"65\" bottom_padding=\"65\" type=\"full_width_background\" text_align=\"left\" image_url=\"2404\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Check Out Some Of The Possible Combinations</h2>\n<p style=\"text-align: center;\">Every site should have its own unique design to it. Salient gives you the power you need to truly create something different!</p>\n[/vc_column_text][divider line_type=\"No Line\" custom_height=\"30\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/1.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/2.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/3.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/4.png\"] [/image_with_animation][divider line_type=\"No Line\"][image_with_animation animation=\"Fade In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/5.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"40\" background_color=\"f4f4f4\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Introducing The AJAX Shopping Cart</h2>\n<p style=\"text-align: center;\">Another cool feature that Salient offers is the AJAX shopping cart. When enabled, users will have immediate feedback after adding items to their cart in\nyour WooCommerce shop. They will also have access to view the contents of their cart without ever leaving or refreshing the page.</p>\n<img alt=\"header options panel\" src=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/07/wooCommerce-ajax-cart1.png\" />[/vc_column_text][/vc_column][/full_width_section]', 'Headers', '', 'inherit', 'closed', 'closed', '', '568-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 568, 'http://localhost:8888/qei_lastseen/2019/05/14/568-revision-v1/', 0, 'revision', '', 0),
(3177, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"center\" scene_position=\"center\" layer_one_image=\"2518\" top_padding=\"0\" bottom_padding=\"0\" layer_four_image=\"2655\"][vc_column width=\"1/1\"][nectar_slider location=\"Home\" full_width=\"true\" arrow_navigation=\"true\" desktop_swipe=\"true\" slider_transition=\"fade\" parallax=\"true\" slider_button_styling=\"btn_with_count\" overall_style=\"directional\" button_sizing=\"large\" slider_height=\"650\" flexible_slider_height=\"true\" autorotate=\"5000\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" bg_color=\"#f4f4f4\" top_padding=\"70\" bottom_padding=\"60\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2>We are a creative agency with a passion for design &amp; developing beautiful creations. We\'ve had the privilege to work with some of the largest names in the business and enjoy blogging about our projects and adventures as often as possible</h2>\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\"][vc_column width=\"1/1\"][nectar_blog layout=\"masonry-blog-full-screen-width\" category=\"fashion,food-for-thought,gaming,music,uncategorized\" pagination_type=\"default\" posts_per_page=\"10\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" bg_position=\"left top\" bg_repeat=\"no-repeat\" scene_position=\"center\" text_color=\"light\" text_align=\"center\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"padding-2-percent\" background_color_opacity=\"1\" column_padding_position=\"all\" background_color=\"#9a4fff\" background_color_hover=\"#2d2d2d\" column_link=\"#\"][vc_column_text]View The Full Blog[/vc_column_text][/vc_column][/vc_row]', 'Home - Landing Page', '', 'inherit', 'closed', 'closed', '', '551-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 551, 'http://localhost:8888/qei_lastseen/2019/05/14/551-revision-v1/', 0, 'revision', '', 0),
(3178, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 'Features', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 30, 'http://localhost:8888/qei_lastseen/2019/05/14/30-revision-v1/', 0, 'revision', '', 0),
(3179, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"center\" top_padding=\"80\" bottom_padding=\"80\" bg_image=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-bg.jpg\"][vc_column width=\"1/1\" enable_animation=\"true\" animation=\"fade-in\" delay=\"400\"][divider line_type=\"No Line\" custom_height=\"30\"][image_with_animation image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-3.png\" img_link_target=\"_self\" animation=\"Fade In\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"center\" bg_color=\"#112023\" top_padding=\"80\" bg_image=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/stars1.png\" bottom_padding=\"80\" parallax_bg=\"true\"][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-diamond\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"30\" subject=\"Drag &amp; Drop Elements\" symbol_position=\"before\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"300\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-stopwatch\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"100\" subject=\"Development Hours\" symbol=\"+\" symbol_position=\"after\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"600\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"steadysets-icon-drink\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"2\" subject=\"More Convenient\" symbol=\"x\" symbol_position=\"after\"][/vc_column][vc_column width=\"1/4\" enable_animation=\"true\" animation=\"grow-in\" delay=\"900\"][vc_column_text][icon color=\"Extra-Color-1\" size=\"regular\" image=\"linecon-icon-banknote\"][/vc_column_text][divider line_type=\"No Line\" custom_height=\"10\"][milestone color=\"Default\" number=\"25\" subject=\"Plugin Value\" symbol=\"$\" symbol_position=\"before\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#ffffff\" text_color=\"dark\" text_align=\"center\" top_padding=\"80\" bottom_padding=\"80\"][vc_column width=\"1/1\" enable_animation=\"true\" animation=\"fade-in\"][image_with_animation image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2014/01/page-builder-2.png\" img_link_target=\"_self\" animation=\"Fade In\"][/vc_column][/vc_row]', 'Page Builder', '', 'inherit', 'closed', 'closed', '', '965-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 965, 'http://localhost:8888/qei_lastseen/2019/05/14/965-revision-v1/', 0, 'revision', '', 0),
(3180, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[full_width_section parallax_bg=\"true\" bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/youtube-bg.png\" top_padding=\"150\" bottom_padding=\"150\" type=\"full_width_background\" text_align=\"center\"][one_whole centered_text=\"true\"][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/tn-for-youtube.png\"] [/image_with_animation][vc_column_text]\n<h2 style=\"text-align: center;\">Videos Get Posted For Every Major Release</h2>\n<h4 class=\"light\" style=\"text-align: center;\">Stop feeling overwhelmed by long text documents without any visual instructions.</h4>\n<h4 class=\"light\" style=\"text-align: center;\">All of the videos ThemeNectar posts also come narrated throughout the</h4>\n<h4 class=\"light\" style=\"text-align: center;\">entire duration to ensure your learning experience is a breeze!</h4>\n[/vc_column_text][/one_whole][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"50\" bottom_padding=\"50\" background_color=\"#fbaa5c\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Check out The ThemeNectar Youtube Channel Now [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://www.youtube.com/user/ThemeNectar\" text=\"Onward to the videos!\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'HD Video Series', '', 'inherit', 'closed', 'closed', '', '770-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 770, 'http://localhost:8888/qei_lastseen/2019/05/14/770-revision-v1/', 0, 'revision', '', 0),
(3181, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/shortcode-generator-bg.jpg\" top_padding=\"140\" bottom_padding=\"100\" type=\"full_width_background\" text_align=\"center\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 style=\"text-align: center;\">Nectar Shortcodes Come In a Visually Intuitive Generator</h2>\n<h4 class=\"light\" style=\"text-align: center;\">This allows you to create beautiful &amp; unique site sections in a matter of sections\nall without memorizing or knowing any shortcode parameters.</h4>\n[/vc_column_text][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/shortcode-generator1.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"50\" bottom_padding=\"50\" background_color=\"#2ac4ea\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Are You Convinced This Is The Right Theme For You? [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://themeforest.net/item/salient-responsive-portfolio-blog-theme/4363266\" text=\"Purchase Salient\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'Shortcode Generator', '', 'inherit', 'closed', 'closed', '', '767-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 767, 'http://localhost:8888/qei_lastseen/2019/05/14/767-revision-v1/', 0, 'revision', '', 0),
(3182, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[full_width_section parallax_bg=\"true\" bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"100\" bottom_padding=\"0\"][vc_column width=\"1/1\" animation=\"none\" centered_text=\"true\" column_padding=\"no-extra-padding\" background_color_opacity=\"1\"][vc_column_text]\n<h2 style=\"text-align: center;\"><span><span style=\"color: #2ac4ea;\">The Control You Desire, All Available At Your Fingertips</span>\n</span></h2>\n<h4 class=\"light\" style=\"text-align: center;\">Experience our user friendly interface loaded with options for everything you\nneed and none of the extra bloat usually seen with such power.</h4>\n[/vc_column_text][image_with_animation animation=\"Grow In\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/09/options-panel.png\"] [/image_with_animation][/vc_column][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"Light\" top_padding=\"50\" bottom_padding=\"30\" background_color=\"#2AC4EA \"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Are You Convinced This Is The Right Theme For You? [button open_new_tab=\"true\" color=\"See-Through\" size=\"medium\" url=\"http://themeforest.net/item/salient-responsive-portfolio-blog-theme/4363266\" text=\"Purchase Salient\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section]', 'Intuitive Options Panel', '', 'inherit', 'closed', 'closed', '', '764-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 764, 'http://localhost:8888/qei_lastseen/2019/05/14/764-revision-v1/', 0, 'revision', '', 0),
(3183, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"40\"][vc_column width=\"1/1\"][pricing_table columns=\"4\"][pricing_column title=\"Basic\" price=\"99\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950167991-0\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>Yes, this too!</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Standard\" highlight=\"true\" highlight_reason=\"Most Popular\" price=\"199\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168041-9\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And this too</li>\n	<li>Maybe even this</li>\n	<li>Nevermind, it\'s not</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Advanced\" price=\"299\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168076-2\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>Even this too</li>\n	<li>Also included</li>\n	<li>You even get this</li>\n	<li>And a little of this</li>\n</ul>\n[button size=\"large\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Premium\" price=\"399\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168111-3\" color=\"Accent-Color\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>Even unlimited this</li>\n	<li>You also get this!</li>\n	<li>Add even some of this</li>\n	<li>Only if you want it</li>\n</ul>\n[button size=\"large\" url=\"http://themenectar.com\" text=\"Sign up now!\"][/pricing_column][/pricing_table][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"20\"][vc_column width=\"1/1\"][heading subtitle=\"Lorem ipsum dolor sit amet, consectetur adipiscing\"] More Pricing Table Examples! [/heading][/vc_column][/vc_row][vc_row][vc_column width=\"1/1\"][pricing_table columns=\"5\"][pricing_column title=\"Free\" price=\"0\" currency_symbol=\"$\" id=\"1389950168281-10\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>But that\'s it</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Light\" price=\"5\" currency_symbol=\"$\" interval=\"Per Week\" id=\"1389950168324-5\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>Even unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Basic\" price=\"10\" currency_symbol=\"$\" interval=\"Per Week\" id=\"1389950168360-1\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And some of  this</li>\n	<li>Even unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Super\" highlight=\"true\" highlight_reason=\"Recommended\" price=\"30\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168396-7\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>And this too</li>\n	<li>Maybe even this</li>\n	<li>[icon size=\"tiny\" image=\"icon-star-empty\"]Unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][pricing_column title=\"Deluxe\" price=\"50\" currency_symbol=\"$\" interval=\"Per Month\" id=\"1389950168435-10\"]\n<ul class=\"features\">\n	<li>This is included</li>\n	<li>You even get this</li>\n	<li>And a lot of this!</li>\n	<li>[icon size=\"tiny\" image=\"icon-star-empty\"]Unlimited this</li>\n</ul>\n[button size=\"medium\" url=\"#\" text=\"Sign up now!\"][/pricing_column][/pricing_table][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"30\"][vc_column width=\"1/2\"][vc_column_text]\n<h2>Are these the only plans?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][vc_column width=\"1/2\"][vc_column_text]\n<h2>Are there any discounts?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"40\"][vc_column width=\"1/2\"][vc_column_text]\n<h2>When will my account be activated?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][vc_column width=\"1/2\"][vc_column_text]\n<h2>Can I cancel at any time?</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta, mi ut facilisis ullamcorper, magna risus vehicula augue, eget faucibus magna massa at justo. Nulla a arcu ut massa hendrerit varius id vitae augue. Sed quis augue ut eros tincidunt hendrerit eu eget nisl. Duis malesuada vehicula massa.[/vc_column_text][/vc_column][/vc_row]', 'Pricing Tables', '', 'inherit', 'closed', 'closed', '', '482-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 482, 'http://localhost:8888/qei_lastseen/2019/05/14/482-revision-v1/', 0, 'revision', '', 0),
(3184, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"no-repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"30\" background_color=\"#fcfcfc\" type=\"full_width_background\" text_align=\"left\" bg_position=\"left top\" scene_position=\"center\"][one_half][toggles][toggle title=\"Toggle Panel #1\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viv erra a, dapibus at dolor. In iaculis vive rra neque, ac eleifend ante lobo rtis id. congue id  [/vc_column_text][/toggle][toggle title=\"Toggle Panel #2\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. [/vc_column_text][/toggle][toggle title=\"Toggle Panel #3\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id.. [/vc_column_text][/toggle][/toggles][/one_half][one_half_last][tabbed_section][tab title=\"Sample Tab #1\" id=\"1396210176255-5\" tab_id=\"1424844241428-6\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][tab title=\"Sample Tab #2\" id=\"1396210195201-9\" tab_id=\"1424844241491-9\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][tab title=\"Sample Tab #3\" id=\"1396210195262-7\" tab_id=\"1424844241547-1\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In neque.[/vc_column_text][/tab][/tabbed_section][/one_half_last][/vc_row][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"90\" background_color=\"#fcfcfc\" type=\"full_width_background\" text_align=\"left\"][one_third centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"small\" url=\"#\" text=\"Small Button\" color=\"extra-color-2\"][/vc_column_text][/one_third][one_third centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"large\" url=\"#\" text=\"Medium Button\" color=\"extra-color-2\"][/vc_column_text][/one_third][one_third_last centered_text=\"true\"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum.\n\n[button size=\"large\" url=\"#\" text=\"Large Button\" color=\"extra-color-2\"][/vc_column_text][/one_third_last][/full_width_section][full_width_section bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"dark\" top_padding=\"90\" bottom_padding=\"50\" background_color=\"#f4f4f4\" type=\"full_width_background\" text_align=\"left\"][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-heart\" color=\"extra-color-1\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-bolt\" color=\"extra-color-2\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large-2\" image=\"icon-leaf\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-key\" color=\"extra-color-1\"][/vc_column_text][/one_sixth][one_sixth centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-lightbulb\" color=\"extra-color-2\"][/vc_column_text][/one_sixth][one_sixth_last centered_text=\"true\"][vc_column_text][icon size=\"large\" image=\"icon-eye-open\" color=\"accent-color\"][/vc_column_text][/one_sixth_last][/full_width_section][full_width_section bg_pos=\"Center Center\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"40\" bottom_padding=\"70\" background_color=\"#f4f4f4\"][three_fourths][image_with_animation animation=\"Fade In From Left\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/03/fontawesome.jpg\"] [/image_with_animation][/three_fourths][one_fourth_last][vc_column_text]\n<h4>[icon size=\"small\" image=\"icon-heart\"] Lorem ipsum dolor sit</h4>\n<h4>[icon size=\"small\" image=\"icon-bolt\"] Rhoncus iaculis et at</h4>\n<h4>[icon size=\"small\" image=\"icon-leaf\"] Curabitur lectus nun</h4>\n<h4>[icon size=\"small\" image=\"icon-key\"] Posuere feugiat risus</h4>\n<h4>[icon size=\"small\" image=\"icon-lightbulb\"] Iaculis et at tortor</h4>\n<h4>[icon size=\"small\" image=\"icon-eye-open\"] Nulla facil Donec</h4>\n[divider line_type=\"Full Width Line\" custom_height=\"50\"][icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-heart\"] Lorem ipsum dolor sit\n[icon size=\"tiny\"  color=\"extra-color-1\" image=\"icon-bolt\"] Rhoncus iaculis et at\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-leaf\"] Curabitur lectus nun\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-key\"] Posuere feugiat risus\n[icon size=\"tiny\" color=\"extra-color-1\" image=\"icon-lightbulb\"] Iaculis et at tortor[/vc_column_text][/one_fourth_last][/full_width_section][full_width_section bg_pos=\"Left Top\" bg_repeat=\"No-Repeat\" text_color=\"light\" top_padding=\"40\" bottom_padding=\"35\" background_color=\"#5de3dc\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][vc_column_text]\n<h2 class=\"light\" style=\"text-align: center;\">Full width color sections anywhere you want! [button color=\"See-Through\" size=\"medium\" url=\"\" text=\"Alt button\" image=\"default-arrow\"]</h2>\n[/vc_column_text][/vc_column][/full_width_section][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"light\" image_url=\"http://themenectar.com/demo/salient/wp-content/uploads/2013/06/elements-testimonials-bg.jpg\" top_padding=\"90\" bottom_padding=\"90\" background_color=\"#fff\" type=\"full_width_background\" text_align=\"left\"][vc_column width=\"1/1\"][testimonial_slider][testimonial name=\"Greg Ross, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec massa placerat, ac aliquam dui. Quisque semper tempus tellus ac lobortis. Aenean consectetur laoreet massa non interdum. Massa non interdum. \" id=\"t1\" title=\"Testimonial\" tab_id=\"1424844241795-8\"][/testimonial][testimonial name=\"Jeff Gemmell, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus erat ac massa placerat, ac aliquam Aenean consectetur laoreet massa non interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. \" id=\"t2\" title=\"Testimonial\" tab_id=\"1424844241877-7\"][/testimonial][testimonial name=\"Mark Levin, ThemeNectar\" quote=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus erat ac massa placerat, ac aliquam dui pulvinar. Quisque semper tempus tellus ac lobortis. Aenean consectetur laoreet massa non interdum. \" id=\"t3\" title=\"Testimonial\" tab_id=\"1424844241906-3\"][/testimonial][/testimonial_slider][/vc_column][/full_width_section][vc_row bg_pos=\"Center Center\" parallax_bg=\"flase\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"70\" background_color=\"#F5F5F5\"][vc_column width=\"1/4\"][milestone color=\"extra-color-1\" number=\"65\" subject=\"Awards Won\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-1\" number=\"37\" subject=\"GitHub Repo\'s\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-2\" number=\"439\" subject=\"Cups of Coffee\"][/vc_column][vc_column width=\"1/4\"][milestone color=\"extra-color-2\" number=\"268\" subject=\"Completed Designs\"][/vc_column][/vc_row][full_width_section bg_pos=\"Center Center\" parallax_bg=\"true\" bg_repeat=\"No-Repeat\" text_color=\"Dark\" top_padding=\"70\" bottom_padding=\"70\" background_color=\"#f1f1f1\"][one_third][vc_column_text]\n<h2>Animated Bar Graph</h2>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie. Curabitur pellentesque massa eu nulla consequat sed porttitor arcu porttitor. Quisque volutpat pharetra felis, eu cursus lorem molestie vitae. Nulla vehicula, lacus ut suscipit fermentum, turpis felis ultricies dui, ut rhoncus libero augue at libero. Morbi ut arcu dolor.  [/vc_column_text][/one_third][two_thirds_last][bar title=\"HTML/CSS\" percent=\"70\" id=\"b1\"][bar title=\"Logo Design\" percent=\"80\" id=\"b2\"][bar title=\"WordPress\" percent=\"100\" id=\"b3\"][bar title=\"Photoshop\" percent=\"90\" id=\"b4\"][bar title=\"Illustrator\" percent=\"75\" id=\"b5\"][/two_thirds_last][/full_width_section][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"80\" bottom_padding=\"30\"][/vc_row][vc_row][vc_column width=\"1/1\"][carousel easing=\"easeInOutQuart\" carousel_title=\"Team Members - Available in Two Styles\" scroll_speed=\"1000\"][item id=\"1389858025982-8\" title=\"Item\" tab_id=\"1424844242054-2\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2319\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026029-3\" title=\"Item\" tab_id=\"1424844242098-10\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2321\" name=\"Carlton Harris\" job_position=\"Developer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026071-1\" title=\"Item\" tab_id=\"1424844242134-9\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2320\" name=\"Amanda Kliens\" job_position=\"Graphic Designer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][item id=\"1389858026114-10\" title=\"Item\" tab_id=\"1424844242169-9\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2322\" name=\"Tyler Johnson\" job_position=\"Front-End Developer\" team_memeber_style=\"meta_below\" link_element=\"none\" color=\"Accent-Color\"][/item][/carousel][/vc_column][/vc_row][vc_row type=\"full_width_background\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"light\" text_align=\"left\" top_padding=\"30\" bottom_padding=\"80\"][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2309\" name=\"Trevor Smith\" job_position=\"Founder / Project Lead\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2305\" name=\"Carlton Harris\" job_position=\"Developer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2307\" name=\"Amanda Kliens\" job_position=\"Graphic Designer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][vc_column width=\"1/4\"][team_member description=\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. \" social=\"Facebook, http://facebook.com, Twitter, http://twitter.com, Youtube, http://youtube.com\" image_url=\"2496\" name=\"Tyler Johnson\" job_position=\"Front-End Developer\" team_memeber_style=\"meta_overlaid\" link_element=\"none\" color=\"Accent-Color\"][/vc_column][/vc_row]', 'Elements', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 207, 'http://localhost:8888/qei_lastseen/2019/05/14/207-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3185, 1, '2019-05-14 05:09:57', '2019-05-14 05:09:57', '[vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][vc_column width=\"1/1\"][heading subtitle=\"The good ol\' standard column, available in a variety of sizes.\"] Standard Columns [/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_half][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half][one_half_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_third][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth][one_sixth_last][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_sixth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" bottom_padding=\"0\"][vc_column width=\"1/1\"][heading subtitle=\"Easily turn any column into a boxed column with one click!\"]Boxed Columns[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Donec Ac Lacus</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Aliquam Ac Urna</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Sed Congue Nunc</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last boxed=\"true\" centered_text=\"true\"][vc_column_text]\n<h4>Consectetur Adipiscing</h4>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"10\"][vc_column width=\"1/1\"][heading subtitle=\"For those looking for a little extra flash in their content.\"] Animated Columns [/heading][/vc_column][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_half animation=\"Fade In From Left\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half][one_half_last animation=\"Fade In From Right\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod fermentum tincidunt. Duis iaculis leo ut urna rutrum aliquam. Proin non fringilla erat. Integer at neque ante, non volutpat lacus. Sed nec ornare augue. Aliquam a lectus enim, eu mattis lorem. Sed suscipit felis ac lectus rutrum eget ultrices libero tempor. Donec posuere justo quis libero rutrum pulvinar. Pellentesque eu arcu et justo adipiscing mattis. Nulla interdum, felis quis tincidunt vehicula, diam quam fringilla lorem, sed euismod ante enim vel lacus. Praesent at dolor mauris, ut imperdiet elit. Donec condimentum condimentum neque, eu sodales ante aliquam nec. [/vc_column_text][/one_half_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_third animation=\"Fade In\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third animation=\"Fade In\" delay=\"300\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third][one_third_last animation=\"Fade In\" delay=\"600\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_third_last][/vc_row][vc_row type=\"in_container\" bg_position=\"left top\" bg_repeat=\"no-repeat\" text_color=\"dark\" text_align=\"left\" top_padding=\"0\" bottom_padding=\"0\"][one_fourth animation=\"Fade In From Bottom\" delay=\"0\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth animation=\"Fade In From Bottom\" delay=\"200\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth animation=\"Fade In From Bottom\" delay=\"400\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth][one_fourth_last animation=\"Fade In From Bottom\" delay=\"600\"][vc_column_text] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac lacus massa. Aliquam ac urna dui, sed sodales ligula. Sed congue nunc vel purus porta vehicula. [/vc_column_text][/one_fourth_last][/vc_row]', 'Columns', '', 'inherit', 'closed', 'closed', '', '205-revision-v1', '', '', '2019-05-14 05:09:57', '2019-05-14 05:09:57', '', 205, 'http://localhost:8888/qei_lastseen/2019/05/14/205-revision-v1/', 0, 'revision', '', 0),
(3186, 1, '2019-05-15 10:11:36', '2019-05-15 10:11:36', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" enable_shape_divider=\"true\" shape_divider_position=\"both\" shape_divider_height=\"100\" bg_image_animation=\"none\" shape_type=\"fan\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h1>What would you want to see</h1>\r\n<h1>one more time if you</h1>\r\n<h1>lost your sight?</h1>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1557912914701{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" project_style=\"9\" item_spacing=\"default\" load_in_animation=\"perspective\" projects_per_page=\"4\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Watch the reel</h2>\r\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" video_external=\"https://www.youtube.com/watch?v=4BfKFCOCJe8\" text_color=\"light\" text_align=\"left\" top_padding=\"15%\" bottom_padding=\"15%\" enable_gradient=\"true\" color_overlay=\"#585b64\" color_overlay_2=\"#2c2f38\" gradient_direction=\"left_to_right\" overlay_strength=\"0.8\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" vertically_center_columns=\"true\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" top_padding=\"5%\" bottom_padding=\"0\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Our sponsors</h2>\r\n[/vc_column_text][divider line_type=\"No Line\" custom_height=\"50\"][clients columns=\"4\" hover_effect=\"opacity\" additional_padding=\"3\" fade_in_animation=\"true\" carousel=\"true\"][client image=\"3216\" title=\"Client\" id=\"1557914669970-4\" tab_id=\"1557914669971-4\" url=\"#\" name=\"Publicis Worldwide\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670041-7\" tab_id=\"1557914670041-7\" url=\"#\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670106-8\" tab_id=\"1557914670107-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670156-4\" tab_id=\"1557914670157-9\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670219-2\" tab_id=\"1557914670219-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670261-9\" tab_id=\"1557914670261-7\" url=\"#\"][/client][/clients][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-autosave-v1', '', '', '2019-05-15 10:11:36', '2019-05-15 10:11:36', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/15/6-autosave-v1/', 0, 'revision', '', 0),
(3187, 1, '2019-05-15 05:18:38', '2019-05-15 05:18:38', '[vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Home\" flexible_slider_height=\"true\" full_width=\"true\" overall_style=\"directional\" bg_animation=\"ken_burns\" button_sizing=\"regular\" slider_height=\"350\" min_slider_height=\"350\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h2>If you wish to make an apple</h2>\r\n<h2>pie from scratch, you must first</h2>\r\n<h2>invent the universe.</h2>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1485638236800{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-15 05:18:38', '2019-05-15 05:18:38', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/15/6-revision-v1/', 0, 'revision', '', 0),
(3190, 1, '2019-05-15 06:04:01', '2019-05-15 06:04:01', '', 'eye-hero-video-mp4', '', 'inherit', 'open', 'closed', '', 'shutterstock_28142716', '', '', '2019-05-15 06:06:53', '2019-05-15 06:06:53', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/shutterstock_28142716.mp4', 0, 'attachment', 'video/mp4', 0),
(3191, 1, '2019-05-15 06:30:30', '2019-05-15 06:30:30', '', 'last-seen-title-master-neg-450x90', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master-neg-450x90', '', '', '2019-05-15 06:30:30', '2019-05-15 06:30:30', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master-neg-450x90.png', 0, 'attachment', 'image/png', 0),
(3192, 1, '2019-05-15 07:00:25', '2019-05-15 07:00:25', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-05-15 07:00:25\"\n    },\n    \"page_on_front\": {\n        \"value\": \"6\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-05-15 07:00:25\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '8d3d06a4-d3a4-4ff1-8c64-d967615f65c6', '', '', '2019-05-15 07:00:25', '2019-05-15 07:00:25', '', 0, 'http://localhost:8888/qei_lastseen/2019/05/15/8d3d06a4-d3a4-4ff1-8c64-d967615f65c6/', 0, 'customize_changeset', '', 0),
(3193, 1, '2019-05-15 08:09:29', '2019-05-15 08:09:29', '', 'Yuma Antoine Decaux', '', 'publish', 'closed', 'closed', '', 'yuma-antoine-decaux-by-kate-mckay', '', '', '2019-05-15 09:26:28', '2019-05-15 09:26:28', '', 0, 'http://localhost:8888/qei_lastseen/?post_type=portfolio&#038;p=3193', 0, 'portfolio', '', 0),
(3194, 1, '2019-05-15 07:17:06', '2019-05-15 07:17:06', '', 'Template: Subject Detail', '', 'inherit', 'closed', 'closed', '', '3193-revision-v1', '', '', '2019-05-15 07:17:06', '2019-05-15 07:17:06', '', 3193, 'http://localhost:8888/qei_lastseen/2019/05/15/3193-revision-v1/', 0, 'revision', '', 0),
(3195, 1, '2019-05-15 07:46:22', '2019-05-15 07:46:22', '', 'Screen Shot 2019-02-25 at 10.18.33 am', '', 'inherit', 'open', 'closed', '', 'screen-shot-2019-02-25-at-10-18-33-am', '', '', '2019-05-15 07:47:08', '2019-05-15 07:47:08', '', 3193, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Screen-Shot-2019-02-25-at-10.18.33-am.png', 0, 'attachment', 'image/png', 0),
(3196, 1, '2019-05-15 07:53:56', '2019-05-15 07:53:56', '', 'Yuma-Test-1920x', '', 'inherit', 'open', 'closed', '', 'yuma-test-1920x', '', '', '2019-05-15 07:53:56', '2019-05-15 07:53:56', '', 3193, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Yuma-Test-1920x.jpg', 0, 'attachment', 'image/jpeg', 0),
(3197, 1, '2019-05-15 07:54:08', '2019-05-15 07:54:08', '', 'Yuma', '', 'inherit', 'closed', 'closed', '', '3193-revision-v1', '', '', '2019-05-15 07:54:08', '2019-05-15 07:54:08', '', 3193, 'http://localhost:8888/qei_lastseen/2019/05/15/3193-revision-v1/', 0, 'revision', '', 0),
(3198, 1, '2019-05-15 07:59:02', '2019-05-15 07:59:02', '', 'last-seen-title-master', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master', '', '', '2019-05-15 07:59:02', '2019-05-15 07:59:02', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master.png', 0, 'attachment', 'image/png', 0),
(3199, 1, '2019-05-15 07:59:03', '2019-05-15 07:59:03', '', 'last-seen-title-master@0,5x', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master05x', '', '', '2019-05-15 07:59:03', '2019-05-15 07:59:03', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05x.png', 0, 'attachment', 'image/png', 0),
(3200, 1, '2019-05-15 08:01:17', '2019-05-15 08:01:17', '', 'last-seen-title-master@0,5xNEG', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master05xneg', '', '', '2019-05-15 08:01:17', '2019-05-15 08:01:17', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master@05xNEG.png', 0, 'attachment', 'image/png', 0),
(3201, 1, '2019-05-15 08:01:19', '2019-05-15 08:01:19', '', 'last-seen-title-masterNEG', '', 'inherit', 'open', 'closed', '', 'last-seen-title-masterneg', '', '', '2019-05-15 08:01:19', '2019-05-15 08:01:19', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-masterNEG.png', 0, 'attachment', 'image/png', 0),
(3202, 1, '2019-05-15 08:09:29', '2019-05-15 08:09:29', '', 'Yuma Antoine Decaux by Kate McKay', '', 'inherit', 'closed', 'closed', '', '3193-revision-v1', '', '', '2019-05-15 08:09:29', '2019-05-15 08:09:29', '', 3193, 'http://localhost:8888/qei_lastseen/2019/05/15/3193-revision-v1/', 0, 'revision', '', 0),
(3203, 1, '2019-05-15 08:18:46', '2019-05-15 08:18:46', '', 'Yuma-Test-800xSketch', '', 'inherit', 'open', 'closed', '', 'yuma-test-800xsketch', '', '', '2019-05-15 08:18:46', '2019-05-15 08:18:46', '', 3193, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Yuma-Test-800xSketch.jpg', 0, 'attachment', 'image/jpeg', 0),
(3204, 1, '2019-05-15 08:22:34', '2019-05-15 08:22:34', '', 'Yuma Antoine Decaux', '', 'inherit', 'closed', 'closed', '', '3193-autosave-v1', '', '', '2019-05-15 08:22:34', '2019-05-15 08:22:34', '', 3193, 'http://localhost:8888/qei_lastseen/2019/05/15/3193-autosave-v1/', 0, 'revision', '', 0),
(3205, 1, '2019-05-15 09:26:28', '2019-05-15 09:26:28', '', 'Yuma Antoine Decaux', '', 'inherit', 'closed', 'closed', '', '3193-revision-v1', '', '', '2019-05-15 09:26:28', '2019-05-15 09:26:28', '', 3193, 'http://localhost:8888/qei_lastseen/2019/05/15/3193-revision-v1/', 0, 'revision', '', 0),
(3209, 1, '2019-05-15 09:47:19', '2019-05-15 09:47:19', '', 'The Auction', '', 'publish', 'closed', 'closed', '', 'the-auction', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3209', 3, 'nav_menu_item', '', 0),
(3211, 1, '2019-05-15 09:47:19', '2019-05-15 09:47:19', '', 'Our Sponsors', '', 'publish', 'closed', 'closed', '', 'our-sponsors', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3211', 5, 'nav_menu_item', '', 0),
(3212, 1, '2019-05-15 09:47:19', '2019-05-15 09:47:19', '', 'Donate', '', 'publish', 'closed', 'closed', '', 'donate', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3212', 6, 'nav_menu_item', '', 0),
(3213, 1, '2019-05-15 09:48:56', '2019-05-15 09:48:56', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h1>If you wish to make an apple</h1>\r\n<h1>pie from scratch, you must first</h1>\r\n<h1>invent the universe.</h1>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1557912914701{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" project_style=\"9\" item_spacing=\"default\" load_in_animation=\"perspective\" projects_per_page=\"4\"][/vc_column_inner][/vc_row_inner][vc_text_separator title=\"Watch you project reel\" i_icon_fontawesome=\"fa fa-play-circle-o\" i_color=\"custom\" color=\"custom\" add_icon=\"true\" i_custom_color=\"#046272\" accent_color=\"#046272\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" video_external=\"https://www.youtube.com/watch?v=4BfKFCOCJe8\" text_color=\"light\" text_align=\"left\" top_padding=\"15%\" bottom_padding=\"15%\" enable_gradient=\"true\" color_overlay=\"#585b64\" color_overlay_2=\"#2c2f38\" gradient_direction=\"left_to_right\" overlay_strength=\"0.8\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-15 09:48:56', '2019-05-15 09:48:56', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/15/6-revision-v1/', 0, 'revision', '', 0),
(3214, 1, '2019-05-15 09:55:27', '2019-05-15 09:55:27', '', 'last-seen-title-master-400x90', '', 'inherit', 'open', 'closed', '', 'last-seen-title-master-400x90', '', '', '2019-05-15 09:55:27', '2019-05-15 09:55:27', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/last-seen-title-master-400x90.png', 0, 'attachment', 'image/png', 0),
(3215, 1, '2019-05-15 09:57:30', '2019-05-15 09:57:30', '', 'Tagline_PWW-Lion-Grey@1', '', 'inherit', 'open', 'closed', '', 'tagline_pww-lion-grey1', '', '', '2019-05-15 09:57:30', '2019-05-15 09:57:30', '', 0, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Tagline_PWW-Lion-Grey@1.png', 0, 'attachment', 'image/png', 0),
(3216, 1, '2019-05-15 09:58:18', '2019-05-15 09:58:18', '', 'Tagline_PWW-Lion-Grey@1', '', 'inherit', 'open', 'closed', '', 'tagline_pww-lion-grey1-2', '', '', '2019-05-15 09:58:18', '2019-05-15 09:58:18', '', 6, 'http://localhost:8888/qei_lastseen/wp-content/uploads/2019/05/Tagline_PWW-Lion-Grey@1-1.png', 0, 'attachment', 'image/png', 0),
(3217, 1, '2019-05-15 10:04:24', '2019-05-15 10:04:24', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" enable_shape_divider=\"true\" shape_divider_position=\"both\" shape_divider_height=\"100\" bg_image_animation=\"none\" shape_type=\"fan\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h1>If you wish to make an apple</h1>\r\n<h1>pie from scratch, you must first</h1>\r\n<h1>invent the universe.</h1>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1557912914701{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" project_style=\"9\" item_spacing=\"default\" load_in_animation=\"perspective\" projects_per_page=\"4\"][vc_text_separator title=\"Watch you project reel\" i_icon_fontawesome=\"fa fa-play-circle-o\" i_color=\"custom\" color=\"custom\" add_icon=\"true\" i_custom_color=\"#046272\" accent_color=\"#046272\"][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" video_external=\"https://www.youtube.com/watch?v=4BfKFCOCJe8\" text_color=\"light\" text_align=\"left\" top_padding=\"15%\" bottom_padding=\"15%\" enable_gradient=\"true\" color_overlay=\"#585b64\" color_overlay_2=\"#2c2f38\" gradient_direction=\"left_to_right\" overlay_strength=\"0.8\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" vertically_center_columns=\"true\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text css_animation=\"fadeIn\"]Our sponsors[/vc_column_text][clients columns=\"5\" hover_effect=\"opacity\" additional_padding=\"none\" fade_in_animation=\"true\" carousel=\"true\"][client image=\"3216\" title=\"Client\" id=\"1557913906078-7\" tab_id=\"1557913906080-9\" url=\"#\" name=\"Publicis Worldwide\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557913906118-5\" tab_id=\"1557913906118-2\" url=\"#\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914353792-0\" tab_id=\"1557914353793-5\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914354596-8\" tab_id=\"1557914354596-3\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914355523-3\" tab_id=\"1557914355524-10\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914357206-6\" tab_id=\"1557914357206-7\" url=\"#\"][/client][/clients][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-15 10:04:24', '2019-05-15 10:04:24', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/15/6-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3218, 1, '2019-05-15 10:12:03', '2019-05-15 10:12:03', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"10%\" bottom_padding=\"10%\" overlay_strength=\"1\" enable_shape_divider=\"true\" shape_divider_position=\"both\" shape_divider_height=\"100\" bg_image_animation=\"none\" shape_type=\"fan\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][split_line_heading]\r\n<h1>What would you want to see</h1>\r\n<h1>one more time if you</h1>\r\n<h1>lost your sight?</h1>\r\n[/split_line_heading][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\"][vc_column_text css=\".vc_custom_1557912914701{padding-right: 10% !important;}\"]Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.[/vc_column_text][/vc_column_inner][vc_column_inner column_padding=\"padding-3-percent\" column_padding_position=\"top-bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/2\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-bottom\" delay=\"100\"][vc_column_text css=\".vc_custom_1485638241294{padding-right: 10% !important;}\"]Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner column_margin=\"default\" top_padding=\"20\" text_align=\"left\"][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/1\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"fade-in-from-left\"][nectar_cta btn_style=\"see-through\" heading_tag=\"h5\" text_color=\"#0a0000\" link_type=\"regular\" alignment=\"left\" link_text=\"Learn More\" url=\"#\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" project_style=\"9\" item_spacing=\"default\" load_in_animation=\"perspective\" projects_per_page=\"4\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Watch the reel</h2>\r\n[/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" video_bg=\"use_video\" video_external=\"https://www.youtube.com/watch?v=4BfKFCOCJe8\" text_color=\"light\" text_align=\"left\" top_padding=\"15%\" bottom_padding=\"15%\" enable_gradient=\"true\" color_overlay=\"#585b64\" color_overlay_2=\"#2c2f38\" gradient_direction=\"left_to_right\" overlay_strength=\"0.8\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" vertically_center_columns=\"true\" scene_position=\"center\" text_color=\"dark\" text_align=\"center\" top_padding=\"5%\" bottom_padding=\"0\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text css_animation=\"fadeIn\"]\r\n<h2>Our sponsors</h2>\r\n[/vc_column_text][divider line_type=\"No Line\" custom_height=\"50\"][clients columns=\"4\" hover_effect=\"opacity\" additional_padding=\"3\" fade_in_animation=\"true\" carousel=\"true\"][client image=\"3216\" title=\"Client\" id=\"1557914669970-4\" tab_id=\"1557914669971-4\" url=\"#\" name=\"Publicis Worldwide\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670041-7\" tab_id=\"1557914670041-7\" url=\"#\"] Click the edit button to add your testimonial. [/client][client image=\"3216\" title=\"Client\" id=\"1557914670106-8\" tab_id=\"1557914670107-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670156-4\" tab_id=\"1557914670157-9\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670219-2\" tab_id=\"1557914670219-8\" url=\"#\"][/client][client image=\"3216\" title=\"Client\" id=\"1557914670261-9\" tab_id=\"1557914670261-7\" url=\"#\"][/client][/clients][/vc_column][/vc_row]', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-05-15 10:12:03', '2019-05-15 10:12:03', '', 6, 'http://localhost:8888/qei_lastseen/2019/05/15/6-revision-v1/', 0, 'revision', '', 0),
(3219, 1, '2019-05-15 10:17:11', '2019-05-15 10:17:11', '', 'The work', '', 'publish', 'closed', 'closed', '', 'the-work', '', '', '2019-05-15 10:17:12', '2019-05-15 10:17:12', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=3219', 0, 'page', '', 0),
(3220, 1, '2019-05-15 10:13:16', '2019-05-15 10:13:16', '', 'The work', '', 'inherit', 'closed', 'closed', '', '3219-revision-v1', '', '', '2019-05-15 10:13:16', '2019-05-15 10:13:16', '', 3219, 'http://localhost:8888/qei_lastseen/2019/05/15/3219-revision-v1/', 0, 'revision', '', 0),
(3222, 1, '2019-05-15 10:26:22', '2019-05-15 10:26:22', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_image=\"https://source.unsplash.com/collection/217569/daily\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#3a3a3a\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"17%\" bottom_padding=\"17%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"padding-7-percent\" column_padding_position=\"all\" background_color=\"#ffffff\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" top_margin=\"-6%\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]\r\n<h5 style=\"text-align: center;\">About the Last Seen project</h5>\r\n[/vc_column_text][divider line_type=\"Small Line\" line_alignment=\"default\" line_thickness=\"2\" divider_color=\"accent-color\" animate=\"yes\" custom_height=\"50\" custom_line_width=\"80\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" offset=\"vc_hidden-xs\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"2/3\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]It was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned![/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" offset=\"vc_hidden-xs\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"2%\" bottom_padding=\"2%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner equal_height=\"yes\" content_placement=\"bottom\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/430x600/daily/?beach\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/420x520/daily/?fashion\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x600/daily/?view\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/800x450/daily/?business\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/800x400/daily/?happy\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/450x650/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][vc_row_inner equal_height=\"yes\" content_placement=\"top\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/450x600/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/700x400/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/700x300/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x650/daily/?shoes\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/600x600/daily/?smile\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/500x650/daily/?adventure\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'About Last Seen', '', 'publish', 'closed', 'closed', '', 'about-last-seen', '', '', '2019-05-15 23:17:01', '2019-05-15 23:17:01', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=3222', 0, 'page', '', 0),
(3223, 1, '2019-05-15 10:22:31', '2019-05-15 10:22:31', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_image=\"https://source.unsplash.com/collection/217569/daily\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#3a3a3a\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"17%\" bottom_padding=\"17%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"padding-7-percent\" column_padding_position=\"all\" background_color=\"#ffffff\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" top_margin=\"-6%\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]\r\n<h5 style=\"text-align: center;\">How The Project Was Received</h5>\r\n[/vc_column_text][divider line_type=\"Small Line\" line_alignment=\"default\" line_thickness=\"2\" divider_color=\"accent-color\" animate=\"yes\" custom_height=\"50\" custom_line_width=\"80\"][testimonial_slider style=\"minimal\" star_rating_color=\"accent-color\" disable_height_animation=\"true\"][testimonial star_rating=\"none\" title=\"Testimonial\" id=\"1557915671803-7\" name=\"Eve Crawford\" subtitle=\"Product Designer\" quote=\"It was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\" tab_id=\"1557915671804-5\"][/testimonial_slider][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"2%\" bottom_padding=\"2%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner equal_height=\"yes\" content_placement=\"bottom\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/430x600/daily/?beach\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/420x520/daily/?fashion\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x600/daily/?view\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/800x450/daily/?business\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/800x400/daily/?happy\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/450x650/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][vc_row_inner equal_height=\"yes\" content_placement=\"top\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/450x600/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/700x400/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/700x300/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x650/daily/?shoes\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/600x600/daily/?smile\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/500x650/daily/?adventure\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'About Last Seen', '', 'inherit', 'closed', 'closed', '', '3222-revision-v1', '', '', '2019-05-15 10:22:31', '2019-05-15 10:22:31', '', 3222, 'http://localhost:8888/qei_lastseen/2019/05/15/3222-revision-v1/', 0, 'revision', '', 0),
(3224, 1, '2019-05-15 10:24:12', '2019-05-15 10:24:12', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_image=\"https://source.unsplash.com/collection/217569/daily\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#3a3a3a\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"17%\" bottom_padding=\"17%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"padding-7-percent\" column_padding_position=\"all\" background_color=\"#ffffff\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" top_margin=\"-6%\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]\r\n<h5 style=\"text-align: center;\">About the Last Seen project</h5>\r\n[/vc_column_text][divider line_type=\"Small Line\" line_alignment=\"default\" line_thickness=\"2\" divider_color=\"accent-color\" animate=\"yes\" custom_height=\"50\" custom_line_width=\"80\"][vc_column_text]It was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned![/vc_column_text][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"2%\" bottom_padding=\"2%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner equal_height=\"yes\" content_placement=\"bottom\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/430x600/daily/?beach\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/420x520/daily/?fashion\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x600/daily/?view\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/800x450/daily/?business\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/800x400/daily/?happy\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/450x650/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][vc_row_inner equal_height=\"yes\" content_placement=\"top\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/450x600/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/700x400/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/700x300/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x650/daily/?shoes\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/600x600/daily/?smile\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/500x650/daily/?adventure\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'About Last Seen', '', 'inherit', 'closed', 'closed', '', '3222-revision-v1', '', '', '2019-05-15 10:24:12', '2019-05-15 10:24:12', '', 3222, 'http://localhost:8888/qei_lastseen/2019/05/15/3222-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3225, 1, '2019-05-15 10:25:36', '2019-05-15 10:25:36', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_image=\"https://source.unsplash.com/collection/217569/daily\" bg_position=\"left top\" bg_repeat=\"no-repeat\" bg_color=\"#3a3a3a\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"17%\" bottom_padding=\"17%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"none\"][nectar_video_lightbox link_style=\"play_button\" video_url=\"https://www.youtube.com/watch?v=6oTurM7gESE\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" bg_color=\"#ffffff\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column centered_text=\"true\" column_padding=\"padding-7-percent\" column_padding_position=\"all\" background_color=\"#ffffff\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" top_margin=\"-6%\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]\r\n<h5 style=\"text-align: center;\">About the Last Seen project</h5>\r\n[/vc_column_text][divider line_type=\"Small Line\" line_alignment=\"default\" line_thickness=\"2\" divider_color=\"accent-color\" animate=\"yes\" custom_height=\"50\" custom_line_width=\"80\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" offset=\"vc_hidden-xs\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"2/3\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]It was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned!\r\n\r\nIt was really fun getting to know the team during the project. They were all helpful in answering my questions and made me feel at ease. The design ended up being better than I could have envisioned![/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" offset=\"vc_hidden-xs\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"2%\" bottom_padding=\"2%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_row_inner equal_height=\"yes\" content_placement=\"bottom\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/430x600/daily/?beach\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/420x520/daily/?fashion\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x600/daily/?view\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/800x450/daily/?business\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/800x400/daily/?happy\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/450x650/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][vc_row_inner equal_height=\"yes\" content_placement=\"top\" column_margin=\"default\" text_align=\"left\"][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\"][image_with_animation image_url=\"https://source.unsplash.com/450x600/daily/?nature\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"100\"][image_with_animation image_url=\"https://source.unsplash.com/700x400/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][image_with_animation image_url=\"https://source.unsplash.com/700x300/daily/?people\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\" margin_top=\"5%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"200\"][image_with_animation image_url=\"https://source.unsplash.com/800x650/daily/?shoes\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"padding-1-percent\" column_padding_position=\"bottom-right\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"300\"][image_with_animation image_url=\"https://source.unsplash.com/600x600/daily/?smile\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][vc_column_inner column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_shadow=\"none\" column_border_radius=\"none\" column_link_target=\"_self\" width=\"1/5\" tablet_width_inherit=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\" enable_animation=\"true\" animation=\"grow-in\" offset=\"vc_col-xs-1/5\" delay=\"400\"][image_with_animation image_url=\"https://source.unsplash.com/500x650/daily/?adventure\" alignment=\"\" animation=\"None\" border_radius=\"none\" box_shadow=\"none\" max_width=\"100%\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]', 'About Last Seen', '', 'inherit', 'closed', 'closed', '', '3222-revision-v1', '', '', '2019-05-15 10:25:36', '2019-05-15 10:25:36', '', 3222, 'http://localhost:8888/qei_lastseen/2019/05/15/3222-revision-v1/', 0, 'revision', '', 0),
(3226, 1, '2019-05-15 10:31:32', '2019-05-15 10:31:32', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" bg_color=\"#181a1c\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"9\" item_spacing=\"1px\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"accent-color\" load_in_animation=\"none\"][/vc_column][/vc_row]', 'The Artists', '', 'publish', 'closed', 'closed', '', 'the-artists', '', '', '2019-05-15 10:34:59', '2019-05-15 10:34:59', '', 0, 'http://localhost:8888/qei_lastseen/?page_id=3226', 0, 'page', '', 0),
(3227, 1, '2019-05-15 10:26:59', '2019-05-15 10:26:59', '', 'The Artists', '', 'inherit', 'closed', 'closed', '', '3226-revision-v1', '', '', '2019-05-15 10:26:59', '2019-05-15 10:26:59', '', 3226, 'http://localhost:8888/qei_lastseen/2019/05/15/3226-revision-v1/', 0, 'revision', '', 0),
(3228, 1, '2019-05-15 10:31:32', '2019-05-15 10:31:32', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" bg_color=\"#181a1c\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_gallery type=\"flickity_style\" flickity_controls=\"material_pagination\" flickity_desktop_columns=\"3\" flickity_small_desktop_columns=\"3\" flickity_tablet_columns=\"1\" flickity_box_shadow=\"x_large_depth\" onclick=\"link_no\" img_size=\"full\"][/vc_column][/vc_row]</p>\n', 'The Artists', '', 'inherit', 'closed', 'closed', '', '3226-revision-v1', '', '', '2019-05-15 10:31:32', '2019-05-15 10:31:32', '', 3226, 'http://localhost:8888/qei_lastseen/2019/05/15/3226-revision-v1/', 0, 'revision', '', 0),
(3229, 1, '2019-05-15 10:34:23', '2019-05-15 10:34:23', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" bg_color=\"#181a1c\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" project_style=\"1\" item_spacing=\"default\" load_in_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'The Artists', '', 'inherit', 'closed', 'closed', '', '3226-autosave-v1', '', '', '2019-05-15 10:34:23', '2019-05-15 10:34:23', '', 3226, 'http://localhost:8888/qei_lastseen/2019/05/15/3226-autosave-v1/', 0, 'revision', '', 0),
(3230, 1, '2019-05-15 10:34:59', '2019-05-15 10:34:59', '[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" bg_color=\"#181a1c\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" top_padding=\"6%\" bottom_padding=\"6%\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"9\" item_spacing=\"1px\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"accent-color\" load_in_animation=\"none\"][/vc_column][/vc_row]', 'The Artists', '', 'inherit', 'closed', 'closed', '', '3226-revision-v1', '', '', '2019-05-15 10:34:59', '2019-05-15 10:34:59', '', 3226, 'http://localhost:8888/qei_lastseen/2019/05/15/3226-revision-v1/', 0, 'revision', '', 0),
(3232, 1, '2019-05-15 10:36:49', '2019-05-15 10:36:49', ' ', '', '', 'publish', 'closed', 'closed', '', '3232', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3232', 2, 'nav_menu_item', '', 0),
(3233, 1, '2019-05-15 10:36:49', '2019-05-15 10:36:49', ' ', '', '', 'publish', 'closed', 'closed', '', '3233', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3233', 4, 'nav_menu_item', '', 0),
(3234, 1, '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 'The Work', '', 'publish', 'closed', 'closed', '', 'the-work-2', '', '', '2019-05-15 10:36:49', '2019-05-15 10:36:49', '', 0, 'http://localhost:8888/qei_lastseen/?p=3234', 1, 'nav_menu_item', '', 0),
(3235, 1, '2019-05-22 22:34:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-05-22 22:34:27', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/qei_lastseen/?p=3235', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Fashion', 'fashion', 0),
(17, 'Food for thought', 'food-for-thought', 0),
(18, 'Gaming', 'gaming', 0),
(19, 'Music', 'music', 0),
(20, 'Quotes', 'quotes', 0),
(21, 'Art', 'art', 0),
(22, 'Awesome', 'awesome', 0),
(23, 'Cars', 'cars', 0),
(24, 'Classic', 'classic', 0),
(25, 'Custom', 'custom', 0),
(26, 'Data', 'data', 0),
(27, 'Epic', 'epic', 0),
(28, 'Funny', 'funny', 0),
(29, 'Gaming Tips', 'gaming-tips', 0),
(30, 'Music', 'music', 0),
(31, 'Photography', 'photography', 0),
(32, 'Standard', 'standard', 0),
(33, 'ThemeNectar', 'themenectar', 0),
(34, 'Videos', 'videos', 0),
(35, 'Wordpress', 'wordpress', 0),
(36, 'About', 'about', 0),
(37, 'About Single', 'about-single', 0),
(39, 'Branding', 'branding', 0),
(40, 'Coding', 'coding', 0),
(42, 'Home', 'home', 0),
(43, 'Home Portfolio Eye Candy', 'portfolio-home', 0),
(44, 'HTML/CSS', 'htmlcss', 0),
(45, 'Illustration', 'illustration', 0),
(47, 'Lowpoly', 'lowpoly', 0),
(48, 'Macro Lens', 'macro-lens', 0),
(49, 'Night To Remember', 'night-to-remember', 0),
(50, 'Photography', 'photography', 0),
(53, 'Weather App', 'weather-app', 0),
(54, 'Main Navigation', 'main-navigation', 0),
(55, 'Link', 'post-format-link', 0),
(56, 'Audio', 'post-format-audio', 0),
(57, 'Video', 'post-format-video', 0),
(58, 'Quote', 'post-format-quote', 0),
(59, 'Artists', 'artists', 0),
(60, 'Subjects', 'subjects', 0),
(61, 'Artwork', 'artwork', 0),
(62, 'Primary Nav', 'primary-nav', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(7, 54, 0),
(8, 54, 0),
(9, 54, 0),
(10, 54, 0),
(11, 54, 0),
(12, 54, 0),
(13, 54, 0),
(14, 54, 0),
(15, 54, 0),
(16, 54, 0),
(17, 54, 0),
(18, 54, 0),
(19, 54, 0),
(20, 54, 0),
(21, 54, 0),
(22, 54, 0),
(23, 54, 0),
(24, 54, 0),
(25, 54, 0),
(26, 54, 0),
(27, 54, 0),
(28, 54, 0),
(82, 19, 0),
(82, 56, 0),
(84, 1, 0),
(84, 57, 0),
(108, 20, 0),
(108, 57, 0),
(110, 17, 0),
(110, 21, 0),
(110, 22, 0),
(110, 23, 0),
(110, 24, 0),
(110, 25, 0),
(110, 26, 0),
(110, 27, 0),
(110, 28, 0),
(110, 29, 0),
(110, 30, 0),
(110, 31, 0),
(110, 32, 0),
(110, 33, 0),
(110, 34, 0),
(110, 35, 0),
(137, 17, 0),
(137, 55, 0),
(152, 1, 0),
(152, 57, 0),
(559, 17, 0),
(615, 42, 0),
(617, 42, 0),
(701, 42, 0),
(1239, 16, 0),
(1251, 16, 0),
(2326, 1, 0),
(2327, 20, 0),
(2327, 58, 0),
(2328, 18, 0),
(2329, 1, 0),
(2671, 1, 0),
(2677, 16, 0),
(3145, 18, 0),
(3146, 54, 0),
(3147, 54, 0),
(3148, 54, 0),
(3149, 54, 0),
(3150, 54, 0),
(3151, 54, 0),
(3152, 54, 0),
(3153, 54, 0),
(3154, 54, 0),
(3155, 54, 0),
(3156, 54, 0),
(3157, 54, 0),
(3158, 54, 0),
(3159, 54, 0),
(3160, 54, 0),
(3162, 54, 0),
(3163, 54, 0),
(3209, 62, 0),
(3211, 62, 0),
(3212, 62, 0),
(3232, 62, 0),
(3233, 62, 0),
(3234, 62, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 6),
(2, 2, 'product_type', '', 0, 0),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'category', '', 0, 3),
(17, 17, 'category', '', 0, 3),
(18, 18, 'category', '', 0, 2),
(19, 19, 'category', '', 0, 1),
(20, 20, 'category', '', 0, 2),
(21, 21, 'post_tag', '', 0, 1),
(22, 22, 'post_tag', '', 0, 1),
(23, 23, 'post_tag', '', 0, 1),
(24, 24, 'post_tag', '', 0, 1),
(25, 25, 'post_tag', '', 0, 1),
(26, 26, 'post_tag', '', 0, 1),
(27, 27, 'post_tag', '', 0, 1),
(28, 28, 'post_tag', '', 0, 1),
(29, 29, 'post_tag', '', 0, 1),
(30, 30, 'post_tag', '', 0, 1),
(31, 31, 'post_tag', '', 0, 1),
(32, 32, 'post_tag', '', 0, 1),
(33, 33, 'post_tag', '', 0, 1),
(34, 34, 'post_tag', '', 0, 1),
(35, 35, 'post_tag', '', 0, 1),
(36, 36, 'slider-locations', '', 0, 0),
(37, 37, 'slider-locations', '', 0, 0),
(39, 39, 'project-attributes', '', 0, 0),
(40, 40, 'slider-locations', '', 0, 0),
(42, 42, 'slider-locations', '', 0, 3),
(43, 43, 'slider-locations', '', 0, 0),
(44, 44, 'project-attributes', '', 0, 0),
(45, 45, 'project-attributes', '', 0, 0),
(47, 47, 'slider-locations', '', 0, 0),
(48, 48, 'project-attributes', '', 0, 0),
(49, 49, 'slider-locations', '', 0, 0),
(50, 50, 'project-attributes', '', 0, 0),
(53, 53, 'slider-locations', '', 0, 0),
(54, 54, 'nav_menu', '', 0, 39),
(55, 55, 'post_format', '', 0, 1),
(56, 56, 'post_format', '', 0, 1),
(57, 57, 'post_format', '', 0, 3),
(58, 58, 'post_format', '', 0, 1),
(59, 59, 'project-type', '', 0, 0),
(60, 60, 'project-type', '', 0, 0),
(61, 61, 'project-type', '', 0, 0),
(62, 62, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'PublicisWW'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'vc_pointers_backend_editor,vc_pointers_frontend_editor'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"7be4d882173681bd7d304b84e94a70dbc149c261bda404f8cf42b96a7df89ac2\";a:4:{s:10:\"expiration\";i:1558737261;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36\";s:5:\"login\";i:1558564461;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '3235'),
(18, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(19, 1, '_woocommerce_tracks_anon_id', 'woo:3f/isK3CEs9Hp+FGLpfcXs5A'),
(20, 1, 'wc_last_active', '1558483200'),
(21, 1, 'wp_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1557888758;}'),
(22, 1, 'wp_user-settings', 'edit_element_vcUIPanelWidth=650&edit_element_vcUIPanelLeft=729px&edit_element_vcUIPanelTop=74px&editor=tinymce&libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1557809224'),
(24, 1, 'community-events-location', 'a:4:{s:11:\"description\";s:8:\"Brisbane\";s:8:\"latitude\";s:11:\"-27.4679400\";s:9:\"longitude\";s:11:\"153.0280900\";s:7:\"country\";s:2:\"AU\";}'),
(25, 1, 'closedpostboxes_nectar_slider', 'a:0:{}'),
(26, 1, 'metaboxhidden_nectar_slider', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(27, 1, 'nav_menu_recently_edited', '62'),
(28, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(29, 1, 'metaboxhidden_nav-menus', 'a:8:{i:0;s:23:\"add-post-type-portfolio\";i:1;s:21:\"add-post-type-product\";i:2;s:12:\"add-post_tag\";i:3;s:15:\"add-post_format\";i:4;s:16:\"add-project-type\";i:5;s:22:\"add-project-attributes\";i:6;s:15:\"add-product_cat\";i:7;s:15:\"add-product_tag\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'PublicisWW', '$P$Bbb70Bqky6.ZygGvMr70bKc2ZTA9pY1', 'publicisww', 'damien.doonan@publicis.com.au', '', '2019-05-14 02:45:40', '', 0, 'PublicisWW');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(10,2) DEFAULT NULL,
  `max_price` decimal(10,2) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=844;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1993;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3236;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
