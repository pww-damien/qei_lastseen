<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'qei_lastseen' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'P~wkY^9Nw0Q*WlNeSm/e0TYbQ<*1mKZshb#NjyTmR16&71$}gH_4-x^L]tDM|6K=' );
define( 'SECURE_AUTH_KEY',  'm(T^wHhh:]M@t&Jl{;fs:}<_YQ_<)r[N}d6W4+F_1pom?>D!AykWj/;Z/=4,-.t3' );
define( 'LOGGED_IN_KEY',    '`+6EL=G11CF+e^n4LN7YI_`KQ(3zm,D8=/sfHOmqmhd>64ft[Gqd`5)8OAAWa9&V' );
define( 'NONCE_KEY',        '.p5B7v4H9oDjSgP%aG%w,uG@~qL+p+wbfR[IR} *i{>h*afmzag45}+!E}< O`~ ' );
define( 'AUTH_SALT',        'jsu]8HysgUB6OIFE}MJw,`Cus`_2`Lf9HL56Ndm&H!*%4O.mi#9<KL3BdxuK-sOV' );
define( 'SECURE_AUTH_SALT', '*KtP8j<5wF%:J*.<ojsX_cK+;TV|ZgaE|pj|b!,A<@IwhLWLtQh->?SN4iA%lM^V' );
define( 'LOGGED_IN_SALT',   '_57-6I=};+dlb./gYa^TfYxCRJtS_qzlU5K*Dr3B@s$zYD+$sWfwF,}%k~4Eo*sK' );
define( 'NONCE_SALT',       '^i<DRam#koQ,_k92ft6jKN(|Pd&bj;KiHSM;+)-,*F3j4m!x90ci_$*`=2@J@,+t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
